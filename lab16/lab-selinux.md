# In Class lab: Advanced permissions and SELinux

>__Objective:__ Work with SELinux

>__Note:__ It is assumed that you are doing this lab on your **development** host and not your production server

It is possible that during this lab you will lock yourself out the host you are working with.  If this is your prod server you will have to reach out for help.  However, if you are using your development host then you can login directly using the VMware console and resolve the issue.

## Confirming the current SELinux status and configuration

First lets take a look at what SELinux packages are already installed and what SELinux man pages are available. It is likely that you only have one selinux man page installed. We will cover DNF and RPM more later in the quarter but in this example we can see a list of the RPM SELinux packages using either the local RPM index or using yum list.

```bash
sudo dnf list installed | grep selinux
rpm -qa 
rpm -qa | grep selinux
```

Again just to show another option we are going to use man with the option **-k** and use **apropos** to see what options and commands are available for selinux.  When using man **-k** we are asking for content that relates to the search query.  When using apropos we see list of content related to the term provided such as **selinux**. The results are often similar between **man -k** and **apropos**.

```bash
man -k selinux
man -k enforce
apropos sestatus
apropos "security context"
apropos security context
```

>**Note:** If the response from **man -k** or **apropos** is ["nothing appropriate"](no-man-pages.png) you may need to install the man pages or possibly remove the existing man pages database files and recreate them.  This can be done using `sudo mandb` or to recreate the database files use `sudo mandb`.  [Ignore any failed updates](mandb-install.png).

Now we will confirm the current state of SELinux on our development host using two different commands.  Note that neither of these commands require sudo access.  If we want to switch from "Enforcing" to "Permissive" this can be done temporarily using `setenforce` and passing in "0" for Permissive and "1" for Enforcing.  Next we can make it permanent by updating the selinux configuration file to match our desired state.

```bash
getenforce
sestatus
cat /etc/sysconfig/selinux
sudo setenforce 0
getenforce
sestatus
cat /etc/sysconfig/selinux
```

Now we can use semodule to display all the installed modules beyond those included in the base. This may take a couple seconds to run.

```bash
sudo semodule -l
```

## Working with SELinux and Port context labels

Our next objective is to run our ssh listener on a non-standard port, in this case 222.  This may require that you have console access, as is typically the case this lab should be completed on your **development** host. First we need to be sure that SELinux is in __Enforcing__ mode.  Next backup your existing sshd_config file, it should be under /etc/ssh/.  

>__Note:__ Remember to do this work on hour **Development** system

```bash
getenforce
sudo setenforce 1
getenforce
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
```

Now we want to change the listen port to 222, to do this uncomment the Port directive and change the value to 222.

```bash
#
Port 222
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```

>__Stretch Challenge:__ Can you backup the config file and update the port using sed in one command?  Think back to the notes on updating the firewalld config and setting [__AllowZoneDrifting__](https://www.ulcert.uw.edu/itfdn-labs/lab14/AllowZoneDrifting.html) to no. The gif below is showing you how to make the change using Vi.

![sshd config](edit_sshd_config.gif)

Save your changes and try to restart sshd.  It should fail, check the status using systemctl status.  

```bash
sudo systemctl restart sshd
sudo systemctl status sshd
```

Review the audit logs `sudo less /var/log/audit/audit.log` and search for message type AVC.  You should find something referring to src=222.

>Note: if you don't see this error and or your sshd service does restart make sure you have SELinux set to `Enforcing`.

```bash
...avc: denied { name_bind } for pid=2231 comm="sshd" src=222 scontext=system_u:system_r:sshd_t:s0-s0:c0.c1023 tcontext=system_u:object_r:reserved_port_t:s0 tclass=tcp_socket
```

To fix this we need to update the SELinux policy and then restart ssh again, confirm the service is running and that we have a listener on that port. There are three `ss` command examples, consider what the differences are in the output. Remember that SELInux is all about labels, in this case we are updating labels on using semanage so this command make take a few seconds to complete.

>__Note__ If you get an error _"sudo: semanage: command not found"_ you may need to install the __policycoreutils-python__ package using yum: `sudo dnf install policycoreutils-python`

```bash
sudo semanage port -a -t ssh_port_t -p tcp 222
sudo systemctl restart sshd
systemctl status sshd
ss -t
ss -t -a
ss -tan
```

Now review your ssh status and try to connect to your host over port 222.  If your connection fails with a connection refused message this probably means something is blocking your inbound request to port 222.  What service manages access to ports and services that we worked with in [lab 14](https://www.ulcert.uw.edu/itfdn-labs/lab14/lab-firewall-mgmt.html) this week?

![ssh fail](ssh-port222.png)

>__Note:__ If your existing SSH session was dropped or closed you will need to use the VMWare console to login to your dev host and resolve the issue.  You can change the port back to 22 or look at lab14 for clue on how to allow traffic on port 222.

Feel free to switch your ssh port back to 22 if you want at this time as this was just a demo exercise.  If you want to leave your listener on port 222 you will need to update your system to allow traffic on port 222.

## Git and Markdown

Now we are going to add some more notes to our Git repo and update our README.md file to point to these notes.

```bash
cd ~/git/gitlab/itfdn-140-inclass-$USER
```

Next copy and paste this next snippet to get your initial file created.

```text
echo "# ACL tips

The following are notes and tips related to using **getfacl** and **setfacl**

" >> ~/git/gitlab/itfdn-140-inclass-$USER/selinux-tips.md
```

Next add a link in your README.md file that points to this new file like this.

```bash
## Additional notes

The following are links to other resources in this repo.

[firewalld and firewall-cmd](firewalld-tips.md)
[getfacl and setfacl cmds](acl-tips.md)
[SELinux cmds](selinux-tips.md)
```

Finally add these update to your gitlab project, commit the changes and then push them to your remote repository.

___

## Lab Complete

___