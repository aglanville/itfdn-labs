# In Class Lab 5

>Objective: Consider the core *nix tenets discussed.  Work with file redirection, standard in, standard out and standard error.

>__Note:__ It is assumed that you are doing this lab on your development server and **NOT** your production server.

You must open your VMWare player or Fusion application and then start your Development server (**itfdn-rhel8-dev01** or **itfdn-dev01**) in order to work through this lab.

**Mac users**

![Fusion start guest](../fusion-start-guest.png)

**Windows users**

![Player start guest](../vmware-player-guest.png)

___

## Consider the Linux core tenets?

Take a moment to think about the core tenets presented in the lecture.  Can you see where this holds true and where it falls short for UNIX and Linux?  Note at least one example of each and discuss it with another student in class.  

___

## Tasks: Working with re-direction, standard out, standard in and standard error

Answer the following two questions, and discuss your findings with your break out group.  Also be prepared to share your observations with the class.

* What happens when you type "echo \*" and press enter from within your home directory.  Note the  \* after the echo command.

![echo](echo.jpg)

* Why is the output from the following two commands different, how is the redirect impacting what is printed to standard out? Just compare the content returned, what is different?

```shell
wc -l  /etc/nsswitch.conf
wc -l < /etc/nsswitch.conf
```

### Re-direct Standard Error (stderr)

If we generate an error message it is possible to redirect that out into a file or into the bit bucket (discard).

We have discussed the **find** utility briefly and mentioned that it can be a very powerful tool on your system.  
In this command example with find we are looking for any files starting at root "/" and working our way through the file system looking for any file that end in "log"

```shell
find / -name "*log"
```

When you run this command without sudo and using your standard account I expect that you will error messages (Permission denied) along with successful results.

![Find Errors](find_errors.jpg)

In order to redirect the stderr output to a file try using the redirect ">" and then a path to a file.  If you do this correctly you will only see output for file that you have access to not those you are denied access to.  

>Note: if you are looking for hints for these commands check out: [Hints](../hints/lab5-hints.html)

Now try to redirect the output to /dev/null instead of a file.

___
Lab Complete
___
