# In class lab: Local Networking

>Objective: Work with local network tools and configuration files.

>Note: It is expected that you are doing this work on your development linux host and **NOT** your production server.

>Note: Review the lecture notes for additional tips on how to complete these steps.

When using the `ip` command the options can be shortened until they become ambiguous.  For example if the option is address you can shorten it to addr or even more and still get the same results.

It is possible that your primary network interface device id is **NOT** ens33, if it is different you will need to replace `ens33` in these examples with your device name.

In this section we are going to print out your network interface details using `ip` and then compare the difference using the -4 flag. 

>__Note:__ Your interface name may NOT be ens33, you may have to change the interface name to run some of these commands

```bash
ip address
ip -4 addr
```

Now we are going to print out just the external interface details and then just the loopback interface details.

>__Challenge:__ How short can you make the argument values for the `ip` command? will it accept **add** in place of **address**?  Can you make it even shorter? what about the other arguments?

```bash
ip address show ens33
ip addr show lo
```

![network interface](network-interface.png)

Display detailed ethernet values using ethtool, note the duplex and speed settings. Note the driver details with -i and check the latest set of statistics using -S

>__Note:__ Your interface name may NOT be ens33, you may have to change the interface name to run some of these commands
  
```shell
ethtool ens33
ethtool -i ens33
ethtool -S ens33
ethtool --help
```

In this section we will be changing the link status.  This means we are going to impact the physical connection to your virtual machine.  Notice the state value for the loopback interface (**lo**).  It should switch between UNKNOWN and DOWN.  Compare this to the external interface value.

>Warning: These commands may disable your system, don't run them on your production host.

```shell
ip link
sudo ip link set lo down
ip link show lo
ip link show ens33
ip link
sudo ip link set lo up
ip link
```

I expect that your SSH session (using Putty or Terminal) will drop with this next command.  Why do I think this will happen and how are you going to fix it?

```shell
ip link set ens33 down
sudo ip link set ens33 down
```

>__Note:__ This is the fix for the previous step.  Login to the vmware **console** and run these commands now.

```shell
ip link show ens33 
ip link set ens33 up
ip link
```

## Configuration files

>Note: file names are based on device names, your file name may be different

Locate your network configuration files

![network scripts](./network-configs.jpg)

Now we can review your network configuration file.  Note your **BOOTPROTO** setting, common values here are NONE, static, and dhcp.  DHCP stands for [Dynamic Host Configuration Protocol](https://www.infoblox.com/glossary/dhcp-server/).  DHCP is a service that will automatically assign an IP address and manage other network settings for the host.  When it is set to NONE or static the IP address and other network settings are set manually.
Note your **ONBOOT** setting, if this is set to yes then your network interface will start automatically when the system starts. Review the other settings and continue on.

```shell
cat /etc/sysconfig/network-scripts/ifcfg-ens33
```

![ethernet device configuration](ifcf-ens33.jpg)

### Configure your development host with a static IP

The IP address and related network settings were provided to our development servers via the VMWare DHCP service.  We are now going to update our development servers from being DHCP clients to having statically assigned IP addresses.  

The IP address used needs to in the same range as what your VMWare network is providing.  We will talk about networks, subnets and routes later but for now just assume that if your current IP address is 192.168.122.129 that you should either keep that address or pick a number in the last octet that is very close such as 192.168.122.128 or 127 for example.

We will backup the existing configuration file as is good practice before making changes.  Then we will change the BOOTPROTO line to none, confirm ONBOOT is set to yes, add our desired IP address with IPADDR, add our NETMASK, GATEWAY and DNS server details.  We will make these changes using Vim or your preferred command line editor.

So, first we will back up the existing configuration file.  In this example you can see we are saving the copy to our home directory using the **~** tilde symbol.

```bash
cp /etc/sysconfig/network-scripts/ifcfg-ens33 ~/ifcfg-ens33.backup
```

![back up config](ifcfg-backup.png)

Now confirm your existing IP address using `ip address show`.  Note the trailing /24 after your IP address this is your NETMASK value.  The standard values are /8 /16 /24 /32 which translate to 255.0.0.0, 255.255.0.0, 255.255.255.0 and 255.255.255.255.

In the following example the IP address is 192.168.122.142/24 but your value may be different.

![ip addr show](ip-addr-show.png)

Next locate your default gateway value using `ip route` your are looking for the value defined for "default".  This is also your DNS server.

![default gateway](gateway.png)

Finally locate your DNS server IP address by checking the value stored in your `/etc/resolv.conf` file as shown the following screenshot.

![resolv.conf](etc-resolv.png)

Based on my example configuration files my final configuration will look like the example below.

>**NOTE:** Do NOT copy my IPADDR, NETMASK, GATEWAY and DNS1 settings unless they match your values.  It is unlikely that you have all the same settings.

![static ip](ifcfg-static.png)

Finally we want to restart the networking service using **nmcli** and confirm the health of the Network Manager service using **systemctl**.

```bash
sudo nmcli networking off; sudo nmcli networking on
sudo systemctl status NetworkManager
```

![nmcli example](nmcli-on.png)

 Once the updates are complete and the services have been restarted establish a second SSH connection to your Development host to confirm everything is working as expected.

___

## lab complete

___
