# Setting up SSH keys
When working with Linux or UNIX system it is common to augment or replace password based authentication with certificate based authentication.

The user needs to setup an SSH key pair.  This can be achieved on Windows using PuttyGen one of the Putty tools available here:  [Putty downloads](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).

On Linux or UNIX systems these keys can be generated using **ssh-keygen** which is typically installed by default.

### **Demo**
The following example shows a Windows client connecting to a Linux host using Putty tools.  This includes how to setup the destination folder structure, authorized keys file, and login using the associated private key.
![ dev host ssh login ](putty-devhost-login-with-public-key.gif)

Loading an SSH private key into the Putty agent on Windows
![Putty load private key](putty-agent-load-key.gif)

