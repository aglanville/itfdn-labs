# In Class lab: Introduction to the AWS Console UI

>Objective: Confirm everyone can login to the AWS console, navigate around and understands how to start the lab.

>Note: In this case we are not working with either the Development or Production servers but rather getting familiar with a new platform the Amazon Web Service (AWS)

## Start the lab

Login to Canvas locate the Modules and then scroll to the bottom and look for a new module labeled **AWS Lab**. Here Click on the lab **AWS Lab**.  This should launch a new tab with a button labeled **Load AWS Lab in a new window** click on this button.

![AWS lab load](load-aws-lab.png)

Next we are need to click **Start Lab** and then click **AWS**.  Don't click **End Lab** as this will remove most of the objects in your AWS profile that you have created unless that is what you want to do.

![lStart AWS lab](aws-console-login.png)

Now that you are logged into the AWS console User Interface (UI).  Take a look around, find the EC2 section, we will be working here later in the quarter.  Find the S3 section this is the most common online storage solution provided by Amazon.

![AWS console](aws-console.png)

Finally take notice of what region you are logged into and what regions are available.  AWS has data centers around the globe, each with potentially different cost basis but all with excellent local redundancy and failover.  We will talk about this more later in the quarter but it is very common for users to create objects and lose them in regions per se'.

![Your AWS Region](aws-region.png)

Review this link for more details on Regions if you are interested.

[AWS Regions](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)

___

### Lab complete

___