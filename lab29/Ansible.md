# In class lab: Introduction to Ansible

>**Objective**: install ansible.  Run Ansible ah-hoc commands and create playbooks.

>**Note**: It is assumed that you are doing this lab on your **development** server and not your Production server. However in this lab you will be interacting with your **production** and **AWS** host from your **Development** server.

In this lab we are going to use your development host as the Ansible control node and run commands against our production and AWS servers.  We will create a dedicated ansible automation user (Spock) for this purpose, this user should have a dedicated key pair.

Ansible is going to authenticate using SSH keys.  You will have the best experience if you have the SSH key loaded for the automation user Spock that we will create later in this lab.

![ansible lab overview](ansible-management-network.drawio.png)

To setup the new account for AWS you will probably need to have your **lab25-key-pair** ssh key loaded. Remember you can have multiple keys loaded at the same time.  In some cases you may have two keys, this means you will need to load both keys into the ssh-agent on your dev host. All the keys should be copied to your dev host under your .ssh directory.  You also need to be sure the permissions for this file are set to 600 and that it is owned by your user.

However, how many keys you use is your choice.  You can setup unique keys for each host, or you can use the same keys for all hosts or you can mix it up.  What does matter is that you must have the private key available on your dev host that corresponds to the public key you placed into the authorized_keys file in your AWS and Production Linux servers.  

In order to login to your AWS host you may want to have your lab 25 key loaded on your Development host. Below are some notes on this process.

>__Note:__ I also have windows video and a mac based video that goes over [SSH authentication using a keys](https://canvas.uw.edu/courses/1545666/pages/how-to-login-to-your-linux-host-using-ssh-keys-windows-and-mac-users?module_item_id=14980616).

If you have a **Mac** or **Linux** host all you need to do is copy the lab25-key-pair file to your Development server.  You could do this using scp (see lab8 for details on scp) or manually copy and paste the contents of the lab25-key-pair file to the Development server.

If you have **Windows** computer and you are using Putty then you have a couple more steps.  First open **PuttyGen** and load your **lab25-key-pair.ppk** file.  Next Click on Conversions and then select **Export OpenSSH key** from the menu.  Save your file but change the extension to pem so that you don't write over your existing ppk file.  Now open the newly created **lab25-key-pair.pem **with notepad or similar text editor tool.  Copy the contents to your Development server into a file called **lab25-key-pair.pem**.

![export key](export-openssh-key.png)

Now that you have your lab25 private key file (lab25-key-pair.pem) on your Development server it should be in the .ssh folder under your home directory.  Now you can follow these note.  You must change the permissions of the file as OpenSSH does not like a private key file with loose permissions.  

>**Question**: Who can READ and WRITE to the lab25-key-pair.pem file after you run the chmod command on the file?

```bash
cd ~/.ssh
chmod 600 lab25-key-pair.pem
chown ${USER}:${USER} lab25-key-pair.pem
ssh-add ./lab25-key-pair.pem
ssh-add -l
```

If you receive an error like "**Could not open a connection to your authentication agent**" you may need to start the ssh agent.  As we have covered many times I prefer to have these lines added to my .bashrc so that the agent and keys are loaded at sign-on each time. If you want more details around these process see either the video posted in the supplementary content section or lab7.

```bash
eval $(ssh-agent) 
ssh-add ./lab25-lab-pair.pem
ssh-add -l
```

![ssh agent](ssh-agent.png)

>__Note:__ If you want to use the **lab25-lab-pair** private key to login to your Production and AWS host you might be wondering where can you get the corresponding public key.  There are a few answers to this but if you have the private key on your Development server you can extract the OpenSSL formatted public key from the private key using `ssh-keygen -y -f lab25-key-pair.pem > lab25-public-key.pem`.

## DNF install Ansible

We can install ansible using the EPEL repository but in this case we are going to install ansible using the official Red Hat build.  This means enabling a couple of the Red Hat repositories of which you will notice there are many that are not enabled by default.  If you see [epel under the repository column](ansible-install-epel.png) when installing then the install command failed.

>__Note:__ Enabling the Red Hat repositories can take a minute or longer as it validates your registration during the process.

```bash
cd /etc/yum.repos.d/
grep "enabled = 0" redhat.repo | wc -l
sudo subscription-manager repos --enable satellite-tools-6.6-for-rhel-8-x86_64-rpms
sudo subscription-manager repos --enable ansible-2.9-for-rhel-8-x86_64-rpms
grep "enabled = 0" redhat.repo | wc -l
grep "enabled = 1" redhat.repo | wc -l
sudo dnf install --disablerepo=epel ansible
ansible --version
cd
```

![redhat ansible install](ansible-install-redhat.png)

The Ansible version output also includes details about the location of primary configuration file and other details.

## Account creation

We have gone over how to create users now multiple times.  You need to create a user that has full sudo rights without a password requirement.  This means you have to update your sudoers file and define this user as not requiring a password.  You also want to create a key pair for this user and copy the corresponding public key to the remote hosts.  

Now decide if you are going to use your existing SSH keys or create a new pair for this user.  In this example I am going to create a new pair for the user **spock**.  Spock finds manual processes [highly illogical](https://www.youtube.com/watch?v=mdf25VY8RYA) but he loves automation tools, so he loves Ansible.  The ssh-key files for this user in my example are called **id_rsa_ansible_user**.

```bash
ssh-keygen
ls -la ~/.ssh/id_rsa_ansible_user*
```

![ssh-keygen](ssh-keygen-ansible.png)

Confirm password based authentication is disabled on your hosts.  This needs to be completed on your prod and AWS host.

>**Note**: The AWS host should have password based authentication disabled by default

```bash
sudo grep PasswordAuthentication /etc/ssh/sshd_config
```

If you find PasswordAuthentication is set to yes, change it to no and restart the sshd service.

>**Question**: Why doesn't sed also match and change the line that starts with **#PasswordAuthentication**?

```bash
sudo sed -i 's/^PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sudo grep PasswordAuthentication /etc/ssh/sshd_config
sudo systemctl restart sshd
```

Create ansible automation user **spock** on the remote AWS and Prod servers.

Login to your remote servers and run the following commands, make sure the password is complex.  Password based authentication should be disabled on your hosts at this time.

```bash
sudo useradd -c "Leonard Nimoy" -G wheel spock
sudo passwd spock
```

Next configure sudo to allow the user spock to run sudo without a password.  Do this by adding the following line to the sudoers file.  Use **visudo** to make this update.

```bash
sudo visudo
```

Now add the following lines just the user **spock** to the sudoers file. However, because we put spock in the wheel group he will be defined in the sudoers file twice.  This is just so you can see both methods of granting sudo file access.  

>Question: How is the **rocky** user granted sudo access, is it based on membership in a group or is the rocky user explicitly defined in the sudoers file like we have in this example for **spock**?

```text
## Same thing without a password
spock  ALL=(ALL)       NOPASSWD: ALL
```

Now we need to add the **id_rsa_ansible_user.pub** key file to the **authorized_keys** file for the user spock.  This means we need to create the .ssh folder and the **authorized_keys** file. Remember the **authorized_keys** file does NOT exist by default.

```bash
sudo su - spock
mkdir .ssh
chmod 700 .ssh
vi .ssh/authorized_keys
chmod 644 .ssh/authorized_keys
```

Now return to your development host and load the key for spock into your ssh-agent and test your login as spock.

```bash
eval $(ssh-agent)
ssh-add ~/.ssh/id_rsa_ansible_user
ssh spock@<YOUR AWS IP ADDRESS>
exit
```

![ssh as rocky](ssh-rocky-aws.png)

If you were able to login to your **AWS server** and your **Production server** from your **Development server** as **spock** then you are ready to move to the next section. Remember the goal here is to manage your Production and AWS linux servers from your **Development server**. Now go back to your **Development** server to setup your Ansible configuration.

## Ansible configuration

Each time Ansible is run it will read the configuration file.  Ansible searches for the default configuration file in the following locations.

1. Checks for the existence of an **ANSIBLE_CONFIG** environment variable
1. searches the current directory ./ansible.cfg
1. searches the home directory for the user initiating the request ~/ansible.cfg and ~/.ansible.cfg
1. /etc/ansible/ansible.cfg

A best practice is to only include the override values in the ansible.cfg file. However, for clarity we will include settings even when they are the defaults.

Now we are going to create a folder in our home directory called **ansible-demo**.  

```bash
mkdir ~/ansible-demo
cd ~/ansible-demo
``` 

Inside this folder create a file called **ansible.cfg** in this file add the following lines.  This line is simply telling Ansible to expect to find the inventory in the current working directory and that our default user is **spock**.

```text
echo "[defaults]
inventory = ./inventory

remote_user = spock" > ansible.cfg
```

As in our example ansible.cfg file one of the first values in the ansible.cfg file is often a path the inventory file.  The inventory file defines the details for all the hosts that will be managed by Ansible. The format for this file can be INI or YAML. We are going to use the [INI format](https://en.wikipedia.org/wiki/INI_file).  The sections are enclosed in square brackets [ ]. There is a default group referred to as **all** that includes all objects.  Categories can be defined with any name.  In this lab we will define a group called **private_cloud** and another one called **public_cloud**. 

>__Notes:__ Check the troubleshooting section at the end of this lab if you see errors or warning running ansible

Now create a file called **inventory** in ~/ansible-demo folder but update the ip address details to reflect **YOUR** Production and AWS Linux host details.

```bash
echo "[all]
140.142.159.XXX
3.236.58.245

[private_cloud]
140.142.159.XXX

[public_cloud]
3.24.58.XXX " > inventory
```

![ sample inventory file](inventory_sample.png)

Next we are going to try and run a simple ad-hoc Ansible command using this inventory file. The **-m** indicates module and in this case we are using the ping module for ansible. These commands need to be run from within the **ansible-demo** folder as that is where we have our ansible.cfg and inventory file.

>__Note:__ You may need to update your security group settings for your AWS host to allow ICMP (ping) IPv4 traffic.  

```bash
ansible all -m ping
ansible private_cloud -m ping
ansible public_cloud -m ping
```

![Ansible ping](ansible-ping.png)

Next we can update your inventory file using the ansible key word **ansible_host**.  This allows us to use short names for our hosts if desired.
There are many other key word options available such as [ansible_port or ansible_user](https://docs.ansible.com/ansible/2.7/user_guide/intro_inventory.html#list-of-behavioral-inventory-parameters).

```bash
[all]
myuw ansible_host=140.142.159.187
myaws ansible_host=34.254.7.34

[private_cloud]
myuw ansible_host=140.142.159.187

[public_cloud]
myaws ansible_host=34.254.7.34
```

Now we can run commands using these short names.

```bash
ansible myuw -m ping
ansible myaws -m ping
```

## Running ad-hoc commands

Now that we have a valid inventory file and ansible.cfg file we can run through a few more ad-hoc examples.  

Generally when running ansible commands and playbooks you don't want to enter a password.  This means the account on the remote system should be configured to allow sudo rights without a password.  But, then you probably want a dedicated ansible user with more controlled access. In this class we have always just given ourselves full root rights with sudo but that is not advised for dedicated service accounts such as one you might use with Ansible. We are not doing that in this lab but you should be aware of this common standard.

Sometimes you may have some hosts that force you to enter a password and others do not, this can be set on a server by server basis.  

It is assumed that you have an nginx web server running on your prod and AWS linux hosts.  These commands will test that server and restart it remotely. The "-b" option below tells ansible to **become** root, this is done using sudo by default.  

>Note: we are using new modules here, dnf and service, previously we used the ping module.

Here we are trying to check the status of the nginx web server on myuw, stop it check it and then start it and check it again.  You might want to start a second terminal to you production server to confirm the status locally.

```bash
ansible myuw -m service -a "name=nginx" --check -b | grep ActiveState
ansible myuw -m service -a "state=stopped name=nginx" -b
ansible myuw -m service -a "name=nginx" --check -b | grep ActiveState
ansible myuw -m service -a "state=started name=nginx" -b
ansible myuw -m service -a "name=nginx" --check -b | grep ActiveState
```

Next we are going to install [Redis](https://redis.io/) a simple database onto our systems and start it using Ansible.

```bash
ansible myuw -m service -a "name=redis" --check -b
ansible myaws -m service -a "name=redis" --check -b
ansible myaws -m dnf -a "name=redis state=present" -b
ansible myuw -m dnf -a "name=redis state=present" -b
ansible myaws -m service -a "state=started name=redis" -b
ansible myuw -m service -a "state=started name=redis" -b
ansible all -m service -a "state=stopped name=redis" -b
ansible myuw -m service -a "name=redis" --check -b
ansible myaws -m service -a "name=redis" --check -b
```

What is happing in the line that says ansible "all" .... We are starting the redis service on all hosts in the inventory file.

In this example we are using another new module **copy** and this time we using the **-a** to provide arguments to the copy module.  Does the color of your output from ansible change the second time you run the command?  Ansible is idempotent meaning it can be run multiple times and it will not change the outcome from the desired state.  The second time it ran the output should be green, indicating all is fine on the remote host.

We are adding some data to the end of the local authorized_keys file to test the diff function with Ansible.

```bash
ansible myaws -m copy -a "src=~/.ssh/authorized_keys dest=~/" 
ansible myuw -m copy -a "src=~/.ssh/authorized_keys dest=~/" 
ansible myaws -m copy -a "src=~/.ssh/authorized_keys dest=~/" --check
ansible myuw -m copy -a "src=~/.ssh/authorized_keys dest=~/" --check
echo "extra line for testing" >>  ~/.ssh/authorized_keys
ansible myaws -m copy -a "src=~/.ssh/authorized_keys dest=~/" --diff
```

In this next line we are moving out of our **ansible_demo** folder and using the **-i** to create an inventory file on the fly.  You need to change XXX to your prod server value.  Then we are running the echo command on your prod host using ansible.  Using the **-i** means we can define our inventory file on the fly in real time.

```bash
cd
ansible all -i ulc-XXX.ulcert.uw.edu, -m shell -a '/bin/echo hello $USER!'
```

![Using the -i to define the inventory in real time](ansible-no-inventory-file.png)

It is a best practice to use modules to solve your Ansible needs. However, sometimes a module is not an option, or sometimes the remote host does not support Python or have Python installed which is required for modules to work.  In these situations you might try running commands directly on the host like this shown below. Now return to the **ansible-demo** folder and work through the following examples.

>**Note**: Ansible commands run a single function on the remote host and have no knowledge of the env variables.

>**Question**: Why don't we use the -b option with the /usr/bin/**who** command but we have to use it with the **chronyc** command?

```bash
cd ~/ansible-demo/
ansible myaws -m command -a /usr/bin/who 
ansible myaws -m command -a "/usr/bin/chronyc makestep" -b
```

If the makestep command fails with no such File or directory that means you do not have chrony installed.  Look at the previous examples and install and start chrony now.

```bash
ansible myuw -m dnf -a "name=chrony state=present" -b
```

Another option is to spawn a shell on the remote system and run commands in that environment.  The Ansible shell is aware of environment variables.

```bash
ansible myaws -m shell -a '/bin/echo "hello $USER!, I see you have logged into $HOSTNAME"'
```

The last option is to run **raw** commands on the remote host, this might be useful if the remote system does not or cannot have python installed.  This is often the case with network hardware such as switches and routers.

### Running playbooks

We already have an Inventory file and an ansible.cfg file.  Now we can create an Ansible playbook.  If each ah-hoc command is considered a play then a playbook is a collection of one or more plays.  In this example we are going to install a new user. 

Playbooks are written in YAML [YAML Ain't Markup Language](https://en.wikipedia.org/wiki/YAML).  Yaml files start with 3 dashes **---** on the first line. YAML files expect all indentation to be based on spaces.  Just like Python YAML considers lines at the same indentation level as part of the same block.  Child blocks should all be indented the same amount inside the parent block. File extensions do not matter in Linux but traditionally YAML files end in either yml or yaml which is good visual cue to you that you are looking at a YAML file.

To support YAML indention you might update or create a `.vimrc` file in your home directory add the following lines. Side note I remember these three settings as I think I need to **SET** my vim configuration Shiftwidth, Expandtab and, Tabstop, just in case that helps you.

```bash
set shiftwidth=2
set expandtab
set tabstop=2
```

Now create a new file called newuser.yaml

```yaml
---
- name: add user
  hosts: myaws
  become: yes
  tasks:
    - name: create user newtest140 exists
      user:
        name: newtest140
        state: present
```

You can check the syntax of your YAML file using **--syntax-check** and you can try a dry run using **-C**.  The command to run a playbook is **ansible-playbook**.

```bash
ansible-playbook --syntax-check newuser.yaml
ansible-playbook -C newuser.yaml
```

Now you can run the playbook and create the user as follows.

```bash
ansible-playbook newuser.yaml
```

Now check to see if your new users exist on your AWS host.  You can sign-on to the AWS host and check directly or run some simple SSH commands to check

```bash
ssh {INSERT YOUR AWS IP ADDRESS HERE} 'grep newtest140 /etc/passwd'
ssh {INSERT YOUR AWS IP ADDRESS HERE} 'id newtest140'
```

![ssh command example](ssh-command.png)

Now create playbook called stop-nginx.yaml and run this playbook `ansible-playbook stop-nginx.yaml`.

```yaml
---
- hosts: all
  tasks:
    - name: stop nginx
      service:
          name: nginx
          state: stopped
      become: yes
```

Now create another playbook called **start-nginx.yaml** and run this playbook `ansible-playbook start-nginx.yaml`.

```yaml
---
- hosts: all
  tasks:
    - name: start nginx
      service:
          name: nginx
          state: started
      become: yes
```

>__Note:__ Be sure to your Nginx instance is running after working through this lab.

## Ansible documentation

Ansible includes documentation for the modules and this is a good place to check for additional options and functions.  The command for Ansible documentation is `ansible-doc`.  Using the **-l** option will list all known modules.  You can pipe this to grep for your key search terms.  Then once you have found the module use the module name as the primary argument for ansible-docs.  Take a moment not to review the docs for the ping and yum modules.

>Note: There may be along delay when trying to list all module docs.

```bash
ansible-doc ping
ansible-doc yum
ansible-doc -l
```

For additional notes on Ansible playbooks see: [introduction to Ansible playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html) by Ansible

---

## Optional ansible install in a Docker container

Here you can create a docker container and install Ansible into another similar Linux OS to Red Hat and Rocky.  This one is called [Alma Linux](https://almalinux.org/).

we want make a new directory under our dockerbuilds for this config.

```bash
mkdir ~/dockerbuilds/almansible
cd ~/dockerbuilds/almansible
```

Now we can create our docker file using the following sample.  This process build process may take a few minutes as we are building a pretty big container image.

```bash
# using another similar linux version from Alma Linux
FROM almalinux:8.10-minimal-20240528
# update to reflect your email address and name
LABEL maintainer="Angus aglanville@gmail.com"
LABEL "description"="Container for Ansible built for UW PCE Class Lab"
LABEL version="0.2"
# we need epel-release to install python3.4
# using microdnf in place of yum or dnf
RUN microdnf -y install epel-release
# handy to have locate installed.
RUN microdnf -y install mlocate
# installing python 3.9 as Ansible prefers something newer than version 3.8 at least
RUN microdnf install python3.9 -y
RUN microdnf install python3-pip -y
RUN pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org --upgrade pip
# updating the PATH for the root user as many of the python tools are installed under root/.local/bin
RUN sed -i 's/PATH=$PATH:$HOME\/bin/PATH=$PATH:$HOME\/bin:\/root\/.local\/bin/' /root/.bash_profile
# updating the bashrc file also just in case.
RUN echo "PATH=$PATH:$HOME/bin:/root/.local/bin" >> /root/.bashrc
# install ansible using pip
RUN pip install ansible
# update the locate index with all the installed software.
RUN updatedb
```

Now we can; build, run, login to our new Alma Linux Ansible container.

```bash
docker build -t alma-ansible .
docker run -td --name myansible alma-ansible
docker exec -ti myansible bash
ansible --version
```

## Troubleshooting tips

Q. When I run a playbook it connects but then seems to just hang, after a few minutes it errors out.  

A. Maybe your user is required to enter a password when running sudo commands, if this is the case then try adding **-K** to the ansible-playbook command line arguments.  Then when you run the playbook it should prompt you for the password.

Q. I have a different username on the remote host than I am using on my Ansible host, how can I define a username for a host?  

A. Add **ansible_user=\<username>** to your inventory file for every host that needs a specific username.

Q. When I run adhoc commands it seems to hang and I see a message that the host is **unreachable**. 

A. Make sure you have your SSH key loaded into the agent and the public key copied to the remove host **authorized_keys** file.

![host unreachable](ansible-unreachable.png)

Q. I see warnings about no inventory file?

A. Ansible will generate errors if you are running it in a location where it can not find the required files, such as inventory and ansible.cfg. Make sure you are running the command from the ~/ansible-demo folder where your inventory file exists.

![ansible file copy error](ansible-file-missing.png)

Q. When trying to run ah-hoc commands on a remote host the request fails due to a password requirement.

A. You probably have sudo setup to require a password.  You can remove the password requirement by updating your sudoers file `sudo visudo` on the remote host. Alternatively you can append an uppercase **-K** to your command to prompt for the sudo password.

![prompt for sudo password](ansible-sudo-passwd-prompt.png)

Q. When checking the state of package I see an error that the package was not installed.

A. This probably means the package is not installed, try to install it now with Ansible by changing the state from **present** to **installed**.

![package missing](ansible-pkg-not-installed.png)

Q. When I run my Ansible scripts I see warnings about the python version.

![ Python Version ](python-warning.png) 

A. Try to add the preferred version details to your ansible.cfg file as shown in the screenshot.  You can find the default python version on your linux host using `which python`.

![ define default python](ansible-python-default.png)

Q. Ansible returns a warning about SFTP and SCP how can I clear this?

A. On CentOS7 hosts you can update your sshd_config file and change the stfp Subsystem value as shown in the screenshot.  When done restart SSHD using `sudo systemctl restart sshd`.

![ SSH SFTP subsytem ](sftp-subsystem.png)

___

## Lab Complete

___