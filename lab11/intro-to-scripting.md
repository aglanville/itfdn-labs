# In Class lab: Introduction to Scripting

>Objective: Create your shell scripting template and work through a couple scripts.

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

First we are going to create a directory for your scripts. In the example we are using "~".  This is a shortcut for the current users home directory.

```shell
mkdir -p ~/scripts/shell
cd ~/scripts/shell
```

In this directory create a file called template.sh.  This is the file I would like you to use for all your scripts as a starting point.  As you see code snippets you like consider adding them to your template.sh file.

```bash
#!/bin/bash
# Title: <script_name>.sh
# Date: 00/00/2022
# Author:
# Purpose: Default script template
# Update:
```

Now either copy your script template to `hello.sh` or create a file called `hello.sh` and **read** in your script file using the `:r <filename>` command.  Notice how there is a blank line at the top of your file when you read in the template file like this?  You must remove this blank line.  The Vim command for this is `<SHIFT key> + dd`   this should delete the entire line.

![Vim read in content](vi-import-content.gif)

This script will accept one argument, the users name.  The script will then respond with "hello {username}, pleased to meet you."

Here is what your script should look like.

```bash
#!/bin/bash
# Title: hello.sh
# Date: 00/00/2022
# Author: Angus
# Purpose: Greet the user by name based on a single argument.
# Update:

# first argument is the name, we hope.
name=$1
# say hello
echo "Hello $name, pleased to meet you."
```

Save this file, change the permissions to read and execute and then test the script.

![hello example](hello.png)

Now lets add a log file to this script to capture the arguments passed to the script.  We are going to create a file in the same location as th **hello.sh** script called **hello.log.**  This log file will contain any arguments we pass into the script which in this case should be our name.  To to this simply add the next 3 lines below the last line of your `hello.sh` script.

__Question:__ What is capturing the arguments in this script?

```bash
mylog="hello.log"
echo "adding args to the log"
echo "$@">>$mylog
```

Now for our second script we will work with an if/then conditional as discussed in the lecture. This script should be called **greetings.sh** which should also be based on a copy of the **template.sh** file. In this example we will add a conditional such that if the user enters "night" the script returns "Good evening" but otherwise replies with "Good day".

If below is the sample code for this script.  Add this code to the bottom of the new file you just created called greetings.sh.

```bash
if [ “$1” = “night” ] # testing for the string "night" lowercase only if we match
 then 
  echo "Good evening"
  exit 0
fi
# default answer if no argument found
echo "Good Day"
```

>__Stretch challenge:__ Add one more if/then block that will return script details if the user enters help. _hint_ you could just add one more `if` block that looks for the word __help__ instead of __night__.

![greetings help](script-greetings-help.jpg)

## Adding content to your in class Git repository

Copy your scripts from /home/${USER}/git/gto /home/${USER}/git/gitlab/itfdn-140-inclass-$USER 
Next add these to git, add a commit message and finally push them to your repo.

Review lab 9 if you want more tips [Git lab](../lab10/install-git.html)

Here is an example pushing just your `template.sh` file.

```
git add template.sh
git commit "Shell Script template" template.sh
git push origin master
git status
git log 
```

___

## Lab complete

___