# Working with ths ss command to monitor network traffic

Netstat a common tool in previous versions of Linux is being replaced by ss.

Display socket summary details
```bash
ss -s
```
Display IPv4 sockets and details
```bash
ss -4
ss -4nlp
```
Display all tcp and udp socket details
```bash
ss -at
ss -au
```
Display connections based on src or dst addr with optional port such as ssh (22)
```bash
ss src XXX.XXX.XXX.XXX
ss dst XXX.XXX.XXX.XXX
ss src XXX.XXX.XXX.XXX:22

