# Working with `xargs`.

You can pass arguments to xargs based on the values found using -I.  
you define the variable using % and then reference it using % later.

Here we are using 
ps -ef | grep ssh-agent | awk '{print $2}' | xargs -I  % ps -eo etime %

ps show elapsed time

## ssh-agent process clean up

print out the users sorted based on ssh-agent process.
ps -ef | grep ssh-agent | awk 'BEGIN{OFS="\t\t"}{print $1, $2}' | sort -t$'\t' -k1

ps -ef | grep ssh-agent | awk '{print $2}' | head -n 50 | xargs -t -I % sudo kill -9 %

kill process by user
ps -ef | grep ssh-agent | awk 'BEGIN{OFS="\t"}{print $1, $2}' | sort -t$'\t' -k1 | grep angus | awk '{print $2}' | xargs -t -I % sudo kill -9 %

add to bash_logout

```
we have something like for most users in the .bashrc file
# start ssh-agent
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval $(ssh-agent)
    ssh-add ~/.ssh/id_rsa_installer
fi
```

To clear up sessions on exit
.bash_logout

```
# clean up ssh-agent on logout
# -n if the SSH agent string value is NOT zero then...
if [ -n "$SSH_AGENT_PID" ] ; then
        eval $(ssh-agent -k)
fi
```