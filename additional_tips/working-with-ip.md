# working with ip to replace route and ifconfig

Working with ip in place of route is required as route is being deprecated.

Dispaly route table with details and cache
```bash
ip route show
ip route show table local
ip route show cache XXX.XXX.XXX.XXX
```
Add routes and default routes
```bash
ip route add XXX.XXX.XXX.XXX/16 via XXX.XXX.XXX.XXX
ip route add default via XXX.XXX.XXX.XXX
```
Display arp details
```bash
ip neigh
ip -s neigh
```
Display IP interface details, enable or disable interface
```bash
ip addr
ip link set eth0 down
ip link set eht0 up
```