# Firewalld and firewall-cmd cheat sheet 
### firewalld commands
```bash
sudo firewalld-cmd --state   # confirm the status for firewalld
sudo firewall-cmd --reload   # After applying a new permanent rule reload the config.
sudo systemctl enable firewalld  # configure the serivce to auto start
sudo systemctl disable firewalld  # disable auto start for the service
```

### Stop and Start service:
```bash
sudo systemctl status firewalld  # Another method to check the firewalld status
sudo systemctl restart firewalld # Restart firewalld
sudo systemctl stop firewalld  # Stop firewalld
sudo systemctl start firewalld  # Start firewalld
```

### Get zone details
```bash
firewall-cmd --get-default-zone # find the default zone
firewall-cmd --get-active-zone # dispaly list of all active zones
firewall-cmd --list-all
```

### how to add rules for https traffic
```bash
firewall-cmd --get-default-zone
firewall-cmd --zone=public --permanent --add-service=https
firewall-cmd --reload 
firewall-cmd --zone=public --list-all
```

### how to add rules for port 53 tcp traffic
```bash
firewall-cmd --get-default-zone
firewall-cmd --zone=public --permanent --add-port=53/tcp
firewall-cmd --reload 
firewall-cmd --zone=public --list-all
```