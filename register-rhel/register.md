# Activate the RHEL license

Redhat now requires registering all systems since the release of RHEL 8.  In order to complete this you will need to create an account with [Red Hat.com](https://www.redhat.com/en) and link that account to the [developer.redhat.com site](https://developers.redhat.com/).  You can find additional notes on setting up your redhat account here: https://www.ulcert.uw.edu/itfdn-labs/lab1/rhel8/redhat-registration.html 

## Register your system

The username and password should be your own RedHat credentials, the ones you probably created in class this week.  This time we are going to register our Production servers from the command line. The single tics around the password are required if you have special characters in the value.

```bash
sudo subscription-manager register --username=YOUR_RHEL_USERNAME --password='YOUR_REHL_PASSWORD'
```

![Activate RHEL license](redhat-registration.png)

Now we want to refresh the subscription service locally and setup the server to auto attach

```bash
sudo subscription-manager status
sudo subscription-manager refresh
sudo subscription-manager attach --auto
sudo subscription-manager status
```

## Unregister your system

To unregister a node use **unregister**.  Use **status** to check the current license mode of your node.

```bash
sudo subscription-manager unregister 
sudo subscription-manager status
```

___
End of document
___