# In class lab: scripting loops and options

> Objective: Work with scripting loops and options. As with most labs this should be completed on your development host. 

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

I expect that you have a template.sh file on your dev host under /home/user/scripts. This file might look something like this by now as maybe you have added a sample if/fi statement.

```
#!/bin/bash
# Title: <script_name>.sh
# Date: 00/00/2018
# Author:
# Purpose: Default script template
# Update

## sample help/usage statement
#if [ "$1" = "-h" ]
#  then
#    echo "What can I help you with?"
#    echo "usage: $0 [ -x | -y | -z ] 
#    exit 0
#fi

```

## Working with **while** loops

The first task is to write a while script called "date.sh" that enters a loop where it will print:

` "hello $user today is $date" `
 
Every 2 seconds for 10 seconds. In the sample we have set an index number to start the loop and then exit after 10 seconds with an exit message.

This script is close but you will have to change/update a few things. You can see that we have the date and user variable defined so your task is just to review the loop and figure out where to enter those value to result in the desired output.  

>Hint: look at the line inside the while loop that starts with $name and remember to use double quotes to surround your input.

```
<insert template.sh here> 

echo "Hello, enter your name: "
read name
number=0
time=`date`
while [ "$number" -le 10 ]
    do
    sleep 2
    number=$(( number+2 ))
    echo $name
done
```

Here is what the running script output might look like:

![ date script](date.sh.jpg)

## Working with **for** loops.

We can use for loops without setting an initial index as we did with the while loop. Also consider that we can run this for loop at the command line also.

```
#!/bin/bash
#...

for letter in a b c
do
    echo "I found letter $letter"
done
```

![for loop](forloop.png)

Now create three files called file1.txt file2.txt and file3.txt enter different text into each file.

Run the following for loop against these files. Change echo to cat and explain the difference to someone else in class.

```
for file in file{1..3}.txt
do
    echo $file
    done
```

>Note: Just like the for loop this can be run from the command line if desired. 

Now add debug statements to both scripts and run them again. This can be done using -x and -v and then both options -xv just after the SHEBANG line `#!/bin/bash -xv`.  Alternatively you can enable debug within a script using the set functions as discussed in the lecture.  

## Working with options

Test the following script

```
if [ "$1" = "-a" ]
then
        option=TRUE
	     shift
else
        option=FALSE
fi
echo "value for option is: $option"
```

Now lets look at a more robust GETOPTS enable script Copy this script to your development host. This script needs some updating but should provide a starting point. The script expects to receive up to 4 options, 2 of which are defined and 2 which need to be added. It may also benefit from some error and input checking.

If you read the notes in the script you see that this script prints out user details. The script expects a username input. The script is looking for you to add functions for -s (shell) and -g (group).

```
# !/bin/sh
# add shell template here
#
# Maybe you should test for any arguments here?
##################################################################
# GETOPTS example
# Here we accept up to 3 options -u user -n name -s shell -g group membership
# Add getopts options for s and g.
# consider, adding one more pattern to compare.
#
while getopts ":u:nsg" option;
 do
 case $option in
 u) user=$OPTARG 
    chage -l $user | grep Last;;
 n) name=Y ;;

# Missing -s and -g here

 :) echo "option -$OPTARG needs a username"
 exit 1 ;;
 *) echo "invalid option -$OPTARG" ;;
 esac
done
# If $name set to Y output full username field value from /etc/passwd.
if [ "$name" = "Y" ]
 then
 # user full name
 echo full name: $(grep $user /etc/passwd | cut -d: -f5)
fi
# now add functionality to support -s and -g
# should we explicitly exit the script? should we set the exit code?
exit
```

End of lab.