# In class lab: Web server install and configuration

> Objective: Install and configure a basic http web server.

> Note: It is assumed that you are doing this lab on your **development** host and not your **production** server.

## Install Nginx web server

Now we are going to install and configure the nginx web server.  Prior to RHEL 8 Nginx was not included in the default RPM repositories. Nginx was often installed using the [EPEL (Extra Packages for Enterprise Linux)](https://www.redhat.com/en/blog/whats-epel-and-how-do-i-use-it) repository.

When we `enable` a service with `systemctl` it means we want that service to auto start when the system is restarted.

>__Question:__ What is the "-y" option doing for you when installing `nginx`?

>__Question:__ How does your linux system now where to find 127.0.0.1?  What file tracks host names locally?

```bash
sudo dnf install nginx -y 
sudo systemctl enable nginx 
sudo systemctl start nginx 
sudo systemctl status nginx 
curl http://localhost
curl http://127.0.0.1
```

![curl localhost](curl-localhost.png)

Now we need to review the document root and the default Nginx configuration file.  We are going to backup the original files and later create new files with simple content that we can build on during the course.  

```bash
cd /usr/share/nginx/html
ls
cat index.html | more
ls -l index.html
sudo mv index.html index.html.orig
cd /etc/nginx
ls nginx.conf*
cat nginx.conf | more
sudo mv nginx.conf nginx.conf.orig
ls -la nginx.conf*
```

Here is what we expect to see for those files now in their respective directories.

![nginx conf list](nginx-conf.png)

Now we need to create a new nginx.conf file.  This new configuration file will provide the base for us to build from throughout the course.  You will need to create this file using your favorite command line editor which may be Vim (`sudo vi /etc/nginx/nginx.conf`) for some of you. Try to add notes explaining the values you understand, I have added one note to the first line as an example.  You don't need to note each line but add the details you know. Remember to start notes with a hash (#) symbol.  

```bash
user nginx; # user that the web server will run as on this host.
worker_processes 1;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    access_log  /var/log/nginx/access.log;

    server {
        listen       80;
        server_name  example.com;
        root         /usr/share/nginx/html;

        location / {
        }
    }
}
```

Once we have created the new nginx.conf file we can test the nginx server to confirm there are no configuration issues.  What is the difference between the -t and -T options?

```bash
sudo nginx -t
sudo nginx -T
```

Now we are going to create a new index.html for our site.  This file needs to exist in the /usr/share/nginx/html directory.  When no default file is found the Nginx server will return an error when we try to check the webserver using the just the hostname or ip address.  We are going to test that out now.  Because we moved the default html file earlier our curl command should return an error. However, if we request index.html.orig then we should see the default page returned.

```bash
curl http://localhost
curl http://127.0.0.1
curl http://localhost/index.html.orig
curl http://127.0.0.1/index.html.orig
```

Now we can create a new index.html file, this is the default file for our webserver. We can create the file using Vim from any location on the file system so long as we include the path to the file destination. This might look like `sudo vi /usr/share/nginx/html/index.html`.  However, in the example below I return to the html directory to create the file.  

>**Question**: Do we need to use sudo to create the index.html file?  Does your user have write permissions to the /usr/share/nginx/html folder?

```bash
cd /usr/share/nginx/html
vi index.html
```

Here is a very simple HTML page sample.

```html
<html>
<head>
<title>
IT FDN dev host default page
</title>
<body>
    Default home page for IT FDN 140 demo host.
</body>
</html>
```

Now try to test your new site agin using curl, we do not expect to see a 403 error this time.

```bash
curl http://localhost 
```

## Configure local firewall 

Now we need to update the firewall rule to allow traffic over port 80 (http).

>Note: We have not covered firewalls yet but we will soon.

One note is that we we add firewall rules they are not applied until we reload the configuration.  You should note that the service http does not show until after you reload the rule set. 

>Note: Check if your firewall process is running, start it if required.

![firewalld status](firewalld-status.png)

```
sudo systemctl status firewalld
sudo firewall-cmd --get-active-zones
sudo firewall-cmd --zone=public --list-all
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --zone=public --list-services
sudo firewall-cmd --reload
sudo firewall-cmd --zone=public --list-services
```

Now try to connect to your development host using your laptop browser.  

## Working with DNS (Name Servers)

We are going to install dig via the bind-utils package and then run some bind queries.

```bash
sudo yum install bind-utils
```

Dig can answer questions about domain names and the services advertised for domain.

If we want to know what IP address is associated to the www.ulcert.uw.edu site we can use dig

```bash
dig www.ulcert.uw.edu
```

![dig response](dns-query.png)

Now edit your /etc/resolv.conf file and change the name server value to `140.142.159.151` and run the same query.  Your SERVER value should have changed.

![update resolv](update-resolv.png)

If we want a short answer with just the IP address we use the +short option

```bash
dig +short www.ulcert.uw.edu
```

If we want to know the DNS servers (Name Servers) for the uw.edu we use the ns option

```bash
dig uw.edu ns
```

if we want to know what email servers are available for uw.edu we ask for mx records

```bash
dig uw.edu mx
```

If you want to know the name associated with a given IP address then use a reverse query

```bash
dig -x 140.142.159.151
```

If we want to see how our DNS server also called a Name Server finds the answer for a question we can trace our query

``` bash
dig uw.edu +trace
```

___
## Lab Complete
___