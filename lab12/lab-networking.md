# In class lab: Networking

>**Objective**: explore your local networking configuration and consider how your host is able to resolve host names.

>**Note**: It is assumed that you are doing this lab on your **development** host and not your **production** server.

## Discovery at home

This is an attempt to understand how our Development server is connected to the internet.  We want to find the IP address for development server, the default route for our development server and the public IP address that our internet provider has assigned to us.

These are the primary commands used in the exercise.  You will have to provide your public IP address to the **traceroute** command.

>**Note**: You may have to install **traceroute**

```bash
ip address show
ip route
curl ifconfig.io
traceroute
```

Look at this diagram and try to fill in the questions for your home network now, what path does your development server use to reach the internet.

![home network](./home-network.drawio.png)

## Networking

Next we are going to look at the details provided by the **ip** command.  In these examples we are working on our filtering skills again.  In the last set of commands we are trying to reduce the output from the ip command to just the network interface name.  

>**Question**: What is the option "-4" doing in the ip command examples below?  What command can you use to figure it out? hint you should check the manual...

```bash
ip addr show 
ip addr show | wc -l
ip -4 addr show 
ip -4 addr show | wc -l
```

>**Question**: What is the option "-o" doing in the ip command examples below?  What command can you use to figure it out? hint you should check the manual...

```bash
ip -4 -o addr show 
ip -4 -o addr show | grep 2: 
ip -4 -o addr show | grep 2: | cut -d' ' -f2
```

## Network manager command line interface (nmcli)

The **nmcli** tool will provide local network management and network status details but we need to provide the network interface name. But, you should now know the name of your network interface device from the previous **ip** command examples.  This is the value that we will use with **nmcli**.  In my example below it is **ens192** but I expect that you may have a different value.

>__Note:__ See [Consistent Network Device Naming](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/consistent-network-interface-device-naming_configuring-and-managing-networking) for details on how the interface device names are generated.

```bash
nmcli
nmcli device show ens192 
nmcli device show ens192 | grep -i ip4.g
nmcli device show lo
nmcli device
```

## Display interface details

Here we will display IP details including interface settings, routing and arp cache.  Keep in mind your interface name may differ from ens192. Consider the differences in the ip addr show commands.

>**Note**: The ip command arguments below show examples of shortcuts such as using **s** in place of **show**.

```bash
ip address show
ip -4 addr s
ip -o a show
ip -o -6 addr show
ip addr show lo
ip addr show ens192
ip route
ip route get 8.8.4.4
ip neigh
```

## Working with socket statistics (ss)

Next using [ss (Socket Statistics)](https://linux.die.net/man/8/ss) review connection details. This command is the IPv6 compliant replacement for netstat. Remember you can use `ss --help` or `man ss` for details about this utility.  

What are you seeing with the **ta** vs **ua** options? Can you list them all together?

```bash
ss
ss -ta
ss -ua
ss -ta src *:ssh
ss -4
ss -s
```

## Network configuration

Now we are going to review some of the core system configuration files.  Consider this questions as you review the files.

* Which file contains your DNS server details?  
* What hostname values are set in the /etc/hosts file?

```bash
cat /etc/hosts
cat /etc/resolv.conf
cat /etc/sysconfig/network-scripts/ifcfg-ens*
```

## System control (systemctl) commands

The systemctl command is one of the systemd utilities.  Systemctl is responsible for controlling and inspecting the systemd system and service manager.  Next we are going to review your network service status and display the other services.  

>**Question**: How can you tell if a service is running using **systemctl**?

```bash
systemctl status NetworkManager
systemctl --type=service
systemctl --all
systemctl --state=inactive
```

## Working with filters and **ip**

This is another chance for you to work with filters at the command line.  We want to identify our IP address, subnet mask and default route IP address.  The following commands attempt to show all the data and then filter the output to answer the 3 questions listed.  However they all just miss the mark, Your challenge is to fix the filter command examples?

```bash
ip -4 -o addr
ip -4 -o addr show | grep 2: | awk '{print $5}' | cut -d/ -f1
ip -4 -o addr
ip -4 -o addr show | grep 2: | awk '{print $4}' | cut -d/ -f1
ip route 
ip route | grep default | cut -d' ' -f5
```

![filter hints](filter-hint.png)

## Managing networking with NetworkManager

It is expected that your network interfaces are **managed** by NetworkManager except for the loopback address.  This is the default for Redhat/CentOS, Debian/Ubuntu systems.

Run ```nmcli device``` to confirm that your ethernet interface is managed.  Interfaces not managed by NetworkManager are listed as  **unmanaged**.  The loopback interface should be an **unmanaged** interface.

![nmcli device](nmcli-managed.png)

Now we are going to change your ethernet network interface and set it to unmanaged by the NetworkManager service.  To do this add `NM_CONTROLLED=no` to your interface configuration under file /etc/sysconfig/network-scripts/.  On my system the script name is `ifcfg-ens192` but your file might be `ifcfg-ens33`.

![device configuration file update](nmcli-no.png)

Restart the NetworkManager service to pickup this change.

```bash
nmcli device
sudo systemctl restart NetworkManager
nmcli device
```

>**Note:**: Restarting the **network** service or **NetworkManager** will pickup this change.

Here is a screenshot of the process showing a restart of the **network** service.

![nmcli unmanaged](nmcli-unmanaged.png)

Now change the **NM_CONTROLLED=no** directive to `NM_CONTROLLED=yes` or remove the line entirely and restart the NetworkManager service.

Next we are going to try and change the DNS setting and point to the google dns servers.  Any of the network settings can be changed using ncmlci tool.  

>**Note**: write down your ipv4.dns value, if you want to revert these changes you will need that IP address.

```bash
ping uw.edu
sudo nmcli connection edit ens33
print connection
print ipv4
print ipv4.dns
remove ipv4.dns
print ipv4.dns
set ipv4.dns 8.8.8.8
save
quit
grep -i dns /etc/sysconfig/network-scripts/ifcfg-ens160 
ping uw.edu
```

If you see an error about "Insufficient privileges" consider if you have rights to change the networks setting as your normal user or if you need to use **sudo**.

![nmcli sudo](nmcl-sudo.png)

>Note: you can leave your DNS server as 8.8.8.8 or switch it back to your internal network value using the steps completed above.

___

## Lab complete

___



