# Git cheat sheet

The following is a brief Git cheat sheet written using Markdown syntax for the purpose of showing some Markdown basics and Git basics.

## Git initialization

When creating a new git repository create a folder and then run `git init` to setup the default folders. Run the following commands.

```bash
git init
git config --global user.name "your name"
git config --global user.email user@example.com
git config list
```

## Git commit and push

The following steps will add a file to your git repo, commit the changes and then push the change to your default repository.

```bash
git add .
git commit -m "git commit message here"
git log
git push origin master
git log
```

## Markdown tips

Markdown is the default mark up language used with gitlab, github and many other git based tools.  Markdown allows you to add formatting elements to a plaintext file.

The headings are defined using `#` symbol.  One `#` is the largest heading, two `##` is the next smaller heading and so on.

### Sample header using 3 # symbols
