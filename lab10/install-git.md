# In class lab: Installing and working with Git

>**Objective:** install Git locally and setup an online account with GitLab

>**Note:** It is assumed that you are doing this lab on your **development** host and not your **production** server.

## Installing GIT

We can do this using dnf.  Once Git is installed we can confirm it is working as expected by simply entering **git --help** which should return the standard help menu.

>**Note:** While the [yum command still works it actually points to dnf](../hints/yum_and_dnf.png) which is the replacement for yum.  Dnf has support for yum command arguments and syntax.

```bash
sudo dnf install git
git --help
```

![install git](local-git-install.png)

Once we have git installed we can setup our local git repositories aka folders.  To start create a folder called "git".  Inside this folder we will create another folder called local_repo and finally another folder called "projects".  Once created we will change directories to the projects folder.  

>Note: You can use the -p option create the folder structure in one command as shown.

```bash
mkdir -p ~/git/local_repo/projects
cd ~/git/local_repo/projects
```

### Initialize your local Git instance

>__Note:__ When we create a new git repository the initial branch by default will be **master**.  Terms like master, slave, whitelist, blacklist were common in the past but are slowly being replaced in the IT space.  We can change the default git branch name using the ```--initial-branch=main``` which will create a default branch called **main**.

Now that we have our folder structure in place we will initialize our git instance and set our username and email address.  We can confirm the settings are in place with --list

```bash
pwd
git init --initial-branch=main
git config --global user.name ${USER} 
git config --global user.email ${USER}@localhost
git config --global color.ui "auto"
git config --list
```

Here is a screenshot of these steps.

![setup your git](git-init.png)

### Add content to your local git repo

Now we can create a file called README.txt inside our "projects" folder. Once the file is created we will add it to our git repo along with a comment.

```bash
echo "This is the initial readme for a local git repository." > README.txt
cat README.txt
git add README.txt
git commit -m "initial version of README.txt" README.txt
git status
git log
```

![git commit](git-commit.png)

Next we want to create a new file with a list of linux commands and some of the options you use.  This file will be called **linux_commands.txt**

>__Question:__ Why do you see a single > and then double >> below? What would happen if they were both using a single > symbol?

```bash
echo "Listing of common linux commands and useful options" > linux_commands.txt
echo "mkdir -p /path/to/files # this will create all the folders in the path" >> linux_commands.txt
git add linux_commands.txt
git commit -m "initial version of linux_commands.txt"
git status
git log
```

![git commit new file](git-commit-new-file.png)

### Working with git ignore

Create a folder called "archive" under " ~/repo/project". Copy /etc/passwd to this new directory.  See if git has noticed the new directory and file.  Then create a .gitignore file and include the archive directory. Git ignore will ignore files and directories based on absolute names and paths, relative paths and using simple regular expressions

```bash
mkdir ~/git/local_repo/projects/archive
cp /etc/passwd  ~/git/local_repo/projects/archive
git status
touch .gitignore
echo "archive/*" > .gitignore
git status
```

![git ignore](git-ignore.png)

Next we are going to update the README.txt file and add notes about the .gitignore file.  

```bash
echo "include a .gitignore file to avoid including directories and files." >> README.txt
```

Check in your updated README.txt file to your git repo.

```bash
git add README.txt
git status
git commit --message "updated README with git ignore details"
git status
git log
```

![git add](git-add.png)

### Setting up your GitLab account

Using gitlab requires generating SSH keys and providing the public certificate to gitlab for authentication. This is something we covered earlier.

>**Note:** You must have a **private key** on the host (dev, prod your local desktop) from which you want to work with Gitlab.  In most cases this means either creating a new key pair or copying your private key to your host.  For this lab I suggest you create a new key pair on your Development host using **ssh-keygen**.

We are going to use **GitLab** this quarter and now you need to setup your own GitLab account unless you already have one you want to use.


If you need to create a new GitLab account go here: [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)

Once you are logged in locate the profile icon in the top right corner, click the down arrow and select either **Edit profile** or **Preferences**.

![git settings](git_settings.png)

Next on the left hand side locate "SSH Keys" and then in the Key box paste in your public SSH key file from your Development server.  The default location for this file on your dev or prod host is something like /home\/**USER**/.ssh/id_rsa.pub.

Once you paste your public key into the Key field the **Title** field should auto populate but you can change the name if you want, maybe include the source of the key such as **itfdn-dev01**.  Finally click **Add key** and you should see your key added to the menu with the default name and fingerprint displayed.

![Git ssh keys](git_sshkeys.png)

Once your account is created and your public certificate or key uploaded we can create our first repository.  Click on the Gitlab icon in the top left corner to open the default projects menu.  Here click **New project**. 

![new git project](git_new_project.png)

On the next screen select **Create blank project**.

![blank project](base-project.png)

Now we have a few options to select and set.

1. Set the project name to **itfdn-140-inclass-\<username>**.  
2. Set the project URL to your git **username**
3. set the project deployment targe to **No deployment planned**
4. ensure the visibility is set to **Private**
5. unselect **Initialize repository with a README**

Finally click **Create project**.

![new git project](git_new_base_project.png)

Now we are going to initialize a new folder for gitlab content, clone your new empty repository and push up our first README.md file.

Start by returning to your development linux host and creating a new folder under /home/USER/git called **gitlab**.  Once the folder is created cd to this location and run the **Git global setup** command examples followed by **Create a new repository** command examples as displayed on the new git repo created page.

```bash
cd ~/git/
mkdir gitlab
cd gitlab
```
Now review the command examples and run those on your development host.

![git clone ssh copy](gitlab-ssh-clone-repo.png)

Now that you are in your new directory under /home\/**USER**/git/gitlab/  you should see your default `README.md` file.  Open this file with Vim and add the contents from this [FILE](README.md)  and then save the updates.  Now we want to update our repo with this updated `README.md`.

```bash
git add README.md
git commit -m "add git and markdown tips to readme" README.md
git push origin main
git status
git log 
```

Now return to the gitlab.com project and confirm that your updates are there also. Notice your README.md file has changed and the landing page for your new repo has been updated.

![readme default file updated](gitlab-readme-md.png)

Now create a new cheatsheet file of how to add content to your gitlab repo and add it to your repo.  You can call this file anything you want but you must add it to the repo, commit the file and then push it your gitlab repo.

### Troubleshooting tips

Q. When I try to add my content to a repo it fails with an error like "fatal: Nota git repository".

A. Make sure you are in the correct git folder for your repository and make sure it has a .git folder.  If this is not the case change to your target git folder and run "git init"

Q. When I try to clone a repo I see a message like "Cloning into 'itfdn-140-scripts'...Permission denied (publickey)"

A. Make sure you have a valid public key registered with Gitlab.  Make sure you have the associated private key for the public key in Gitlab on your source host.  If this is a linux host check under /home/$USER/.ssh for a file called id_rsa.  This should be the private key to the public key you have copied to Gitlab.  Finally make sure the private key is loaded into your ssh-agent. Typically you can do this using "ssh-add" which by default will try to read in your id_rsa private key.

___

## lab complete

___
