# In class lab: Certificates and web servers

>Objective: Work with openssl to create a self-signed certificate and enable and SSL web server.

>Note: It is assumed that you are doing this lab on your development host and **not** your production server

First let us run through the default experience. Create a new folder called certs in your default users home directory. Change directory into the new certs folder to complete the certificate creation process.

```shell
 cd
 mkdir certs
 cd certs
```

Running the next command will create a new public certificate and private key pair. You will be prompted for certificate details during the process.

>__Question:__ do you need to run this command using `sudo`?  If you do run the command using `sudo` who will own the files created?

```bash
 openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout private_certificate.key -out public_certificate.crt
```

input sample:

```bash
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:US
State or Province Name (full name) []:Washington
Locality Name (eg, city) [Default City]:Seattle
Organization Name (eg, company) [Default Company Ltd]:UW PCE
Organizational Unit Name (eg, section) []:IT FDN
Common Name (eg, your name or your server's hostname) []:mydev.localhost
Email Address []:admin@localhost
```

![sample input](openssl-new-cert.png)

Now you can view the contents of the output from this the openssl command.  You should find two files `private_certificate.key` and `public_certificate.crt`.

![certs and keys](certs_and_key.png)

Next we are going to review the values set in our certificate. 
Here note your serial number, it will look similar to this: "c3:ce:b1:9c:ca:a8:6a:1e".  Later in the lab we are going to look for this value using your HTTPS server.

```shell
openssl x509 -in <your public certificate> -text -noout
```

Now confirm that your private key is valid using the following openssl command.

```shell
 openssl rsa -in <your private key> -check
```

If you lose the private key the cert is no longer valid, just like your ssh key is required for the public key to allow logins to occur.  To protect your private key we are going to use the `chattr` command to make the key immutable.  We can use `lsattr` to check the applied permissions.

In the following section we can see how only root can set the immutable attribute on a file.  We can see how even root cannot delete a file once this attribute is set and how only root can assign this attribute to a file.

```bash
sudo chattr +i ~/certs/*.key
lsattr ~/certs/*.key
sudo rm ~/certs/*.key
touch ~/testfile.txt
chattr +i ~/testfile.txt
sudo chattr +i ~/testfile.txt
sudo lsattr -l ~/testfile.txt
sudo rm ~/testfile.txt
sudo chattr -i ~/testfile.txt
sudo rm ~/testfile.txt
```

![chattr example](chattr-i.png)

Now in order to illustrate how to update the default openssl options and values we are going to copy the default openssl config file and update some of the values to meet our needs.

First find your local copy of openssl.cnf. If you don't know where it is use locate to find it. If you don't have this installed do so now. There is a feature in dnf that allows you to search existing packages for utilities like locate, this is the **provides** option.  So, we could search using `dnf provides "/usr/bin/locate"` but if the utility was stored under /usr/local/bin/locate for example we would not find a match. To avoid this we can use a wildcard search "*/locate" which allows us to search any path in the package with locate.  

```shell
 sudo dnf provides "*/locate"
 sudo dnf install mlocate
```

If you are installing locate for the first time be sure to create the initial index database and then search for the config file.

```shell
sudo updatedb
locate openssl.cnf
```

Once you have found the openssl.cnf file copy it to your home directory under the certs folder.

```shell
cp /etc/pki/tls/openssl.cnf openssl.cnf
```

Now update following values under [ req_distinguished_name ]; note the default values. Also note once this is in place you will still have to answer the questions only now the default value is something you set so pressing \<ENTER> sets your new default value.

```shell
[ req_distinguished_name ]
countryName = Country Name (2 letter code)
countryName_default = US
countryName_min = 2
countryName_max = 2
stateOrProvinceName = State or Province Name (full name)
stateOrProvinceName_default = Washington
localityName = Locality Name (eg, city)
localityName_default = Seattle
0.organizationName = Organization Name (eg, company)
0.organizationName_default = UW PCE
# we can do this but it is not needed normally :-)
#1.organizationName = Second Organization Name (eg, company)
#1.organizationName_default = World Wide Web Pty Ltd
organizationalUnitName = Organizational Unit Name (eg, section)
organizationalUnitName_default = IT FDN
```

___
Creating a custom **openssl.cnf** file
___

![openssl custom](openssl-custom.gif)

___

Now create a new key pair using your custom openssl.cnf configuration file.

```shell
openssl req -x509 -config openssl.cnf -nodes -days 365 -newkey rsa:2048 -keyout private_key_custom.key -out public_cert_custom.crt
ls -lart
```

Now we are going to create a new nginx and nginx/private directory under /etc/pki/ and copy your new certificate and key files into these new directories after they are created.

```shell
 sudo mkdir /etc/pki/nginx
 sudo mkdir /etc/pki/nginx/private
 sudo cp ~/certs/public_cert_custom.crt /etc/pki/nginx/
 sudo cp ~/certs/private_key_custom.key /etc/pki/nginx/private
```

Now we are going to install and configure the nginx web server to support SSL. Backup your existing nginx.conf file and then add the following lines to support https with your new ssl certificate and key.  Make sure the certificate and key names and paths match the values in the configuration file below.

You should be working with the basic nginx configuration file that we created in [lab13](https://www.ulcert.uw.edu/itfdn-labs/lab13/lab-web-server-install.html).  You nginx.conf should be around 30 lines if it is longer then you probably have not replaced the default nginx.conf file.

```shell
server {
        listen       443 ssl http2 default_server;
        listen       [::]:443 ssl http2 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
        ssl_certificate "/etc/pki/nginx/public_cert_custom.crt";
        ssl_certificate_key "/etc/pki/nginx/private/private_key_custom.key";
        location / {
        }
    }
```

Your completed nginx.conf file should look similar to this example.  Be sure to check the opening and closing brackets.  If you are using Vim move to the first opening curly brace "{" and then use % to find the matching closing "}".

![nginx ssl config](nginx-ssl-config.png)

Here are the commands to test and restart nginx

```shell
sudo nginx -t
sudo systemctl stop nginx
sudo systemctl status nginx
sudo systemctl start nginx
sudo systemctl status nginx
```

Once you have restarted the Nginx web server you can test your new https server for connectivity using curl.

```shell
curl https://localhost
curl -k https://localhost
```

Now we need to update your firewall rule to allow traffic over port 443 (ssl). Notice that the new https service is not listed until after the **reload** command is completed.

```shell
sudo firewall-cmd --get-active-zones
sudo firewall-cmd --zone=public --list-all
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --zone=public --list-services
sudo firewall-cmd --reload
sudo firewall-cmd --zone=public --list-services
```

Test your site and confirm you are receiving your new certificate.  Do this using a standard browser (yes you will see warnings that your site is not trusted). 

If your VMware dev host is configured to use a NAT network connection then this web server will only be available from your local laptop or desktop.  This is how I expect your VMWare host to be configured. But, if you were to change your network settings to "Bridged" then this web server would potentially be available to anyone on your home private network.  This does not mean anyone of the internet but just anyone else on your local network.

![not trusted](not-trusted.png)

Check your certs again either in a browser or from the command line. Confirm the serial number of your certificate.  You can view your certificate serial number from the command line using openssl.

```shell
openssl x509 -in <your public certificate> -text -noout
```

___

## Lab complete

___
