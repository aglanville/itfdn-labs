Also look at 
https://www.axllent.org/docs/view/ssh-geoip/

also https://rubysash.com/operating-system/linux/bash-script-update-ufw-rule-for-dynamic-host/ 


#!/bin/bash
########################################################
# SIMPLE SLOWLORIS ATTACK PREVENTION
# craig@123marbella.com
# version 1.1 - 7/1/2014
# version 1.2 - 6/2/2014 - added whitelisting ability
#
# the purpose of this script is to prevent slowloris attacks
# slowloris is difficult to watch for so you need to have
# apache mod_qos installed so that it logs any such attacks as mod_qos(045) to the error_log
# then we parse error log for any entries containing the string mod_qos(045) and blacklist based on the total attacks
#
# This is the process used to blacklist the ip's who appear to attack the server
# 1. parse the error_log and search for certain string like mod_qos(045)
# 2. save a list of results to a temp file which holds the ip address and the total attempts
# 3. parse the temp file and check how many attempts per ip
# 4. if the attempt is => the BLOCK_LIMIT, we attempt to ban the ip
# 5. before banning we have to check the white list to see if the ip is whitelisted
# 6. if the ip is not on the whitelist, we block the ip and write an entry to the logs
#
# TIPS
# a. you can monitor each ban during an attack by running $ tail -f /var/log/hsws-banned-ips
# b. you can whitelist ips by adding them to the whitelist usually at /root/hsws-whitelist-ips 
# c. if its a heavy attack then you can adjust the BLOCK_LIMIT variable
########################################################

#define some variables used within the script
SOURCE_LOG=/usr/local/apache/logs/error_log  #Change this path to the file you want to read
OUTPUT_LOG=/root/hsws-log-slowloris-attacks #change this to the location you want to output the results to
BANNED_LOG=/var/log/hsws-banned-ips #location of the log specifially for tailing bans
SEARCH_PATTERN="mod_qos(045)" #This is the pattern you want to search within the log $SOURCE_LOG
BLOCK_LIMIT=25 #the total number of attempts after which we block the ip
HOSTS_DENY=/etc/hosts.deny #the hosts.deny file
WHITELIST=/root/hsws-whitelist-ips #this is a list of ips that we want to make sure are not blacklisted

########################################################
#scripting starts here - no need to change anything after here
########################################################

#Check for existance of WHITELIST
if [ ! -e "$WHITELIST" ] ; then 
  touch "$WHITELIST" 
  echo "SUCCESS! Whitelist has been created at $WHITELIST" >> $SOURCE_LOG
  echo "SUCCESS! Whitelist has been created at $WHITELIST"
fi
#if we cannot write to the whitelist then throw an error
if [ ! -w "$WHITELIST" ] ; then 
  echo "[$(date)] [error] ERROR! CANNOT WRITE TO $WHITELIST" >> $SOURCE_LOG 
  echo "ERROR! Cannot write to $WHITELIST" 
  exit 1 
fi

#parse the log file at $SOURCE_LOG and save the log file to $OUTPUT_LOG
cat $SOURCE_LOG | grep "$SEARCH_PATTERN"  | awk '{ print $8 }' | sort | uniq -c  | sort -n | sed 's/]\+$//' > "$OUTPUT_LOG"

#test for existence of the $OUTPUT_LOG file
if [ -f $OUTPUT_LOG ]
then

    #if you are using asl firewall then use this block
    #awk '$1>'$BLOCK_LIMIT'{system("asl -bl "$2)}' < $OUTPUT_LOG
	#echo "SUCCESS > parsing of $OUTPUT_LOG has been completed!"
	#exit 1

    #check OUTPUT_LOG for any ip with more than  BLOCK_LIMIT
    #because we only need to block ip's with more than a certain number of attempts
	cat $OUTPUT_LOG | while read ATTEMPTS IP
	do
    	#if the ip address has exceeded the $BLOCK_LIMIT then we need to blacklist the $IP
		if [ $ATTEMPTS -gt $BLOCK_LIMIT ]
		then
			#first we need to check to see if the ip appears in the whitelist file
			if grep -q "$IP" $WHITELIST; then
   				echo "$IP is whitelisted"
   				echo "[$(date)] [warn] blacklisting SKIPPED for $IP as its whitelisted in $WHITELIST" >> $SOURCE_LOG
   				
   				#we need to check if its inside the /etc/hosts.deny file and remove it
   				if grep -q "$IP" $HOSTS_DENY; then
   				  sed -i '/$IP/ d' "$HOSTS_DENY"
 				  echo "$IP is whitelisted and was found in $HOSTS_DENY but now its been removed"
 				  echo "[$(date)] [warn] $IP was removed from $HOSTS_DENY as its whitelisted in $WHITELIST" >> $SOURCE_LOG	
   				fi
   				
 			else		
		
				if grep -Fq "$IP" $HOSTS_DENY
				echo "$IP already exists in blacklist"
				then
				continue
			else
    	        #if ip not exists in hosts.deny, then add the ip to the hosts.deny file
				echo "ALL:$IP" >> $HOSTS_DENY
				
				#try to get the dns name
				DNSNAME=$(host -tPTR $IP) || DNSNAME="1 1 1 1 dns not found"
		        DNSNAME=$(echo $DNSNAME|cut -d" " -f5-)

                echo "[$(date)] [warn] $IP ($DNSNAME) BLOCKED FOR $SEARCH_PATTERN - $ATTEMPTS Attempts recorded" >> $SOURCE_LOG
                echo "[$(date)] [warn] $IP ($DNSNAME) BLOCKED FOR $SEARCH_PATTERN - $ATTEMPTS Attempts recorded" >> $BANNED_LOG
				echo "$IP ADDED to blacklist"
				fi
			fi
		fi
	done

	#message here that the process is completed
	echo "[$(date)] [warn] PROCESSED $OUTPUT_LOG" >> $SOURCE_LOG
	echo "SUCCESS > parsing of $OUTPUT_LOG has been completed!"
	exit 1
	
else	
echo "ERROR > $OUTPUT_LOG not found!"
exit 1
fi

########################################################
#scripting ends here
########################################################