# In class lab: Working with Kubernetes

>Objective: Configure your linux development server to connect to one of the class kubernetes clusters

>Note: It is assumed that you are doing this lab on your **development** host and not your production server

## Installing kubectl

The primary utility for managing kubernetes clusters is [**kubectl**](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/).  We need to install this tool on our development servers.  To do this need to download the binary and move it into our path.

>Note: To get the latest version use `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"`. However, you want to keep your kubectl client within in one version of the kubernetes version as a best practice.

```bash
export KVERSION=v1.29.5
curl -LO https://dl.k8s.io/release/${KVERSION}/bin/linux/amd64/kubectl
sudo mv kubectl /usr/local/bin/
sudo chmod 755 /usr/local/bin/kubectl
kubectl version --client
```

![kubectl install](../lab28/images/kubectl-install.png)

We also need to set the **KUBECONFIG** environment variable. The KUBECONFIG file includes a selection of values that are used when you interact with the kubernetes instance such as the API endpoint, users, and certificates.  

There are three methods for configuring your client to work with your new K8S cluster.

1. use --kubeconfig flag, if specific
2. use KUBECONFIG environment variable, if specified
3. use $HOME/.kube/config file 

We are going use option 3.  To start we need to copy a config file to our development hosts from the class web server and update the server details.  I removed the server directive on purpose to make it a little harder for someone outside the class to use this config file and potentially access our class cluster.

>**Note**: you will only see **config.bak** if you run this twice or more..

```bash
cd
mkdir .kube
cd .kube/
curl -O https://www.ulcert.uw.edu/docs/lab-config
mv lab-config config
sed -i.bak 's/REPLACE_WITH_SERVER_DETAILS/https:\/\/140\.142\.159\.160:6443/' config
kubectl get nodes
```

![kube config](images/kube-config.png)

Now we want to add command completion for kubectl to our environment.  This feature takes advantage of the **bash_completion** package which you might have to install if you have not done so already.  Once bash completion is installed we will update our **.bashrc** and then log out and back in again to enable this feature.

```bash
cd
sudo dnf install bash-completion
echo 'source <(kubectl completion bash)' >>~/.bashrc
tail .bashrc
exit
ssh <your dev host> 
```

![kubectl cmd completion](../lab28/images/kubectl-completion.png)

Next we are going to load another tool called [kubens](https://github.com/ahmetb/kubectx).  This tool will switch your default namespace context.  Later in the lab we are going to create unique namespaces and this tool will allow you to switch to your specific namespace.

```bash
sudo git clone https://github.com/ahmetb/kubectx /opt/kubectx
sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens
kubens
```

![kube ns](../lab28/images/kubens.png)

Next we are going to explore the cluster.  First check out the cluster details, nodes, namespaces, and services.

```bash
kubectl cluster-info
kubectl get nodes
kubectl get nodes -o wide
kubectl get namespaces
kubectl get services
kubectl get services -A
```

Next we are going to create a new namespace based on your prod server short name such as ulc-161 or ulc-184. Once the namespace is created we are going to use `kubens` to change our default namespace to that value.

```bash
kubectl create namespace ulc-{YOUR PROD SERVER VALUE HERE}
kubectl get namespaces
kubectl describe namespaces ulc-{YOUR PROD SERVER VALUE HERE}
kubens ulc-{YOUR PROD SERVER VALUE HERE}
kubens
```

![namespace](images/namespace.png)

Now we are going to create a Pod running an nginx container. The first command shows you the yaml required to create the object. This is the purpose of the `--dry-run=client -o yaml` command.  The next line skips that step and just creates the pod file and then you manually create the pod from this file.


```bash
kubectl run busybox --image=busybox --dry-run=client -oyaml --restart=Never -- sleep 1000 
kubectl run busybox --image=busybox --dry-run=client -oyaml --restart=Never -- sleep 1000 > busybox.yaml
kubectl create -f busybox.yaml
kubectl get pods
kubectl get all
```

We can login to busybox using a standard bourne shell.  Busybox is often used for quick testing but it has a very limited tool set but you can take a look around.  Does the ip address you find in the busybox pod match what you see using `get pods -o wide`?

```bash
kubectl get pods -o wide
kubectl exec -it pod/busybox -- sh
ip addr show
ping google.com
nslookup uw.edu
exit
```

Now we are going to create a deployment. A deployment allows kubernetes to monitor and ensure the pod or pods are always running. Unlike a stand alone pod which if it dies nothing will recreate it.  The replica set is how kubernetes ensures the pod remains. Which pod is recreated after being deleted?

Using the `-o wide` option we can see which worker node the pod was deployed too.  Using describe we can see our pod details.



```bash
kubectl create deployment --image=nginx web1 --dry-run=client -o yaml
kubectl create deployment --image=nginx web1
kubectl get pods
kubectl get pods -o wide
```

The pod name in a deployment has a random hash value associated to the name.  You will have to use your **TAB** key to auto expand the web1 name to reflect this value.  You can of course manually type in the name too.

```bash
kubectl describe pod web1-<TAB>
kubectl get all
```

Kubernetes provides a [GUI based dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/) app that can be used for basic administrative functions.  A popular CLI app that provides similar functionality is [k9s](https://k9scli.io/).  k9s is looking for a kube config value, think about the __.kube__ directory and the __config__ file or having a KUBECONFIG env variable.  The install is very simple, make sure your kubectl is working, download the k9s binary, make sure it is executable and move it a location in your PATH.

The following notes will install k9s version 24.14

```shell
wget https://github.com/derailed/k9s/releases/download/v0.24.14/k9s_Linux_x86_64.tar.gz .
tar -xvzf k9s_Linux_x86_64.tar.gz
chmod 755 k9s
sudo mv k9s /usr/local/bin/
k9s
```

Once you have k9s running you will see a screen similar to the following

![k9s client](../lab28/images/k9s-client.png)

So, what is you don't see deployments?  How did that magic happen?  Using __SHIFT + :__ similar to how enter commands when working with [Vim](https://linuxhint.com/vim_basics_tutorial/) you can enter command mode.  Here in command mode we can use simple commands like __deployments__ or __nodes__ to show those kubernetes objects.  If you want to leave or exit the k9s console window use __quit__.  As you start to enter the word deployment you will see auto completion options show up such as __daemonsets__ or you may notice that deploy will also show deployments.  Use the escape key __ESC__ to exit any option or back out of a selection.

Don't worry if you don't see the same objects as we have not deployed anything to the cluster yet.  There are some examples coming up later but for now you probably just have the default content.

![k9s client options](../lab28/images/k9s-options.png)

Use __Ctrl + A__ to display all the aliases available within k9s.

Handy tool, right?

## Clean up

Now we are going to delete our test pods.  You are going to have to find the pod name for web1 as it will include some random characters.

```bash
kubectl get pods
kubectl delete pod busybox
kubectl delete web-<RANDOM CHARACTERS>
kubectl get pods
```

---
---

### Troubleshooting

---

If you see a message about failed certificate authentication confirm the time in your virtual machine.  If the time has slipped more than a couple minutes your authentication will fail.

```bash
date
sudo chronyc makestep
date
kubectl get nodes
```

---

Lab Complete

---