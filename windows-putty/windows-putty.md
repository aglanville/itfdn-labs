# How to Log In to your Linux hosts from windows with a password

In this quick document we are going to talk about using Putty on Windows but there are other solutions such as [the Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about)  

The first thing you need on your windows computer is the putty ssh client which you can download here: [https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)

There is no install required if you just download the [64 bit x86 putty.exe](https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe). You can install the entire putty suite of tools. Later in the quarter, you will find some of the additional tools in the suite helpful to have installed but they are not required at this time.

![Putty downloads](putty-install-page.png)

For this example just download the putty.exe binary and launch it from your desktop.

Here you can enter the IP address for your development host or the domain name for your production server.  In this screen shot I have an IP address of my development server in the hostname field.  Once entered I click open to connect to my development linux server. The username here is the one you defined when installing Linux in class.

>__Remember__ you have to start VMWare and your development virtual server before trying to connect to it with Putty.

![Connect to dev](connect-to-dev.png)

The following is an gif that tries to demo logging into your prod server using the **itfdn140** user.  This time I am not using the IP address I am using the Fully Qualified Domain Name (FQDN).  You should have received an email with this detail earlier in the week for your Production server.

![prod login](connect-to-prod.gif)

Finally in this gif I am showing how to save sessions and add default usernames.  Notice that after I add my username **itfdn140** this value is auto populated when I start my SSH session with the Putty client.

![saved sessions](saved-session.gif)

Ok, I hope that helps you get logged into your Production system.

___

End of guide

___