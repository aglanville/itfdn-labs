# In class lab: AWS storage and load balancing

>**Objective**: Review AWS S3 menus and options and the EC2 load balancing menus.

>**Note**: It is assumed that you are doing this lab on your **development** host and not your production server

## Share files to the public using S3 

The objective of this first section is to create a bucket, upload a file and then share that file to the public.  

First we need to login to the AWS console.  This may require opening the AWS lab again and launching the Vocareum portal.  You should already have the lab started and the AWS CLI credentials copied locally.

>Note: Remember do not select **End Lab** as it will remove all your AWS objects. Confirm you are in the **US-West(Oregon)** region once logged in.

![vocareum_portal](vocareum_portal_aws.png)

Locate the services search option in the top left corner and enter **s3**.  Then move your mouse to the S3 section which should expand and reveal a link to **Top features**.  Here click on **Buckets** which should redirect you to Amazon S3 Buckets landing page.  

![services search](services_search.png)

Here we are going to select `Create bucket`

>__Note:__ What region are you working in now?

![Create S3 bucket](s3-bucket-create.png)

This should redirect you to the "Create bucket" menu.  Next we define the bucket name, this must be unique across all Amazon S3 deployments.  Confirm that the AWS Region is set to "**US West(Oregon) us-west-2**.

Next enable **ACLs enabled** this will expose the Object Ownership options.  **Bucket owner preferred** should be enabled.

Uncheck "Block all public access", this should also unselect all the child options.  You will also have to acknowledge that these settings may allows objects in this bucket to be publicly available.

Leave the remaining settings with the default values and click **Create bucket**.

![s3 bucket name](s3-bucket-name.png)

Now click the link for your new bucket name which should redirect you to a page where we can upload images.  

![select bucket](select-bucket.png)

In the next screen click **Upload**

![upload](s3-bucket-upload.png)

Now click **Add Files** button.  Select a small text file or image for testing purposes.  Once you are returned to the Upload menu select your images, expand **Permissions**, and select **Grant public-read access**.  Check the box that indicates you understand the risks of granting public-read access to the specified objects.  Finally click **Upload**.

![add files](add-files.png)

Now you should be able to click on your test image or file which will take us to a page with details about our object including the public URL to it.

![files uploaded](s3-bucket-object.png)

Here you can review the object values and also find the public object URL.  Try to use wget to copy your object to your dev host. The following is just and example command. 

```bash
cd
wget https://{your-aws-s3-bucket-url-here}
```

![file url](file-url.png)

When ready try to delete your bucket, it will require that you empty the bucket first... see if you can figure this out. 

![delete bucket](delete-bucket.png)


## View Load balancer options

The load balancer menu is a sub menu under **EC2**.

Locate the services option in the top left corner and then using the services search locate EC2 and then select the EC2 option which should take you to the EC2 landing page.

![services search](services-search-ec2.png)

We are just reviewing the options of this functionality at this time. On the left side menu navigate down to Load Balancing and expand this section if necessary.  Here you should see **Target Groups** and **Load Balancers**.  

![EC2 load balancing](ec2-load-balancing.png)

Select `target groups` and then click `Create target group`, here you can see the target groups choices.  You are able to create target groups based on VPC members, IP addresses or lambda services.  Review some of the health check options and cancel the request when done.

![EC2 create target groups](ec2-load-balancer-target-group.png)

Next select `load balancers` and then click `create load balancers`.  Here you should see the options we discussed in the lecture.  The Classic load balancer, the Application load balancer and the Network load balancer.

![EC2 load balancers](ec2-load-balancer-create.png)

Here you can review the options and click "learn more" if interested in additional details.  Click cancel when complete as we are not adding a load balancer at this time.

____

### Troubleshooting tips

__

Q. When I try to view my S3 bucket image I am denied access.

![s3 request denied](s3bucket-denied.png)

A. Select the __Permissions__ tab then click __edit__ and grant everyone read access.

![permissions](s3-perms.png)

![edit perms](s3-perms-edit.png)

___

## Lab Complete

___