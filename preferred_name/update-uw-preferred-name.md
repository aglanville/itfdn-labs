# How to update your preferred name within  TEAMS and on other UW systems.

The name we see in our TEAMS site for each member is based on the NetID username and not a real or possibly your preferred name.  You can manage value for this name using the profile in [MyUW.edu](https://my.uw.edu).

To see your current display name click on the avatar or image in the top right corner of teams. In this example I have updated my preferred name to display "Angus G", this replaces my NetID value which is **angus1** in my case.

![TEAMS name](TEAMS-name.png)

If you want to update your display name login to MyUW.edu select profile from the left menu.  Now in the middle pane you should see a section labeled "UW Directory", click on "Change Directory Settings".

![my uw profile](myuw-profile.png)

This should redirect you to another login screen which in turn redirects you to [Identity.uw.edu](https://identity.uw.edu/).  Here you can edit your Preferred name.  It seems to want some value for **Last Name** I was tempted to enter "[the scotty dog](https://smile.amazon.com/Angus-Cat-Marjorie-Flack/dp/0374403821/ref=sr_1_1)" for mine :-).

![identity uw](preferred-name-update.png)

Once your change is in place it will take about 24 hours for it to publish.

No need to do this if you don't want to but you may prefer this name to your default uw NetID value.
