# In class lab: Working with SCP

>Objective: Install and work with both the command line (CLI) Graphical User Interface (GUI) versions of SCP.

>Note: It is assumed that you are doing this lab on your development host and __NOT__ your production server.

## Cyberduck a GUI based SCP tool for MAC

If you are using a MAC then CyberDuck provides a decent GUI based SFTP client. SFTP is slightly different from SCP as this protocol support directory listings and can resume interrupted file transfers.

Install CyberDuck on your mac [https://cyberduck.io](https://cyberduck.io/)

Start Cyber duck and review the options to launch a connection or create a profile.

![Cyber Duck connection](cyber_duck_connections.gif)

click the "+" to create a profile for your DevHost

![Cyber duck bookmarks](cyber_duck_connections_bookmark.gif)

Now try to copy the /etc/passwd file to your local host from your development host.  

## WinSCP a GUI based SCP tool for Windows.

If you are using a Windows host WinSCP is a decent SCP and SFTP client to install.

Install WinSCP on your Windows client [https://winscp.net/ent/index.php](https://winscp.net/eng/index.php) 

I recommend starting WinSCP and selecting the explorer view.
Create a connection to your DevHost.  Here you want to enter your IP (dev host) or hostname (prod host) in the hostname field.  

![New winscp login](winscp-save-host.jpg)

Now save this configuration for a quick connection next time.

![save host name](winscp-namehost.jpg)

Now you can login to your host using winscp.

![winscp login](winscp-connected.jpg)

Now try to transfer a file from your linux host to your local windows desktop such as /etc/passwd. 

## Using SCP from the command line from either a Windows or Mac host

Now that you know how to use SCP and SFTP using a GUI client like Cyberduck or WinSCP we are going to try and work with the command line tools.

## Transfer files with SCP from a Mac or Linux host.

SCP is included in the SSH tools that were installed with OpenSSH or both the Mac and Linux hosts typically.  The primary concept to understand is that the order is "command SOURCE DESTINATION".  Think about source as the location of the file or files you want to move and destination as where you would like that file to appear.  

In this example we are going to copy the file /etc/group from your dev host to your local system.  Then we are going to copy the same file AGAIN but this time we are giving the file a name on our local system.

So the steps at a high level are:

- Open a terminal session on your local laptop.
- Make sure your ssh-agent is running and that your private key is loaded. 
- Copy the /etc/group file from your dev host to your local host.  

Check if you have a key loaded, if when you run `ssh-add -l` you see your private key listed you are good, if not add the key.  Key's with no passphrase will be automatically added those with a passphrase will require your enter that before adding it to the ssh agent.

```bash
ssh-add -l
ssh-add
ssh-add -l
```

![adding your keys](add-keys.png)

Now we assume you have your key loaded. Copy the /etc/group file to your local system, copy it again to an explicit local file name and then work with the files a little bit.

```bash
cd
scp user@devhost:/etc/group .
scp user@devhost:/etc/group dev_groups.txt
cat group
cat dev_group.txt
diff group dev_group.txt
echo "new line" >> group
diff group dev_group.txt
```

What is the difference between your local files group and dev_group.txt before and after your echo command.  What would happen to the group file if you only used one > sympbol?

![copy group file locally](copy-group-file.png)

## Using SCP from the command line on Windows

The putty scp file is called pscp by default. This process is a bit challenging but you can do it.  In most cases you will probably use WinSCP but you should at least know how it could be done.

>Note: You can rename this to scp if you want or leave it as pscp.

First make sure you have your private key loaded into the Putty Agent.  The agent is part of the Putty Suite of tools and should show up under the Putty menu on your start menu or you can search for it.

![launch putty agent](../lab7/start_putty_agent.png)

Next add your private key to the Putty agent which should be in your system tray.  Just like your linux host if you have a passphrase assigned to your private key it will ask you for this before adding the key to the agent.

![load private key](../lab7/putty_agent_add_key.png)

In this task we are trying to copy the passwd file to your local host.

Open a windows command prompt

Windows -> Run  
Enter the text **"cmd"** and press \<Enter> 

![Windows start run](win_start_run.gif)

This should launch  a screen simliar to the one shown below.

![Windows prompt](windows_prompt.jpg)

Change to the putty install directory, this may differ on your host but the process is similar to the commands listed below.  In this series of commands we are trying to copy the /etc/password file from your dev host back to your local windows host.

```bash
cd "C:\Program Files <x86>\Putty"
pscp.exe user@{devhost IP Address here}:/etc/group C:\Users\user\
```

Below is an example of what this may look like.

![windows scp example](windows_pscp_with_arrows.gif)

>Note:  Review the screenshot closely and notice the change directory command (cd) and the back slash symbols used to define the path.  Also note the colon ":" before the /etc/passwd path. You should see some file transfer feedback related to the file and the transfer size and speed.

Why do you know from this gif that the private key was not loaded or the Putty Agent was running?

![Windows scp example](windows-scp.gif)

___
Lab Complete
___
