# In class lab: Docker and Python 3

>**Objective**: Create a Docker Container and configure it as your AWS CLI environment.

>**Note**: It is assumed that you are doing this lab on your **development** host and not your production server

In this lab we are going to build a docker container that will include the AWS CLI tools and supporting packages.  We are going to copy into this container our AWS credentials which will allow us to authenticate and then work with AWS from the command line.

You should know how to login to the AWS console using our Vocareum login if not review [lab 19.5](https://www.ulcert.uw.edu/itfdn-labs/lab19.5/aws-console-ui.html) where we go over this process.

## Docker review

We don't have to use a docker container to connect to AWS but doing so provides some more hands on experience with Docker and containers. Using a container also means we have the same environment for everyone in class, same Python version, same PIP version, same patch level and so on.  

This first section is intended to help review the Docker container basics along with how to build a docker container.

### Starting a docker container

First we want to see if there are any docker containers running and if there are we want to stop them.

>**Note**: We can use the command substitution to feed a list of details to our **docker** command. So, we can get a list of all the running docker containers and then feed those values to docker stop for example.  Notice we are using the **-q** option with **docker** which will return just the container ID values without any other details. We can use this in line with **docker stop** to stop all running docker containers `docker stop $(docker ps -q -a)`.

```bash
docker ps
docker ps -a
docker ps -a -q
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```

Note: See [lab21](https://www.ulcert.uw.edu/itfdn-labs/lab21/lab-docker.html) if you don't have docker installed on your development server yet.

We can start a docker container and Sign-in to that container also.  Pay attention to the docker container ID value returned by `docker ps` and the value returned when you run hostname from within the docker container.

```shell
docker run --name demoweb -d nginx
docker ps
docker inspect demoweb
```

Now we are going to sigin to the docker container and verify the hostname and review the utilities available under common folders /bin, /sbin, /usr/bin, /usr/sbin.

```bash
docker exec -it demoweb bash
hostname
curl localhost
ls -la /bin
ls -la /sbin
ls -la /usr/bin
ls -la /usr/sbin
cat /etc/nginx/nginx.conf
exit
```

Now stop and remove the demoweb docker container.  The docker ps command shows running containers but using the `-a` option shows running and stopped containers.

```bash
docker ps
docker stop demoweb
docker ps
docker ps -a
docker rm demoweb
docker ps -a
```

### Building custom containers

Now we are going to create a folder called `dockerbuilds` and inside of this folder will be two sub folders one called `alpine` and the other `rockyawscli`.  We are going to work on an [Alpine Linux](https://alpinelinux.org/about/) based container first.  

```bash
cd 
mkdir -p  ~/dockerbuilds/{alpine,rockyawscli}
cd ~/dockerbuilds/alpine
```

Now create a file called `Dockerfile` (yes it needs to start with a Capitol **D**).

```shell
vi Dockerfile
```

Now paste the following text in to the Dockerfile file.  When working with Docker we can create images based on existing docker containers.  The first layer of our Docker container will be based on Alpine Linux.  The first line of our **Dockerfile** says `From alpine`. Because we are not defining the version of Alpine Linux we want we will get the latest build.  If we wanted a particular version we would use the the following syntax: `FROM alpine:3.10` which would build the image based on [Alpine linux 3.10](https://dl-cdn.alpinelinux.org/alpine/v3.10/). The `RUN echo` commands simply run the echo command and print the message to your terminal as it is building.

>__Note:__ Alpine linux does not use `yum` for package management, it uses `apk`.  If you wanted to update your image you could add `RUN apk update` the **Dockerfile**.

```shell
FROM alpine
RUN echo "building simple docker image."
CMD echo "hello this is your container"
```

Next you want to build the image and then test the image.  Note in the build process how images are created, and removed and your final images is completed and given both an image name and image ID.

>__Note:__ If you want to remove an image use `docker image rm {image id}` but if you want to remove all images not in use then use `docker image rm $(docker image ls -q -a)`

```shell
docker images
docker build -t alpinehello .
docker images
docker run --rm alpinehello
```

![Docker build](image-build.jpg) 

![Docker image](image-label.jpg)

![Docker run](alpinehello-run.jpg)

The `docker run --rm` command tells docker to start the container but remove it once the task is completed.  We created a docker image designed to print out one message `hello this is your container` after which is was shutdown and removed.

>Optional Challenge: Go back to your Dockerfile and have echo print out the an environment variable like `$HOSTNAME`.  To do this edit the Dockerfile and update the CMD line as follows `CMD echo "hello this is your container ${HOSTNAME}`.  Rebuild the container `docker build -t alpinehello .` and then run it again.

## The Vocareum lab

Login to [canvas.uw.edu](https://canvas.uw.edu) select modules and slide down until you see a section titled **AWS Lab* **.
Here we want to open **Lab 1** and then click **"Load Lab 1 in a new window"**.  Next you will be presented with a "Terms and Conditions" page to review and accept.  Click "I Agree" at the bottom of the page once you have completed reviewing the document. 

![Agreement](bottom_agree.jpg)

This will launch the Vocareum portal.  Here you should see a list of buttons one to open.  You need to start the lab.  This lab should run for the rest of the class, we do not want to "End Lab". Once the lab has been started click details to obtain your AWS credentials.

![vocareum portal](vocareum_portal.jpg)

Once you click Details you should see a **Show** clicking this should present your AWS credentials

![Vocareum Details](vorareum_details.jpg)

The Credentials page should include another **Show** button for both the AWS CLI and SSH key.

![Vocareum Credentials](vocareum_credentials.jpg)

Click "Show" to open the AWS account details page.  We want to copy these details into a file called credentials in ~/dockerbuilds/rockyawscli.  We need everything shown starting with `[default]...`

```bash
cd ~/dockerbuilds/rockyawscli/
vi credentials
```

During the docker build process this file will be copied into your container into a folder called .aws in the home directory for the root user.

![aws credentials](aws_cli_credentials.jpg)

>**Note**: Once you start the lab do not end the lab or it will remove your AWS objects.  You may need to Start or essentially re-Start your lab but do not End the lab.  

>**Note**: The AWS credentials used for the credentials file may expire, if so you will have to update this file in the future even though you have not ended your lab.
 

## AWS CLI container

Now we are going to create a container to run the AWS CLI. Before we start we need to be sure time on your dev host is accurate as this is how your containers base their time. Problems with time will prevent your AWS session from authenticating successfully.

>Question: what does `sudo chronyc makestep` do?

```bash
date
sudo chronyc makestep
date
cd ~/dockerbuilds/rockyawscli
```

Now we need to create a new **Dockerfile** in `~/dockerbuilds/rockyawscli`.  Copy and past the following content into this file. Notice that this image is based on RockyLinux version 9.3.  This Dockerfile will add some meta data to the image, install some dependencies, get the latest AWS CLI package and unzip it.  The docker file also installs the AWS CLI and copies in to the image your credentials file.

Review the contents of the file and update the maintainer to reflect your name and email.

```shell
# using a slightly different OS as compared to the class servers.
FROM rockylinux:9.3-minimal
# update to reflect your email address and name
LABEL maintainer="angus1@uw.edu"
LABEL "description"="Container to work with AWS CLI for UW PCE Class Lab"
LABEL version="0.1"
# Update the container to the latest version
RUN microdnf -y update
RUN microdnf clean all
# Install dependencies for the AWS cli
RUN microdnf install glibc -y
RUN microdnf install groff -y
RUN microdnf install less -y
RUN microdnf install unzip -y
# pull down the latest awscli install files
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
# install the aws cli
RUN /aws/install --bin-dir /usr/local/bin --install-dir /usr/local/aws-cli --update
# making the .aws folder required for the credentials file and copy in the credentials data.
RUN mkdir /root/.aws
COPY credentials /root/.aws/credentials
```

Once the file is created we need to build the container.  This may take a minute or two to complete.  If the build fails take note of the error message before it failed.  Keep in mind the tasks are being completed in the container not on your development host.  But if you see errors related to dnf package installs confirm that you have a working dnf client on your development host.  You can do this by running a sample dnf command like `sudo dnf provides "*/dig"`.  If the command fails maybe try to clean up your local dnf cache and configuration files using `sudo dnf clean all`. If the build fails again try to reboot your development server remember you can do this using `sudo reboot`.  You will lose your putty or terminal session but you should be able to connect again very soon.  Once your development system is back online again make sure the time is accurate and update it if not using the **makestep** option for **chronyc** and then restart your build process again.

```shell
cd  ~/dockerbuilds/rockyawscli/
docker build -t rockyawscli .
```

![ docker build ](dockerbuild.png)

Next we need to launch the container and the configure our local client for AWS. The `--td` option tells docker to detach the container and allocate a tty session. 

```bash
docker images
docker run -td --name myaws rockyawscli:latest
docker ps 
```

>**Stretch question**: Where is the AWS credentials file in your container?  Read the Dockerfile to see where you copied this file too.

Now we have our image running and ready to for us to log in.  The `-ti` provides an interactive terminal session to sign in to our docker container.

In the next steps we are going to login to our new RockyLinux based docker container with the AWS cli tool installed. 

```bash
docker exec -ti myaws bash
```

Once you are logged in enter **aws configure** to start the configuration tool.  You can accept the default answers which are based on your credentials file you only need to define your default region which should be **us-west-2**.  

```bash
aws configure
exit
```

>**Note**: To fix any input error just rerun `aws configure` again.

![ docker run ](docker-run-aws-configure.png)

Now we confirm our container is still running and login one more time.

```bash
docker ps -a
docker exec -ti myaws bash
```

>**Note**: what did we just do? we built a RockyLinux 9.x patched it, installed the AWS requirements and imported our own AWS credentials file in seconds. If we ever don't want/like/need this image we can delete it and recreate it at will, isn't that impressive?

Once this is complete we can test the connectivity by requesting a list of all the available AWS regions.  After that run a few commands locally to review your new Rocky Linux(RHEL)9 container.

```bash
 aws ec2 describe-regions --output table
 curl google.com
 cat /etc/passwd
 cat /etc/redhat-release
 exit
```

![AWS CLI regions table](aws_cli_regions.gif)

Now we have a container that is configured with our AWS credentials and configured to connect to AWS console programmatically.  We can stop our container and restart it anytime we want to work manage our AWS environment.

Here we are going to stop our container, confirm it is still on disk, start it back up and test our connection to the AWS console.

```bash
docker ps 
docker stop myaws
docker ps 
docker start myaws
docker exec -ti myaws bash
aws ec2 describe-regions --output table
exit
```

## Login to the AWS Console.

Next we are going to login to the AWS console and quickly view the available services. To do this return to the AWS lab link in Canvas and launch the Vocareum lab. Here click on "AWS".

![vocareum portal](vocareum_portal_aws.jpg)


First make sure you are in the UW West 2 Region.  It is very common for users to create objects in random regions and lose track of them until the bill shows up.

![US West 2](us-west-region.png)

Nex locate the Services option in the top left corner.  

![AWS services](aws_console.png)

Then search for and click on **EC2**.

![aws ec2 console](aws_console_ec2.png)

Now review some of the options listed here. We will be deploying an EC2 server later in the class.

![aws ec2 Landing](aws_console_ec2_landing.png)

## Additional resources for the AWS CLI

* [AWS linux install notes](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-linux.html)
* [AWS CLI user guide](https://docs.aws.amazon.com/cli/latest/userguide/aws-cli.pdf)

### Environment variables

You can pass in your AWS variables using environment variables in place of the credentials file.  However; in general this is a less secure method as they are visible to anyone that has access to your shell.  

```bash
 export AWS_ACCESS_KEY_ID=AKIAJCKHRT25KSLBSH4A
 export AWS_SECRET_ACCESS_KEY=*****************************
 echo $AWS_ACCESS_KEY_ID
 echo $AWS_SECRET_ACCESS_KEY
```

----

### Troubleshooting tips

----

**Q** When using the AWS cli you receive an AuthFailure.  
`
![Auth failure for AWS](aws-authfailure.jpg)

**A.** It is likely that your container time is wrong, this is picked up from your your docker host which is your dev host.  Make sure the time is correct and fif not update it.

 - `sudo chronyc makestep`

If your credentials file needs to be updated you can copy it from your local host to a running container using `docker cp`

```shell
cd ~/dockerbuilds/rockyawscli
docker cp credentials myaws:/root/.aws/credentials
```

----

## Lab Complete

----
