# In Class lab 4

> __Objective:__ Learn to work with Vi or Vim

>__Note:__ It is assumed that you are doing this lab on your development server and **NOT** your production server.

You must open your VMWare player or Fusion application and then start your Development server (**itfdn-rhel8-dev01** or **itfdn-dev01**) in order to work through this lab.

**Mac users**

![Fusion start guest](../fusion-start-guest.png)

**Windows users**

![Player start guest](../vmware-player-guest.png)

___

## Tasks

In this lab we are going to install and run the vimtutor.  If you are comfortable with an alternative command line editor such as emacs or nano then you are welcome to use those editors in place of Vi or Vim.  However, for this one lab I do want to to complete the following tasks.

Install the vimtutor and update vim using yum

```shell
sudo yum install vim-enhanced -y
```

Launch the vimtutor

```shell
/usr/bin/vimtutor
```

![vimtutor](vimtutor.jpg)

To exit vimutor use the following commands

Press \<ESC>, hold down \<SHIFT> + the colon key ":"

Release \<SHIFT> and enter "q" for quit and press \<ENTER>.

If that worked run vimutor again and try to complete Lesson 1 (all six sections) .

>Note: Each time you start vimutor it creates a new file for you.  You can save that file to your home directory and then use **that** version of the vimtutor to continue on. 

![Vimtutor file save](myvimtutor.gif)

## Part 2

Next create a new file using Vim.

```shell
vi newfile.txt
```

![vim default](vi-command.jpg)

Enter "i" for insert mode and then type the following string.

**Text is easy to edit and read**

![vim edit mode](vi-edit.jpg)

This time we are going to save the file.
press <ESC> to exit Insert mode, hold down <SHIFT> + the colon key, enter "wq" for write and quit, finally press \<ENTER>.

>Note: pro tip, you can substitute "x" for "wq" here if you want.

Finally use "/bin/cat" to confirm your file contains the requested string.

```shell
/bin/cat newfile.txt
```

___
Lab complete
___
