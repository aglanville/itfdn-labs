# lab 5 hints

Remember that we are dealing with:

- standard out
- standard in
- standard error.  
  
The file handles for these are 0, 1 and 2 respectively. This means the results you are allowed to see are sent to standard out which is file handle 0.  The error messages are sent to file handle 2 which is standard error.  

Here we are sending the error messages to a file but you are still seeing the results for everything you are allowed to see.  Anything that would generate the message "permission denied" is being printed to standard error which we are sending to the file "/tmp/find_errors.txt". This means you should NOT see these error messages on your screen.

```
find /var -name "*log" 2> /tmp/find_error.txt
```

In this gif you can see a login to a server using ssh from a terminal for **angus@ulc-187.ulcert.uw.edu** .  
Next you can see the find command is run with a redirect of the errors to a file.  
A file is created under /tmp that is called **find_errors.txt."  
Next we use the change directory command "cd" to view the contents in tmp.  
Finally `more` is used to page through the file.  Notice this file has all the "Permission Denied" messages.

![find redirect](find-redirect.gif)