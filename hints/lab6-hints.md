# Lab 6 hints

When modifying existing users don't forget to use the -a and -G if you need to add groups.  If you just use -G the system will remove all groups except the ones you specify.  You can assign multiple groups using a comma separator `usermod -G users,games <USERID`>.

To add Dennis Ritchie to the users and research group run the following commands.

```
sudo useradd -c "Dennis Ritchie" dritchie
sudo usermod -aG users,research dritchie
groups dritchie
```

Remember when adding groups to use the `groupadd` command and then assign those groups to the users with `aG`.

Example of creating groups and modifying existing users.

![usermod and groupadd ](../hints/add-groups.gif)

## How to set a password

Use sudo to set the password, it will prompt for the password and then ask you to confirm the password. When changing your own password without `sudo` you will be asked for the current password first.  This step is not required when using sudo.  The password is stored in a hash form in /etc/shadow.  Before we set the password this file has a blank field where the password is stored.  Can you spot the hash value in this example after the password is set?

```shell
sudo grep kthompson /etc/shadow
sudo passwd kthompson
sudo grep kthompson /etc/shadow
```

Did you see errors related to the password you tried to set?  Because you are root they are not enforced and the password is created even though it does not meet the requirements.

![set password](./set-password.gif)