# Lab 3 hints

1. When working with cut remember it is "Cutting" out a section of the file.  You have to tell cut where to start cutting.  The option "-d" means delimit or delimit by meaning use the argument passed to -d as place of reference.  If we look at the password file we can see that each row is delimited by ":"s and then tell it which row or field to cut out.

So, first look at the file.
next try some cut commands
notice the different output when you use -f1 and -f4 compare this to the contents from the entire file as seen with the `cat /etc/passwd` lines.

```shell
cat /etc/passwd
cut -d: -f1 /etc/passwd
cat /etc/passwd
cut -d: -f4 /etc/passwd
cat /etc/passwd
```

2. Now you know how to cut fields out of /etc/passwd but what if see the duplicate results?  Here we can use a  pipe "|" to connect the output from one command and direct it as the input into another command.  How about sort? now what command could you pipe that into for **unique** values.  The hint there being the word unique....   Oh, -f4 in the following cut command is the wrong field but you can fix that right? Which field shows the shell assigned to each user?  Look at the last field in /etc/passwd for each user and adjust the field value from -f4 to the field number that is associated to the values like /bin/bash or /bin/nologin.

```shell
cut -d: -f4 /etc/passwd | sort
cut -d: -f4 /etc/passwd | sort | wc -l
cut -d: -f4 /etc/passwd | sort | uniq 
cut -d: -f4 /etc/passwd | sort | uniq | wc -l
```

3. One other option might be to use cut -d= These means delimit or define the key and the value based on content separated by the equal "=" sign.

```shell
grep clean_requirements_on_remove /etc/yum.conf | cut -d= -f2
```

4. The date field is number 3

```shell
who | awk '{print $3}'
```

We can do some more data stream manipulation using the `tr` command. We used Awk to print output and separate the data using tabs, now we could use `tr` to translate a tab into a single space using the squeeze option and then cut columns based on spaces.  Odd but fun just to play with a little.

```shell
who | awk '{print NF "\t"  $0}' | cut -c1-2
who | awk '{print $0}' | tr -s ' ' 
who | awk '{print $0}' | tr -s ' ' | cut -d' ' -f1
who | awk '{print $0}' | tr -s ' ' | cut -d' ' -f2
who | awk '{print $0}' | tr -s ' ' | cut -d' ' -f3
```

5. This is a bit harder and here we are trying to use `xargs` to help demonstrate what it can do for us. But start with finding the files from /etc/ and down

```shell
find /etc/ -name "*.conf"
```

Next we can reduce the search to __just__ the /etc/ directory using the -maxdepth option

```shell
find /etc/ -maxdepth 1 -name "*.conf"
```

Now pass the output from find to xargs. Xargs will take the output and use it as an argument for grep. 

```shell
find /etc/ -maxdepth 1 -name "*.conf" | xargs grep example
```

> Note: Did you get some "permission denied" messages this time?  Which command was denied?  maybe use your sudo rights to fix that problem.

```shell
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example
```

If you remove xargs then your grep will be looking at the file names and not the file contents.  None of the file names searched have the word example in the name.  However some of the file names do include for example "sudo" or "lib".

```shell 
find /etc/ -maxdepth 1 -name "*.conf" | grep example
find /etc/ -maxdepth 1 -name "*.conf" | grep sudo
find /etc/ -maxdepth 1 -name "*.conf" | grep lib
```

Now we can return to searching the contents of these files for the word `example`.  We can do this using xargs to pass the file name to grep again and then print just the first column of the files that match this requirement. 

```shell
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' 
```

Now we can clean up the output a bit more and remove the trailing # symbol and any other content other than the file names.

```bash
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' 
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' | cut -d'#' -f1
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' | cut -d'#' -f1 | cut -d':' -f1  
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' | cut -d'#' -f1 | cut -d':' -f1 | cut -d'/' -f3
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' | cut -d'#' -f1 | cut -d':' -f1 | cut -d'/' -f3 | uniq
find /etc/ -maxdepth 1 -name "*.conf" | xargs sudo grep example | awk '{print $1}' | cut -d'#' -f1 | cut -d':' -f1 | cut -d'/' -f3 | uniq -c 
```

6. sed is a stream editor, strange idea but powerful once you get the concept.  We can use -p to print and -d to delete.  But sed is going to print everything by default so we have to suppress output when using the print command to control the output a bit.

```shell
sed 2,3p /etc/passwd
sed 2,3p /etc/passwd | wc -l
sed -n 2,3p /etc/passwd
sed 2,3p /etc/passwd | wc -l
sed '2,5d' /etc/passwd
sed '2,5d' /etc/passwd | wc -l
```

7. This is something you have done now using cut, this time pass it to sort and find an option for reverse.

``` shell
cut -d: -f1 /etc/passwd
cut -d: -f1 /etc/passwd | sort 
cut -d: -f1 /etc/passwd | sort -r
```

8,  This is a chance to play with grep, replace `root` with your username.

```
grep root /etc/passwd
grep root /etc/passwd | cut -d: -f1
```

9.  We can use **wc** to count the lines.  Translate is another interesting tool, it can be used to squeeze out extra white space on in this case change spaces to tabs. In the first example we are using **cat** to print the contents of the file to the screen and then piping them to **tr**.  In the second example we are redirecting the contents of the file gettysburg.txt into **tr**.  We are going to cover redirection in more detail later in the quarter.

```
cat gettysburg.txt | tr -s ' ' '\n'
tr -s ' ' '\n' <gettysburg.txt
```

10. Sed can do this job.  In the first example we are only changing the FIRST occurrence of the to THE.  Using the /g we can change all occurrences to uppercase.  However, did you notice that we are changing **the** when found within an existing word?  How can we fix that?  We can search for \<space> the \<space> so the third example looks good but not perfect, what is wrong?  We are not replacing it spaces around **the**.  See the last example for a better sed solution to this problem.  

```
cat gettysburg.txt | sed 's/the/THE/'
cat gettysburg.txt | sed 's/the/THE/g'
cat gettysburg.txt | sed 's/ the /THE/g'
cat gettysburg.txt | sed 's/ the / THE /g'
```

## End of the hints
