# In class lab: AWS networking

>**Objective**: setup your SSH keys for AWS and define your network.

>**Note**: It is assumed that you are doing this lab on your **development** host and not your production server.

AWS allows customers to create Virtual Private Clouds (VPC) networks within the AWS environment.  Our objective now is to create a VPC and authentication keys for use with our AWS hosted server that will be deployed later in the class. These keys are another set of SSH keys only this time they will be used to authenticate to AWS hosted Linux systems.

## Setting up SSH keys

First we need to login to the AWS console.  This may require opening the AWS lab again and launching the Vocareum portal.  You should already have the lab started and the AWS CLI credentials copied locally.

>Note: Remember do not stop your lab as it will remove all your AWS objects.

![vocareum_portal](vocareum_portal_aws.jpg)

Locate the services option in the top left corner and then using the services search locate EC2 and then select the EC2 option which should take you to the EC2 landing page.

![services search](services_search.png)

>Note: Make sure you are in the US West Region, this is known as us-west-2.

![US West 2](us-west-region.png)

Using the left hand menu scroll down to **NETWORK & SECURITY** and click on **Key Pairs**.

![aws key pair](aws-key-pair.png)

On the next screen click **Create Key Pair** which should redirect you to the **Create key pair** menu. The key pair we generate next will **NOT** be encrypted by default.  

![ create key pair ](create-key-pair.png)

Here we want to name our key pair. I suggest you name this key pair **lab25-key-pair**.  Next select the Private key file format you want.  If you are using a Windows desktop and putty then select **.ppk**. If you have enabled [Windows Subsystem for Linux (WSL)]( https://docs.microsoft.com/en-us/windows/wsl/about), or you have Mac or if you are using Linux then select **.pem**.  Once you have named your keypair and selected the private key format click **Create key pair**.  AWS will generate the key pair on the demand now and attempt to download the private key to your local host. AWS will **NOT** store your private key, if you lose it there is no backup.  The key will not be encrypted by default so be sure to store it in a secure location. Anyone with this private key and knowledge of the default user may be able to login to your AWS hosted servers where the corresponding public key has been stored.

>__Remember:__ If you are using Putty you should select the ppk format to avoid having to convert the key.

![name key](name-key-pair.png)

## Creating a VPC

A Virtual Private Cloud (VPC) is the network into which you will deploy your linux server later.  Return to the Services search menu and search for **vpc**, click on the VPC menu option returned.

![find vpc](find_vpc.png)

This should redirect to the VPC landing page.  On this page ensure you have enabled the **New VPC Experience** and click **Create VPC**.

>Note: Make sure you are in the US-West-2 or **Oregon** Region

![Launch VPC](launch_vpc.png)

Here we are going to define all the values for our single public subnet VPC using the 10.9.0.0/16 network.

* Resources to create: select **VPC and more**
* Name tag auto-generation: check **Auto-generate** and enter your name in the field
* IPv4 CIDR block: **10.9.0.0/16**
* IPv6 CIDR block: select **No IPv6 block**
* Tenancy: set to **Default**
* Number of Availability Zones: **1**
* Customize AZs: **expand**
  * First availability zone: **us-west-2a**
* Number of public subnets: **1**
* Number of private subnets: **0**
* Customize subnets CIDR blocks
   * Public subnet CIDR block in us-west-2a: **10.9.8.0/24**
 * NAT gateways: **None**
 * VPC endpoints: **None**
 * DNS options: select both **Enable DNS hostnames** and **Enable DNS resolution**

Finally click **Create VPC**

![VPC settings](vpc_settings.png)

If there are no issues you should see the Create VPC workflow menu display the progress of your new VPC. Once complete click **View VPC**

![Create vpc workflow](create-vpc-workflow.png)

Now you should be able to view your VPC and inspect the various objects created to support the VPC environment.  Review your Subnets, Internet Gateway and Route table.

![Your VPC](your-vpc.png)

Congratulations! you have just created your first custom VPC.  We could have create two subnets, one Public (includes an internet gateway) and one private (only has a nat gateway to the public subnet).  In this sort of design you might place your customer facing web server in the public subnet and your backend application or database server in the private subnet.  This means the application or database server would not have direct access to the internet and would be a that much safer in general.

## Working with the AWS CLI and SSH keys

Now we are going to use the AWS CLI to create, list and delete SSH keys.  This is just an demo exercise as we already have our keys.  We are going to try and do this using our Docker AWS CLI container.

It is very likely that your session token has expired if you are working on this lab.  Think about the purpose of the credentials file, what is it?  The credentials file is how the aws cli is able to authenticate you to the aws console.  The credentials file includes a session token.  A session token will expire and then it needs to be regenerated.  A session token is created when we login to the AWS console also.  The same concept is used by many websites.  Think about your bank website I suspect that once you login if you don't do anything for a few minutes they will log you out and force you to log back in.  This happens typically because your session token expired and when you log back in you create a new session token.  We do this with Vocareum by clicking **Start lab** and then finding the new session details.  Check the troubleshooting section at the end of this lab if you need more help on this process.

```shell
 docker ps -a
 docker start myaws
 docker exec -ti myaws bash
 mkdir ~/awskeys
 cd ~/awskeys
 aws ec2 describe-key-pairs
 aws ec2 create-key-pair --key-name mynewkey_itfdn --query 'KeyMaterial' --output text > itfdn_key.pem
 aws ec2 create-key-pair --key-name mynewkey_itfdn_g2 --query 'KeyMaterial' --output text > itfdn_key_g2.pem
 cat itfdn_key.pem
 aws ec2 describe-key-pairs --key-name mynewkey_itfdn
 aws ec2 describe-key-pairs
 aws ec2 delete-key-pair --key-name mynewkey_itfdn
 aws ec2 describe-key-pairs
```

## Working with VPCs and the AWS CLI

Next we are going to list, create and delete VPCs using the aws cli.

```shell
 aws ec2 describe-vpcs
 aws ec2 create-vpc --cidr-block 10.0.0.0/16
 aws ec2 describe-vpcs
 aws ec2 delete-vpc --vpc-id vpc-<name>
```

VPC ID example

![aws vpc id](aws-vpcid.jpg)

___
>Additional resources

[Configuring your VPC](https://docs.aws.amazon.com/vpc/latest/userguide/configure-your-vpc.html)

[Amazon EC2 key pairs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)

____

### Troubleshooting tips

___

Q.  When I try to run an aws cli command from my docker container it says "The security token included in the request is invalid" or "Request has expired".

![session expired](aws-cli-request-expired.png)

A. You need to renew your session token.  Return to the Vocareum console and click "Start Lab" again and then click "Show".

Once you have your new credentials details update the credentials file in your **cenawscli** folder.  Next you will need to copy this updated file to your running docker container. You can copy files from your local host to a running container using `docker cp`.

![get credentials](vocareum-new-credentials.png)

Now from your development host run these commands.

```shell
cd ~/dockerbuilds/cenaswcli
docker cp credentials myaws:/root/.aws/credentials
```

Now try again, if it fails still check the date in your container, is it off?  If so update the time on your development host and try again. If that still fails stop your **myaws** container, and restart your development host.  when it is back online check the time again and then restart your **myaws** container and try again.
____

## Lab Complete

____

