# Course: Network and System Administration
## Bonus lab: Creating shortcuts on your Linux host

>__Note:__ This lab is not required to successfully complete the course.  This lab is available as it may help to improve or expand your Network and Linux experience and knowledge

>__Note:__ Complete this work on your UW Linux Development or Production server.

> __Objective:__ Discuss the concepts of creating hard and soft links in Linux.

The objective of this short lab is to create some links often called shortcuts to our script files.  In some cases you may create scripts that you find helpful.  One of those might be your store and recall scripts.  If that is the case this short lab may show you how to make them more accessible in your shell.

As you may know your shell includes a set of default variables.  These variables define things like your host name, user name, home directory and much more.  Another variable is called the PATH variable.  This variable maintains a list of locations that your shell will look for command by default.  We can see the values for this now using the echo command.

```bash
echo $PATH
```

The value of this variable is a list of paths delimited by colons ":".  You should see that one of the pre defined path values is /home/${USER}/bin.  This means by default your shell will look for a folder called bin in your home directory.  This folder does not exist by default but you can add it now.

```bash
mkdir ~/bin
```

Now we can create a shortcut in your bin folder that points to your store and your recall scripts.  We do this using the ln command and the -s option.

```bash
cd ~/bin
ln -s ../scripts/shell/store.sh store
ln -s ../scripts/shell/recall.sh recall
```

![links](links.png)

Now you can run store or recall from anywhere in your shell.  Notice I used store and recall and not store.sh and recall.sh as the extension is not required and why not make it shorter to run the command.

So, now keep adding content to your store.db file and clone this to any new server you create.

The ln or link command allows us to create links to files or directories.  There are two types of links soft and hard.  We have created soft links in this lab.  Soft links are pointers to files and directories.  If you remove the destination file or folder the link will simply point to nothing.  But if you delete the soft link nothing happens to the original file or directory.

Hard links are less common and flexible.  Hard links and sources must exist on the same disk.  Hard links can only be created for regular files.  Hard links are essentially a mirrored copy of the original file or directory.

In general I find soft links to be more flexible and suggest you start there and move to hard link if you find the soft link is not meeting your needs.

Resources:

[Hard links and soft links in Linux explained](https://www.redhat.com/sysadmin/linking-linux-explained)

[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)
___

End of lab

___
