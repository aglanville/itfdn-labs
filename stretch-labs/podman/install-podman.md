# Install Podman on RockyLinux 8 and RHEL8

## Podman overview

Podman is a daemonless container engine for developing, managing, and running OCI Containers on your Linux System. Similar to Docker.  Unlike Docker, Podman does not require a daemon process to launch and manage containers. Podman containers run with the same permissions as the user who launched them. Podman seeks to be a drop-in replacement for Docker CLI.

PodmanCan not build container images. It relies on additional tools such as Buildah for creating images.

## Docker overview

Docker is an open platform for developing, shipping, and running applications. Docker is open source; anybody can check out the source code and review it for themselves. Docker allows users to build new container images, push those images to Docker Hub, and also download those images from the Docker Hub. Docker Hub acts as a container image library, and it hosts container images that users build.

## RockyLinux

Podman is available on Rocky Linux 8 base repository.

```bash
sudo dnf update -y
sudo dnf install podman -y
sudo systemctl enable podman --now
sudo systemctl status podman
podman --version
```

## RHEL8

Podman is included in the container-tools module, along with Buildah and Skopeo Buildah is a client tool for building OCI-compliant container images. Skopeo is a client tool for copying container images to and from container registries. 

```bash
sudo yum module enable -y container-tools:rhel8
sudo yum module install -y container-tools:rhel8
podman --version
```

## Resources

- [Podman.io](https://podman.io/)
- [Install Podman](https://podman.io/getting-started/installation)
- See lab 21 for notes on installing Docker on RHEL and RockyLinux

