# Adding new storage to an existing RHEL based system

>Objective: Add new disk to your system using LVM and the XFS filesystem

>Note: It is assumed that you are doing this lab on your **development** host and not your production server. However in this lab you will be interacting with your **production** and **AWS** host. 

## Overview

We are going to add new disk and partition it using parted.  Next we are going to create Physical Volume that identifies that new block disk.  Once that is in place we will create a new Volume group on the new Phyicsal Volume.  We can add disk to the Volume group later if we wantt and extend our overall diskspace.  Then we are going to create some Logical volumes ontop of the Volume group with and then create a new file system on these logical volumes.

The last step is to mount the disk and then add it to our fstab so that the new disk is mounted during every reboot.


## Discover new disk

The first step is to add a new hard drive or disk space to your system.  Using VMWare, Virtualbox, or physical hardware this is a step required before you start this lab.  In this class an additional drive has already been added to your system.  If you want to add a new disk to your development system go to your vmware console and add the hard disk there.  Once added reboot and your system should automatically detect the new drive at a BIOS level and expose it your Operating System.

Start with checking your existing block storage devices using lsblk.  Which disk do you think is already in use and which one is ready for you to configure and format?

```bash
lsblk
sudo lsblk /dev/sda
sudo lsblk /dev/sdb
```

## Partition the disk using parted

We are going to use parted to manage our disk.  The other common option is fdisk.  Parted is a newer tool and can recongize larger disks, provide more partitions support and is part of the [UEFI framework](https://en.wikipedia.org/wiki/UEFI).

Using parted to list the drives one should show filesystem and the other should be **unrecognized**.

```bash
sudo parted -l
```

![parted list](_images/parted-list.png)

Now we are going to use parted to setup our disk and create one partition.

```bash
sudo parted /dev/sda
mklabel gpt
print
mkpart primary 0% 100%
quit
```

![make partition](_images/parted-make-partition.png)

You can still use fdisk to view paritition details.  You should not see the new partition created on sda called sda1

```bash
sudo fdisk -l
```

## What is LVM

insert details here

## Creating a physical volume

Now we are going to create a new Physical volume using LVM. We should only see one Physical volume to start.  We are going to add a new physical volume.  We are going to create 2 sets or copies of the disk meta data.  If we did have issues with the disk later this could be used to restore the details and having two copies provides some redundancy.

```bash
sudo pvs
sudo pvcreate --pvmetadatacopies 2 /dev/sda1
sudo pvscan
```

## Creating a new Volume group

Currently you should just have one volume group.  We are going to add a second one using the new disk /dev/sda1.  To make it simple we will call this new volume group **vl1**.

>Question: What is the new of your existing volume group?

```bash
sudo vgs
sudo vgdisplay
sudo vgcreate -v vg1 /dev/sda1
sudo vgdisplay
```

## Creating a new Logical group

We can check or existing logical groups and then add our new volume groups.  We are going to create two groups which will later be mounted to two separate locations and used for potentially two different purposes. The first logical group will consume 80% of the available disk and the second group will consume the remaining disk.

>Note: use lvremove and the logical link to remove groups you don't need or want. An example is `lvremove /dev/mapper/vg1-lv1`.

```bash
sudo lvs
sudo lvcreate --name lv1 -l 80%VG vg1
sudo lvs
sudo lvcreate --name lv2 -l 100%FREE vg1
sudo lvs
```

![lv create](_images/lvm-lv-create.png)

## Create the filesystem and mount the new disk

```bash
sudo mkfs.xfs /dev/mapper/vg1-lv1
sudo mkfs.xfs /dev/mapper/vg1-lv2
sudo mkdir /opt/vm
sudo mkdir /opt/storage
sudo mount /dev/mapper/vg1-lv1 /opt/vm
sudo mount /dev/mapper/vg1-lv2 /opt/storage
df -h
```

![mount disk](_images/mount-disk.png)

To make the mounts persistent after a reboot add them to the fstab.

```bash
sudo cp /etc/fstab ~/fstab.orig
```

now edit the fstab file add append the following lines.

```text
/dev/mapper/vg1-lv1 	/opt/vm			xfs	defaults	0 0
/dev/mapper/vg1-lv2 	/opt/storage		xfs	defaults	0 0
```

___

Lab complete

___

