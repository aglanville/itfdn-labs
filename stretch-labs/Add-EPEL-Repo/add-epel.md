# Course: Network and System Administration
## Bonus lab: How to enable the EPEL Repository

>__Note:__ This lab is not required to successfully complete the course.  This lab is available as it may help to improve or expand your Network and Linux experience and knowledge

>__Note:__ Complete this work on your Development, Production or AWS Linux server.

> __Objective:__ This lab will describe how you can enable the EPEL repository on your Linux server.

## Background

The [Extra Packages for Enterprise Linux (EPEL) repository](https://docs.fedoraproject.org/en-US/epel/) provides additional software packages for Red Hat Enterprise Linux (RHEL) and its derivatives like [RockyLinux](https://rockylinux.org/) and [AlmaLinux](https://almalinux.org/) that are not available by default. 

The EPEL repository expands the range of available software, allowing users to access more tools, libraries, and applications than those provided by RHEL’s default package sources. This is particularly valuable for users who need to use open-source software that Red Hat doesn’t officially support or include in their core distribution. 

EPEL is designed to be compatible with the RHEL system, ensuring that the additional packages it provides do not conflict with the core RHEL packages. This compatibility helps maintain system stability and integrity. 

EPEL is maintained by the [Fedora Project](https://fedoraproject.org/), which aims to address the needs of the broader community of RHEL users who require more than what’s available in the official repositories.

How to install EPEL for RHEL8

```bash
sudo subscription-manager repos --enable codeready-builder-for-rhel-8-$(arch)-rpms
sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
```

How to install EPEL for Rocky Linux 8, AlmaLinux, Navy Linux 

```bash
sudo dnf config-manager --set-enabled powertools
sudo dnf install epel-release
```

[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)

---

End of lab

---