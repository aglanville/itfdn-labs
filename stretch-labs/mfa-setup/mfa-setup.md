# Setup and configure MFA (Multi-Factor Authentication) with Duo

We can use [DUO](https://duo.com/) to provide two factor authentication for our linux hosts.  We can enable this for many services such as SSH, login logins and web based authentication such as the cockpit web log in.

## Setup your duo account

Create a new free account with Duo.  This account has limited features and a user base of just 10 but it will cover most of the common use cases.

[duo security](https://duo.com/)

Once you have your account created log in and navigate to Applications -> Protect an Application.  Here enter unix in the search bar and select UNIX Application.

![duo app](images/duo-apps.png)

## Configure your UNIX application

Create a new repo file for duo.

>**Note**: We are using the \ to escape the special meaning of $ as we need the $ character in the config file.

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/duosecurity.repo 
[duosecurity]
name=Duo Security Repository
baseurl=https://pkg.duosecurity.com/CentOSStream/\$releasever/\$basearch
enabled=1
gpgcheck=1
EOF
```

Next import the public cert for duo and install the duo app

```bash
sudo rpm --import https://duo.com/DUO-GPG-PUBLIC-KEY.asc
sudo yum install duo_unix -y
```

Next we need to configure the duo config file found under /etc/duo.  This file will be missing the **ikey**, **skey** and **host** values.


resources:
- https://duo.com/docs/duounix