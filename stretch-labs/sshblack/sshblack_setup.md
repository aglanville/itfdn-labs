# Course: Network and System Administration
## Bonus lab: Automating a deny list for SSH attackers

>__Note:__ This lab is not required to successfully complete the course.  This lab is available as it may help to improve or expand your Network and Linux experience and knowledge

>__Note:__ Complete this work on your UW Linux server.

> __Objective:__ Demonstrate one option to improve your SSH service safeguards.

# SSHBlack

SSHBlack is security monitoring tool for SSH that monitors your logs and watches for questionable login attempts.  The SSHBlack will add the source IP information to a deny list for the protected host.

SSHBlack is written in a combination of Shell and Perl scripts both of which are fairly easy to read and understand.

The github repo can be found here: [https://github.com/OleHolmNielsen/sshblack](https://github.com/OleHolmNielsen/sshblack)
The previous home page can be found here for historical details: [http://www.pettingers.org/code/sshblack.html](http://www.pettingers.org/code/sshblack.html) 

## Install requirements

You need to have git installed and know how to clone a repo but we know how to do that now...

```bash
sudo dnf install git -y
git
```

## How to install

Before we start we can backup our firewalld/iptables rule set and we can compare this later.  Next need to be sure we have git installed.  Next we are going to create a folder under ~/git called github and clone the SSHBlack project into that location.  Find the git clone link on the [github.com site](https://github.com/OleHolmNielsen/sshblack). Then we just need to make the install script executable and run it as root.  

```bash
mkdir -p ~/git/github
cd ~/git/github
git clone <clone repo link>
cd sshblack
chmod 755 install.sh
sudo ./install.sh
```

## Review the configurations and status

check for the status of the process using systemctl and make sure the service is set to auto start on reboot.

```bash
systemctl status sshblack
```

We will come back to crontab and scheduled jobs later in the quarter but for now take a look at the your /etc/crontab file.  You should see something related to sshblack listed.  

![sshblack crontab](crontab-sshblack.png)

Next look at the log file for sshblack. Can you see in the logs any indication yet of a host being blocked?

```bash
ls -la /var/log/sshblacklisting
cat /var/log/sshblacklisting
```

Logroate is another common system function we will look at later in more detail but you can review the configuration put in place by sshblack.

>__Note:__ Here is a short article on logroate if you want more details now [redhat.com/sysadmin/setting-logrotate](https://www.redhat.com/sysadmin/setting-logrotate)

```bash
cat /etc/logrotate.d/sshblacklisting
```

Check the pending list of IPs

```bash
cat /var/lib/sshblack/ssh-blacklist-pending
```

Now we can compare the firewall rule updates using vimdiff.

### Troubleshooting

This install error seems to be based on files already existing. The script is not checking for and trying to install them twice.  I believe you can ignore this warning.

![install error](sshblack-install-error.png)

[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)

---

End of lab

---



