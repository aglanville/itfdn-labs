# Course: Network and System Administration
## Bonus lab: Working with xargs

>__Note:__ This lab is not required to successfully complete the course.  This lab is available as it may help to improve or expand your Network and Linux experience and knowledge

>__Note:__ Complete this work on your UW Linux server.

>__Objective:__ Purpose to the lab is to help provide more understanding and experience with xargs.

Most of the time when we pipe the output from one command to another it is seen as a stream of data.  However, in some situations you might want that stream to be interrupted as a command.  When this is the case one solution is to leverage xargs.  Xargs is able to take information from standard input and converts it to arguments that can be used in a command. Xargs can literally build and execute command lines from standard input.

xargs accepts two inputs:
- stdin (file handle 1): A list of strings separated by whitespace
- on the command line: an incomplete command that requires arguments.

![xargs command line](xargs-cmd-line.drawio.png)

## Tasks

Now we can work with xargs.  In this first example we want to know the disk size consumed by each directory under /.  We can use the xargs to help answer this question using. There are of course other ways to solve this problem but this is one solution.

```bash
cd /
sudo du -sh 
ls | xargs sudo du -sk
ls | xargs sudo du -sk | sort -n
```

xargs can also quickly convert multiple lines of output into a single line. Consider the following example where we create a single line of all the user names found in a passwd file.

```bash
cut -d: -f1 /etc/passwd
cut -d: -f1 /etc/passwd | xargs
cut -d: -f1 /etc/passwd | sort | xargs
```

In the following example we want to insert a comment before each line of output created with the help of xargs.  We can do this using the -I option.  The I option allows us to control where the generated command appears.  To do this we use -I with a string like {} or %% or XX.  Next we can use this string in our output as shown below.

```bash
cut -d: -f1 /etc/passwd | sort | xargs -I XX echo "I found user: XX"
```

We can also use xargs to create files based on the contents of file.  Imagine that we have a file called dates.txt which contains the following lines.

```txt
cd 
jan9.txt
feb4.txt
mar31.txt
apr15.txt
may4.txt
jun21.txt
```

Now our goal is to create new files based on these names.  We can do this cat printing the contents of the file dates.txt to standard out and then passing that list to xargs which will then use it as an argument for touch. Touch is a tool that will create blank files with any given name.  We will do this work in a folder called xargs-demo

```bash
cd
mkdir xargs-demo
cd xargs-demo
echo "jan9.txt" >> dates.txt
echo "feb4.txt" >> dates.txt
echo "mar31.txt" >> dates.txt
echo "apr15.txt" >> dates.txt
echo "may4.txt" >> dates.txt
echo "jun21.txt" >> dates.txt
cat dates.txt
cat dates.txt | xargs touch
ls 
```

There are more features to xargs but this should help to explain a few more of the ways that xargs might help you out.

check out the man page or help details too!

```bash
man xargs
xargs --help
```

[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)

---

End of lab

---