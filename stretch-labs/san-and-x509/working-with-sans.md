# Working with Subject Alternative Names (SAN) and your certificates

This lab will go over some of the details around creating certificates requests and signing certificates with SAN values.

The SAN value lets certificates include additional host name values including IP addresses, sites and common names.  SAN is an extension of the x509 that has been part of the standard for a long time.  Some of the early uses cases for a SAN included the MS Exchange Server in 2007.  Today you often find a SAN value used when a certifcate includes the domain name and the domain name prefixed with www such as:  

washington.edu  
www.washington.edu  
uw.edu  
www.uw.edu  

![uw san](san-example.png)

A SAN can support host names on different base domains such as organizations that might purchase both .com and .io or info domains.

example.com  
example.io  
examle.info  

When using a SAN all of these names could be defined in the same certificate.  Most browsers and applications will suppor the use of a SAN values but in some older mobile devices may not.  In this case it is a good practice to set the common name and one of the SAN attributes to the same value for these devices.

The SAN values can be defined in the Certificate Signing Request (CSR) and in the signed certificate.  The req_extensions put the SAN values into the CSR.  The x509_extensions add the SAN values to the actual certificate file.

## Adding a SAN value to the certificate signing request (CSR)

In this scenario we will generate a CSR that will be provided to a 3rd party signing authority (CA).  We want to include the SAN values in the CSR so we will point to the req extensions. 

Here is a sample openssl configuration file.  Save this file as `mysan.cnf` note we have a line under ` [ req ]` that defined the x509 extensions.

```
[ req ]
default_bits       = 2048
distinguished_name = req_distinguished_name
req_extensions     = req_ext
x509_extensions    = req_ext
[ req_distinguished_name ]
countryName                  = Country Name (2 letter code)
countryName_default          = US
stateOrProvinceName          = State or Province Name (full name)
stateOrProvinceName_default  = Washington
localityName                 = Locality Name (eg, city)
localityName_default         = Seattle
organizationName             = Organization Name (eg, company)
organizationName_default     = UW PCE
commonName                   = Common Name (e.g. server FQDN or YOUR name)
commonName_default           = itfdn140
[ req_ext ]
subjectAltName = @alt_names
[alt_names]
DNS.1   = itfnd140
DNS.2   = ulc-187.ulcert.uw.edu
DNS.3   = angus.ulcert.uw.edu

IP.1    = 192.168.5.2
IP.2    = 192.168.122.130
IP.3    = 192.168.23.100
```

We are using the -reqexts to indicate that we are using additional attributes and referencing the `req_ext` section of the configuration file.

```
openssl req -new -nodes -newkey rsa:2048 -config mysan.cnf -reqexts req_ext -keyout my_private.key -out my_certificate_request.csr
```

Next if we want to confirm the values set in the csr file we can view them using openssl.  Also note that if you view the csr file directly it starts with **BEGIN CERTIFICATE REQUEST**.

```bash
openssl req -noout -text -in my_certificate_request.csr
```

## Adding a SAN value to a self signed-certificate

First we need to create a custom openssl configuration file called `mysan.cnf`

```bash
[ req ]
default_bits       = 2048
distinguished_name = req_distinguished_name
req_extensions     = req_ext
x509_extensions    = req_ext
[ req_distinguished_name ]
countryName                  = Country Name (2 letter code)
countryName_default          = US
stateOrProvinceName          = State or Province Name (full name)
stateOrProvinceName_default  = Washington
localityName                 = Locality Name (eg, city)
localityName_default         = Seattle
organizationName             = Organization Name (eg, company)
organizationName_default     = UW PCE
commonName                   = Common Name (e.g. server FQDN or YOUR name)
commonName_default           = itfdn140
[ req_ext ]
subjectAltName = @alt_names
[alt_names]
DNS.1   = itfnd140
DNS.2   = ulc-187.ulcert.uw.edu
DNS.3   = angus.ulcert.uw.edu

IP.1 = 192.168.1.5
IP.1 = 192.168.3.25
IP.1 = 192.168.100.130
```

In the configuration file update the DNS.X and IP.X values to meet your needs.  There is not hard limit on the allowed number of values but generally keeping it under 100 is a best practice.

```bash
openssl req -x509 -config mysan.cnf -extensions req_ext -nodes -days 365 -newkey rsa:2048 -keyout private_custom.key -out cert_custom.crt
```

Now you can verify the details of your new certificate using openssl at the command line.

```bash
openssl x509 -in <cert file name> -noout -text
```

![san values](san-cert-values.png)

### Creating your own self signed certificate with SAN values.

Follow the steps in the section " Adding a SAN value to self signed-certificate.  During this process when you create a new openssl configuration file update the values to define your dev host name and IP values. Confirm your SAN values are included using the openssl.  Finally update your Nginx webserver to point to these new files ( your private key and public certificate), restart your webserver and test your nginx.conf file from the command line.

![nginx cert update](nginx-cert-update.png)

Now test your certificate from a browser.  You may see an error that you certificate is not trusted or that the browser cannot understand the communication.  If you are using Chrome once you see this error type onto the error page "thisisunsafe" or "badidea".

![unsafe site](chrome-ssl-error.gif)

