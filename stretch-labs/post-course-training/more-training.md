# Additional training recommendations

The following is a list of training available online that is either free or published by [O'Reilly(Safari OnLine)](https://www.oreilly.com/) or [Linkedin Learning](https://www.linkedin.com/learning/).  You should have access to both resources so long as your **UW NetID** is valid.  Look for an SSO login option and use your **UW email address**.  If your UW NetID has been disabled you might find that your local library system has access that you can check out for a limited time.  Alternatively both services offer a free trial period.

If you are considering paying for an online educational resource I suggest that you try out [PluralSight Skills (not Flow )](https://www.pluralsight.com/product/skills). They tend to offer %50 discounts a few times a year so unless you are in a rush take the free trial and then wait for the sale to sign-up.

## Training recommendations

I think this one hour course from O'Reilly is a good short overview of many of the fundamental skills we covered in the first few weeks of the course.

- [Linux Command Line Essentials](https://learning.oreilly.com/videos/linux-command-line/10000DIHV202275/)

Red Hat has a free course targeting new RHEL 8 system administrators.

- [Red Hat Enterprise Linux Technical Overview(RH024)](https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?section=overview)

There is another slightly longer UNIX command line course on Linkedin learning that covers a lot of the fundamentals we covered in class too.

- [Unix Essential Training](https://www.linkedin.com/learning/unix-essential-training/work-with-unix-from-the-command-line)

We have been using GitLab as a single user repository.  We have a good understanding of how to create a repo, how to clone a repo and how to add content.  We have not done much with branches and working with GitLab in a group setting but this course will go over how to use GitLab with a TEAM and take advantage of branching and forking.

- [Learning GitLab](https://www.linkedin.com/learning/learning-gitlab-14539757?u=2284609)

We have worked with HTTP since about week 4.  You have installed the NGINX Web Server on your development, production and AWS Linux servers.  We have also used curl and wget to interact with Web Servers from the command line.  This course will review and expand upon your understanding of HTTP and Web Servers.

- [HTTP Essentials Training](https://www.linkedin.com/learning/http-essential-training/anatomy-of-a-request-header)


## Stretch training

This course will walk you through working with GitLab as a part of your CI/CD pipeline.  The course takes advantage of AWS S3 buckets so there may be some cost to run the labs.  However, AWS will grant you $300 credit for the first year if you sign up for an account.

[Continuous Integration and Continuous Delivery with GitLab](https://www.linkedin.com/learning/continuous-integration-and-continuous-delivery-with-gitlab/learn-continuous-integration-and-delivery-with-gitlab)

We have worked with AWS and touched on some of the core services.  However, if you want to gain a better overall understanding of the AWS platform this course is a good starting point.

[AWS Cloud Practitioner Exam Prep Course 2021](https://learning.oreilly.com/videos/aws-cloud-practitioner/9781803235486)

## One more time with me...

If you are truly are a glutton for punishment you might consider joining me in the fall for the [DevOps with Containers and Kubernetes](https://www.pce.uw.edu/courses/devops-with-containers-kubernetes) course that I teach. Just like this course there will be weekly assignments and the course will move quickly.  I recommend for those with limited hands-on experience they complete the [Network and System Administration](https://www.pce.uw.edu/courses/devops-with-containers-kubernetes) course first. There is some overlap in the courses but we dive much deeper into Git, Containers, Kubernetes and automation.  We leverage the [Google Cloud](https://cloud.google.com/) in place of [AWS](https://aws.amazon.com/), so you will have some experience with two of the largest cloud providers once you have completed both courses.

___
End of Guide
___
