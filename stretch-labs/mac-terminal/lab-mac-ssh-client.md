# Course: Network and System Administration

> __Objective:__ Details on how to sign into your Linux servers using a username and password from a Mac client.

## Tasks

When trying to connect to your Development or Production Linux servers from your mac you can use the default terminal application.  One way to find this application is using the finder application selecting Applications, entering terminal in the search box and selecting the Applications filter.  This should display the Terminal application.

![Terminal app](find-terminal.png)

If you are going to sign in to your development server you need to know your username and the password you created during the install.  The command to login is ssh USERNAME@IP-ADDRESS.  Your IP address will likely be something like 192.168.X.X where x is replaced by a value between 0-255.

![development server](development-server-login.png)

If you are going to sign in to your Production server you need to find your server domain name, the username which is **itfdn140** and your unique password value.

![Production server](prod-server-login.png)

A free alternative to the default Mac terminal is [iTerm2](https://iterm2.com/).  This application provides a few more features and functions that you might find beneficial.

![iTerm2](iterm2.png)


[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)

---

End of lab

---