# In Class lab: Installing Prometheus

>Objective: Work with advanced permissions to view, set and update settings.  View, manage and restore ACLs

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

We need to create a user, create an application folder, change ownership and then download the install package

```
sudo useradd -s /bin/bash -c "Prometheus Monitoring" prome
sudo mkdir /opt/prometheus
sudo chown prome:prome /opt/prometheus
sudo su - prome
cd /opt/prometheus
wget https://github.com/prometheus/prometheus/releases/download/v2.23.0/prometheus-2.23.0.linux-amd64.tar.gz
# tar -xvzf prometheus-2.23.0.linux-amd64.tar.gz -C /opt/prometheus/prom-2
# cd promtheus-2
# cp prometheus.yml prometheus.yml.orig
```

Next we need to create our new systemctl service

```
[Unit]
Description=Prometheus Server
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target

[Service]
User=prome
Group=prome
Type=simple
Restart=on-failure
ExecStart=/opt/prometheus/prom/prometheus \
  --config.file=/opt/prometheus/prom/prometheus.yml  \
  --storage.tsdb.path=/opt/prometheus/prom/data

[Install]
WantedBy=multi-user.target
```

Now we want to load the service, start it and check the status

```
sudo systemctl daemon-reload
sudo systemctl start prometheus
sudo systemctl status prometheus
```