# Installing Terrform on RHEL8 for KVM

Terraform is a [Hashicorp](https://www.terraform.io/) project.

First install yum-utils which will provide yum-config-manager and then grab a copy of the hashicorp repo.  Once this is in place you should be able to use yum/dnf to install terraform and verify the install.

```bash
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
sudo yum install terraform -y
terraform version
```

