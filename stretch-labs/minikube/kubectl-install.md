# Installing kubectl

>objective: install kubectl on your Windows or Mac desktop

>__Note:__ We are going to install Kubectl on our host system, this means your Windows or Mac desktop.  This is __not__ something we install in our dev or prod linux systems.

Select either the Windows or macOS link to download kubectl.  In both cases you will be using `curl` to copy the file to your local system.

![download kubectl](kubectl-download.png)

__Windows users__ When you download the binary kubectl file you will then have to manually move it something like "C:\Windows\System32\" and you may want to rename the file and remove the .exe.  If not you will have to enter kubectl.exe when you use the tool.  To move kubectl into the System32 folder may require that you open a command prompt as the Administrator.

![cmd as admin](cmd-admin.png)

__Mac OS users__ After you download kubectl you will need to make it executable and move it something in your PATH like /usr/local/bin. 

```shell
chmod 755 kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
```

![kubectl macos](kubectl-mac.png)