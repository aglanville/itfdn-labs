# minikube tasks

First we are going to wipe your minikube based k8s environment and reset it to blank. We are also enabling the `ingress` add-on again.  This is based on the [Nginx-ingress](https://github.com/kubernetes/ingress-nginx) project and included with Minikube by default.

```
minikube delete
minikube status
minikube start
minikube addons enable ingress
```

Next we are going to deploy to containers from [Hashicorp](https://www.hashicorp.com) called [http-echo](https://github.com/hashicorp/http-echo).  This container will return an html page with any text provided when it is started up.  We are going to use `kubectl` to create a couple of pods with this container deployed.  To do this will need to create a file called **echo-deploy.yaml** with the following content.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: itfdn-echo
spec:
  selector:
    matchLabels:
      app: itfdn-echo
  replicas: 1
  template:
    metadata:
      labels:
        app: itfdn-echo
    spec:
      containers:
      - name: itfdn-echo
        image: hashicorp/http-echo
        args:
        - "-text=Hello from your kubernetes deployed application, using a Nodeport for Ingress"
        ports:
        - containerPort: 5678
```

Review the lecture notes and then review this file to see if some of the values are more relatable.  We are expecting to find 1 pods because in this file we are setting the replicas value to 1.

Now we are going to check the current environment for any pods and then deploy our echo.  When we use the **-A** argument we are checking all namespaces, without this option we will only see objects in the default namespace. We are deploying our objects to the default namespace.

```shell
kubectl get deployments
kubectl get deployments -A
kubectl get pods
kubectl get pods -A
kubectl create -f echo-deploy.yaml
kubectl get pods -A
```

Now we need a service deployed to expose our pods, we need something that will route traffic to either POD.  In this case we are going to use a NodePort service.  To do this create another file called **echo-svc.yaml** with the following content.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: echo-svc
spec:
  type: NodePort
  ports:
  - port: 80
    targetPort: 5678
  selector:
    app: itfdn-echo
```

Now we need to deploy our service to the kubernetes cluster.  Again this will be deployed to the default namespace but you can review all namespaces using -A if you want.  When we review the details we will see the IP address and port used.  But, because we are using Minikube as our Ingress provider we will ultimately use the Minkube IP address to access the resource.  If we we working with AWS then it would be an email address they provided and not Minikube.  

```shell
kubectl get svc
kubectl create -f echo.svc.yaml
kubectl get svc
minikube service echo-svc
```

>Note: We are using minikube to launch the test page but note the target port in your browser url and match that to the values displayed when viewing the output from `kubectl get svc`.

Next we are going to update our deployment and confirm the changes.  In this example we are just going to change the text in our **echo-deploy.yaml** file.  We are just updating the text that will be returned by the echo server.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: itfdn-echo
spec:
  selector:
    matchLabels:
      app: itfdn-echo
  replicas: 2
  template:
    metadata:
      labels:
        app: itfdn-echo
    spec:
      containers:
      - name: itfdn-echo
        image: hashicorp/http-echo
        args:
        - "-text=Updated echo deployment application; K8s will deploy 2 new PODs and terminate the existing POD."
        ports:
        - containerPort: 5678
```

To push this update we will use the `apply` kubectl directive.  Are the updated is initiated review the PODs in your cluster and confirm that you see two new PODs being created.

>Note: We are expecting to find 2 pods this time because in this file we are setting the replicas value to 2.

```
kubectl get pods
kubectl apply -f echo-deployment
kubectl get pods
minikube service list
minikube service echo-svc
```