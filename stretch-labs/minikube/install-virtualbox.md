# Installing Virtualbox

>Objective: Install virtualbox on Windows or a Mac

The VirtualBox install page is here: https://www.virtualbox.org/wiki/Downloads 

>__Note:__ We are going to install VirtualBox alongside VMWare player on our host system this is your Windows or Mac desktop.  This is __not__ something we install in our dev or prod linux systems.

The first step requires downloading either the Windows or Mac installer, click the links under VirtualBox 6.1.22 platform packages.

![virtalbox installer](virtualbox-downloads.png)

Next you should be able to accept all defaults.  There is no need in this lab to login to the Virtualbox console.  When we install minikube it will do all that for us as part of the install.  So, just accept the defaults and then start to download kubectl to your desktop.

[Return to Kubernetes and Minikube lab](https://www.ulcert.uw.edu/itfdn-labs/lab28/minikube.html)

___
___