# Kubernetes and minikube

>Objective: Install minikube locally to provide a test environment for simple Kubernetes testing.

In this lab we are going to install another embedded hypervisor, virtualbox.  Other hypervisors are supported but Virtualbox is the best supported platform for this environment.    We will have to load three components locally.  Once installed we will start minikube from the command line and it will launch the Virtualbox instance of minikube.  You should not try to start, stop or delete minikube from within __Virtualbox__.

* First we need to install Virtualbox, find the install notes for your platform, Windows or Mac here: [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

  * Additional notes on [installing VirtualBox](https://www.ulcert.uw.edu/itfdn-labs/lab28/install-virtualbox.html) 

* Next we are going to install the kubectl command on our local machine, follow the install guide for your platform, Windows or Mac here: [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

  * Additional notes on [installing kubectl](https://www.ulcert.uw.edu/itfdn-labs/lab28/kubectl-install.html) 

* Finally install Minikube on to your local platform, Windows or Mac here: [Minikube](https://github.com/kubernetes/minikube/releases)

Once this is complete you can start, stop and delete your Minikube instance from the command line.

>Note Windows users: The file extensions are often hidden but required at the command line so you may need to use **minikube.exe start** for example.  Also you may need to start the windows cmd prompt with administrative rights.o

Running minikube with no options is the same as running minikube with the --help option.  The result is summary of the available commands to use with minikube.  You can take this one step further using minikube a primary directive and then use the --help option for additional details about feature.

```shell
minikube
minikube --help
minikube delete --help
```

Now we can start minikube and then check the status once it completes the start up process.

```shell
minikube start
minikube status
 ```

If you want to start over with minikube and remove anything and everything you might have installed the run stop and delete as shown below.

```
 minikube stop
 minikube delete
```

The minikube install may take 10 minutes or so to get going. While you wait start playing with this online environment: [Learn Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics)

## Once minikube is up.

You should see an all clear message like the one shown below.

![minikube](minikube-running.jpg)

Now we are going to check on your minikube local IP.  Then we will review some of the environment configurations. Finally we will launch the kubernetes dashboard. 

```
minikube ip
kubectl config view
kubectl get pods
kubectl get pods --all-namespaces
kubectl get services
kubectl get nodes
minikube dashboard
```

![kube dashboard](kube-dashboard.jpg)

Now close the dashboard session using "ctrl+c" on your keyboard. 
Next we are going to deploy "hello world" for minikube and then relaunch the dashboard.

We are enabling the addon "ingress" which routes traffic to our nodeport using the minikube IP address.  The concept of an ingress is one used by many organizations to route traffic to their kubernetes deployments but the IP is provided by the service provider such as AWS, Azure, GCE etc.

```
minikube addons enable ingress
minikube addons list
kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.10
kubectl get pods
kubectl get pods -A
kubectl get deployments
kubectl get services
kubectl expose deployment hello-minikube --type=NodePort --port=8080
kubect get services
minikube services list
minikube services hello-minikube
minikube dashboard
```

Kill the dashboard again "ctrl+c"

Now let's grab some kubernetes cluster details.

```
 kubectl cluster-info
 kuberctl cluster-info dump
 kubectl get 
 kubectl get endpoints
 kubectl describe service/kubernetes 
 ```
 
 If or when you want to delete the hello-minikube application then you need to delete the deployment.

```
 kubect get deployment hello-minikube
 kubect delete deployment hello-minikube
```

Once you are done playing with kubernetes in your minikube environment stop the service.

```
 minikube stop
```

>Note: Remember if you want to start fresh then run `minikube delete`

## Troubleshooting and other tips

**Q.** I am running out of memory in my kubernetes environment how can I increase this?

**A.** From the command line stop minikube, reconfigure the guest and restart minikbe.  This example set the RAM to 8Gb, but you could turn it down to 2Gb.

```
minikube stop
VBoxManage modifyymv "minikube" --memory 8092
minikube start
```

**Q.** I am not able to connect to my nodeport service even though it shows as configured and healthy.

**A.** delete and redeploy your deployment and make sure the ingress addon is enabled.  It seems that after restarting minikube services often stop functioning as expected.

```
kubectl delete deployment
kubectl get deployments
kubectl create deployments
```

## lab complete.