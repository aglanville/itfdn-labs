## In class Lab1 B
> Objective: Work with some of the common commands just discussed in the lecture.  Learn to use an SSH connection and avoid using the VMWare console to login to your development host.

Login to your development CentOS 7 guest using your user account, not root.  If you are using a MAC this means opening your terminal client or if you are using a Windows host this means opening your [Putty SSH Client](https://the.earth.li/~sgtatham/putty/latest/w32/putty.exe).  Remember don't login as Root.  

Here is an example login from a MAC client as the user **angus**

![SSH connection as normal user](simple-ssh-connection.png)

Once logged in run the following commands

```
pwd
ls
cd /
ls
pwd
cd 
pwd
echo hello       world
echo "hello      world"
wc /etc/passwd
mkdir /tmp/TEST
mkdir /tmp/TESTWAIT
rmdir /tmp/TEST
rmdir -i /tmpTESTWAIT
touch fakefile.txt
ls 
mv fakefile.txt newfile.txt
ls
man man
man ls
cat /etc/passwd
wc /etc/passwd
```
Here are some sample commands from the list above

![example ls command](simple-ls.png)

![example echo](simple-hello_world.png)

### Create another local account.
> Here the objective is to create a new user that is a member of the group admin.  The group admin is something you will also create.

First we need to create a group called admin
```
sudo /usr/sbin/groupadd admin
```

Next create a local account for the user Dennis Ritchie

```
sudo /usr/sbin/useradd -c "Dennis Ritchie"  -G admin dritchie
```

next we are going to set a password for this new account.

```
sudo /usr/bin/passwd dritchie
```

Here is a screenshot of what it would look like to create a new account for the user Student One with a userid of student1 and set the password.

![Add user](add_user.png)

### Directory structure review
> The objective here is to discover some of the directories and files on your system as you become more comfortable at the command line

Open up two SSH sessions to your development host and place them side by side.

Change directories (cd) to root (/) in the first session and list (ls) the contents.
Change directories (cd) to usr (/usr) in the second session and list (ls) the contents.
Compare these two windows, do you see duplicate directories? Where can find log files on your system?

Next we are going to review some more of the commands discussed in the lecture.

* use the "who" command to determine which account you are logged in as.
* use the "whoami" command to confirm this.
* use "who am i" now and review the difference in the output.

Now using sudo become the root user
```
sudo su -
```
now repeat the who , whoami and who am i commands.  What differences do you see?

Next compare the differences between **cat** and **echo**

```
/bin/cat /etc/passwd
/bin/echo /etc/passwd
```
Is the output different and if so consider why and the difference between the two commands.

### Navigating the filesystem

* Using **cd** move to /var/log. 
* Use /bin/pwd to confirm your working or current directory.
* Enter **cd** with no additional options and use /bin/pwd to reveal your location. 
* Finally enter cd followed by a dash "-" and then /bin/pwd.
* Did your working directory change and if so to where?

Using **mkdir** create a directory under /tmp called myTestDir

``` 
/usr/bin/mkdir /tmp/myTestDir
```

Confirm it exists using the /bin/ls and then remove the directory using **/usr/bin/rmdir**

The /bin/touch command is another simple utility.  The purpose of this command is to change file timestamps but it will also create empty files.  Using touch now create an empty file under tmp called testDel.txt

``` 
/usr/bin/touch /tmp/testDel.txt
```

Now remove the file
```
/usr/bin/rm -i /tmp/testDel.txt
/usr/bin/rm --help
```
What did the -i option do? what does the --help option do?

### pty and tty sessions
Noting the different types of logins available with your development and production servers.
login to your development host using putty or your MAC terminal via SSH. Next login using the VMWare console, using who list the active connections on your host.  Can you tell which one is connected using SSH and which one was established from the VMWare console (this simulates a local login).


