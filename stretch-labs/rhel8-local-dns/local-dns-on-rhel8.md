
# DNS resolution on Red Hat 8

In Red Hat Enterprise Linux (RHEL) 8, the /etc/resolv.conf file pointing to 127.0.0.1:53 indicates that the system is configured to use a local DNS resolver running on the localhost

A local DNS resolver service systemd-resolved, is running on your system and listening on the loopback address 127.0.0.1 at port 53, the standard port for DNS queries.

systemd-resolved:

In many modern Linux distributions, systemd-resolved is responsible for network name resolution. It acts as a caching DNS stub resolver and handles DNS queries from local applications, forwarding them to upstream DNS servers as needed.
The `/etc/resolv.conf` file typically points to `127.0.0.1` to use this local resolver.
Configuration of systemd-resolved is managed through the systemd configuration files and settings, often found in /etc/systemd/resolved.conf.

## How It Works

1. DNS Query Flow:
When an application needs to resolve a domain name, it sends a DNS query to the address specified in /etc/resolv.conf. In this case, it sends the query to 127.0.0.1:53.
The local resolver (systemd-resolved or dnsmasq) receives this query.

2. Processing the Query:
The local resolver checks its cache to see if it has a recent answer to the query. If so, it responds with the cached result.
If the answer is not in the cache, the local resolver forwards the query to the upstream DNS servers, which are defined in its configuration.

3. Receiving the Response:
The upstream DNS servers process the query and return the result to the local resolver.
The local resolver caches this result and sends it back to the application that made the initial query.

How to check the configuration

To check the status and configuration of systemd-resolved:

```bash
# Check the status of systemd-resolved
systemctl status systemd-resolved

# View the current DNS servers and configuration
resolvectl status
```
To configure systemd-resolved, edit the /etc/systemd/resolved.conf file. Here’s an example snippet:

```text
[Resolve]
DNS=8.8.8.8 8.8.4.4
FallbackDNS=1.1.1.1 1.0.0.1
Domains=example.com
DNSSEC=yes
Cache=yes
```

After making changes, restart the service:

```bash
systemctl restart systemd-resolved
```


