# Stretch assignment lab:  Working with Access Control Lists (ACLs) 
>Objective: Work with systems processes including how to background and foreground a process.  Start and stop services using SystemD and explore the concept of run-levels and targets. 

Standard permissions do not allow for more than one user or group to own a file or folder.  This is where using an ACL can make the difference.  Typically we can only set one user, one group and then define what level of access Other is granted. 

![Standard permissions](standard-permissions.jpg)

### ACL utilites

There are a few new utlities for use with ACLs.  Typically ACLs are installed by default on RHEL/CentOS 7


* getfacl - get file access control lists
    * getfacl -a file.txt # list all ACL details.
    * getfacl -d file.txt # list default ACL details
* setfacl - set file access control lists
    * setfacl -m user:username:XXX file.txt # replace XXX with R, W, X as required
    * setfacl -m u:username:XXX file.txt # shortcut for user is u 
    * setfacl -m group:groupname:XXX file.txt # replace XXX with R, W, X as required
    * setfacl -m g:groupname:XXX file.txt # shortcut for group is g 
    * setfacl -R -m user:username:XXX file.txt # add -R to apply permissions recursively.
    * setfacl -d -m user:username:XXX file.txt # add a default permission that will apply to all files created in that directory in the future.
    * getfacl -c ~/acl-lab/file5.txt > file5-acls.txt # get ACL values and write them to a file for importing or restoring later.
    * setfacl -M ~/acl-lab/file5.txt file5-acls.txt # set ACL values from a file

## Listing ACLs

You can list the ACLs on a given file using getfacl and you can also redirect the output to a file.

```
mkdir ~/acl-lab
touch ~/acl-lab/file{1,2,3}.txt
getfacl ~/acl-lab
getfacl ~/acl-lab > ~/acl-lab/acl-lab-dir-default-acl.txt
cat ~/acl-lab/acl-lab-dir-default-acl.txt
```

## Setting ACLs on a directory and a file

You can set an ACL for the user, the group or as a default ACL for all users.  You can set the ACL permissions on a directory but you an also set them on all files in a directory using the "recursive" switch (-R).  Once you have an ACL set on a file of a directory you will note a plus "+" sign when using a long listing on a file or directory. In this example you can see where the user kthompson has been given read (r) access to the file default.db.  Also notice that the facl list includes "mask", this is the maximum permission allowed for that file for folder, more on this later.

![set ACL on a file](setfacl-file.jpg)

Now we are going to create a new user, a new group and then apply this user and this group to the acl-lab directory and file1.txt in the directory. Note when you print out the ACL settings for the folder and directories the differences between file1.txt and the other text files.  Which ones have a mask value? Which ones have an entry for kthompson and acl-research? 

We can set default ACL values for a user or for all users with the -d option. Using u:gvrossum:rx ~/acl-lab will result in any new file being created having a default ACL set granting Guido Read and Execute permissions.  We can also set default group permissions using the group:**groupname** syntax.

```
sudo useradd -c "Ken Thompson" kthompson
sudo useradd -c "Guido van Rossum" gvrossum
sudo groupadd acl-research
setfacl -m user:kthompson:rwx ~/acl-lab
setfacl -m u:gvrossum:r ~/acl-lab/file1.txt
getfacl ~/acl-lab/file1.txt
setfacl -R -m u:gvrossum:r ~/acl-lab
getfacl ~/acl-lab/file2.txt
```

>Note: Here we are adding default ACL values to the acl-lab, review the output from getfacl ~/lab-lab to confirm the group default ACL. Next pay attention to the ACLs set on file4.txt that was just created. Keep in mind that default ACLs only apply to directories.

```
setfacl -d -m group:acl-research:rwx ~/acl-lab
setfacl -d -m user:gvrossum:r ~/acl-lab
getfacl ~/acl-lab/ | grep default:group
getfacl ~/acl-lab/file2.txt
touch ~/acl-lab/file4.txt
getfacl ~/acl-lab/file4.txt | grep gvrossum
setfacl -R -m group:kthompson:rx ~/acl-lab
getfacl  ~/acl-lab
getfacl  -R ~/acl-lab
```

## Deleting an ACL for a file or folder.

You may need to delete an ACL on a file or folder.  The switch to delete an ACL is different if you want to delete an ACL or default ACL or if you just want to delete all ACLs.  Use the -x to remove an explicit ACL, use the -k to remove all default ACLs and use -b to simply remove all ACLs. We can add the -R to recursively remove the ACLs from a directory and files.

```
setfacl -m user:kthompson:r ~/acl-lab/file2.txt
setfacl -m u:gvrossum:rwx ~/acl-lab/file2.txt
getfacl ~/acl-lab/file2.txt
setfacl -x u:kthompson ~/acl-lab/file2.txt
getfacl ~/acl-lab/file2.txt | grep user
setfacl -x u:gvrossum ~/acl-lab/file2.txt
getfacl ~/acl-lab/file2.txt | grep user
```

Remember that Default ACLs only impact the directory.  So, we are now going to review and remove default ACL values there.

```
getfacl ~/acl-lab | grep default
setfacl -k ~/acl-lab
getfacl ~/acl-lab | grep default
getfacl -R ~/acl-lab
setfacl -R -b ~/acl-lab
```

## ACL tricks and tips
A few tips related to working with ACL files.  The first one is that the umask ACL value set on a file or directory is the max value that any user or group and be assigned.  Using this you can immediately impact users access without changing the actual ACL values.

![ACL mask setting](acl-mask.jpg)

You can set this using the keyword mask.  To remove all access quickly set the mask to - removing all effective rights.

```
touch file ~/acl-lab/file5.txt
getfacl ~/acl-lab/file5.txt
setfacl -m user:kthompson:rwx ~/acl-lab/file5.txt
setfacl -m g:acl-research:rwx ~/acl-lab/file5.txt
getfacl ~/acl-lab/file5.txt
setfacl -m mask::- ~/acl-lab/file5.txt
getfacl ~/acl-lab/file5.txt | grep mask
setfacl -m mask::rwx ~/acl-lab/file5.txt
getfacl ~/acl-lab/file5.txt | grep mask
```

To write all existing ACL values to a file which can be used to set the values in mass later use the -c option

```
getfacl ~/acl-lab/file5.txt 
getfacl -c ~/acl-lab/file5.txt > file5-acls.txt
setfacl -b ~/acl-lab/file5.txt 
getfacl ~/acl-lab/file5.txt 
setfacl -M acl-lab-acl-settings.txt file5.txt
getfacl ~/acl-lab/file5.txt 
```