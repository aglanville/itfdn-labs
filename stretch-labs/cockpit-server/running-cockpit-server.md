# RHEL cockpit server setup

RHEL provides the cockpit service to expose a web based admin tool for the given host.

## Updating the SSL certificates for cockpit

The default self signed certs can be found under /etc/cockpit/ws-certs.d/.

Copy your new certificates to the same folder.  Make sure they are text file based (PEM) and not encrypted. The public cert file name should end in cert or crt.  The private certificate file should end in .key. Both files should have the same name such as host1.crt and host1.key.

If you have used letsEncrypt to create your certs then copy those files fullchain.pem and privkey.pem to the /etc/cockpit/ws-certs.d.  You can check to make sure the names and permissions are good using.

```bash
sudo cp /etc/letsencrypt/live/ul-kvm1.ulcert.uw.edu/fullchain.pem {server-name}.crt
sudo cp /etc/letsencrypt/live/ul-kvm1.ulcert.uw.edu/privkey.pem {server-name}.key
sudo /usr/libexec/cockpit-certificate-ensure --check
sudo systemctl restart cockpit
```

Then test the site, be sure to load a new page using <shift> enter or spawn a new browser window or you may see the cached self signed cert returned.

## Managing virtual machines with cockpit

```bash
sudo yum install cockpit-machines
```

## Enable MFA with DUO for cockpit

### Prerequisites

- You already have setup a duo free account
- You have already enrolled your admin user
- You have already added the basic unix app config file

Confirm that you have the `/etc/duo/pam_duo.conf` file created with the correct values from the duo admin page.

![duo config](images/pam_duo-config.png)

For more details on setting up MFA with DUO see [mfa-setup](../mfa-setup/mfa-setup.md)

Create a new repo file for duo.

>**Note**: We are using the \ to escape the special meaning of $ as we need the $ character in the config file.

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/duosecurity.repo 
[duosecurity]
name=Duo Security Repository
baseurl=https://pkg.duosecurity.com/CentOSStream/\$releasever/\$basearch
enabled=1
gpgcheck=1
EOF
```

Next import the public cert for duo and install the duo app

```bash
sudo rpm --import https://duo.com/DUO-GPG-PUBLIC-KEY.asc
sudo yum install duo_unix -y
```

Next backup the cockpit file found under `/etc/pam.d/`.

Next add a new line as shown below to the cockpit file.

```text
auth       required     /usr/lib64/security/pam_duo.so
```

![pam cockpit](images/pam-cockpit.png)

Now restart the cockpit service

```bash
sudo systemctl restart cockpit
```

If SELinux is enabled update the policy.

```bash
sudo ausearch -c "cockpit-session" --raw | audit2allow -M cockpit-session-duo
sudo semodule -X 300 -i cockpit-session-duo.pp
```

## To enable ssh login with MFA

Backup the existing pam file password-auth 

```bash
cd /etc/pam.d/
sudo cp password-auth password-auth.orig
```

Next and update the password line and change sufficient to requisite and add new line for duo as sufficient.

```text
password    sufficient    /usr/lib64/security/pam_duo.so
```

![password auth](images/pam-password-auth.png)

Resources:

- https://www.youtube.com/watch?v=5cchlzMDXPs
- https://www.popebp.com/cockpit-duo-security-2fa/ 