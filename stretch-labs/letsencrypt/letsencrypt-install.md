# Enable HTTPS using LetsEncrypt

This guide is based on RHEL 8 systems.

## prerequisites

It is assumed you have nginx installed and listening on port 80 with the firewall allowing traffic on ports 80 and 443.  The following is a quick run through of the commands to install nginx, start the server and expose the http and https ports.

```bash
sudo yum install nginx -y
sudo systemctl enable nginx --now
sudo firewall-cmd --add-service=http --permanent --zone=public
sudo firewall-cmd --add-service=https --permanent --zone=public
sudo firewall-cmd --reload
```

Start with installing and starting the snapd service.  We have to add a soft link to support snapd also.

>Note: The snapd package is part of the epel repo

```bash
sudo yum install -y epel-release
sudo yum install snapd -y
sudo systemctl enable snapd --now
sudo ln -s /var/lib/snapd/snap /snap
sudo snap install core; sudo snap refresh core
```

Now we can install the letsencrypt snap

```bash
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

Now we can request a cert for our host, using the -d if you want to have multiple SAN (subject alternative names) values.

```bash
sudo certbot --nginx -d ulc-XXX.ulcert.uw.edu -d {secondary custom name}
```


