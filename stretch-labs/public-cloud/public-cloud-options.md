# Public Cloud options

Your access to the AWS cloud using your UW NetID will end about 3 weeks after the last class session.  However, if you want to continue working in the Public cloud you can sign up for free AWS account.  But there are some other free and inexpensive Public cloud options.

Before discussing some of the options it is also valid to use your local embedded hypervisor solution for testing and working with Linux systems.  You may have installed VMWare Player or Fusion both of which will allow you to run additional linux server instances.  Mac users with M1 or M2 chips should have UTM installed and can also install additional linux servers.  Other embedded hypervisors options include [VirtualBox](https://www.virtualbox.org/), [Parallels](https://www.parallels.com/) and [QEMU](https://www.qemu.org/).


## [AWS](https://aws.amazon.com/free/)

You can sign-up for a free AWS account that will include a $300 credit.  Once this credit expires there are some services that will remain alway free but an EC2 instance is not part of this plan last I checked.

It is worth noting that I have found running the RHEL based systems more expensive with AWS than running AWS linux and Rocky Linux.  Which is just to say keep an eye on your RHEL based systems and the costs.  I find I can run a t2.small based Rocky Linux for a month for less than $20 for reference.  Most students in the class never exceed $15.

![lab costs](lab_costs.png)

![uptime](uptime.png)

## [Google Cloud](https://cloud.google.com/free)

The Google cloud offers something similar to AWS with a $300 credit for new users.  Google of course offers one of the best if not the best Kubernetes in the cloud experience.  Google does offer some compute engine (same concept as the AWS EC2 offering) in the always free tier but not enough to keep an instance running non-stop all month.

## [Oracle Cloud Infrastructure (OCI)](https://www.oracle.com/cloud/free/)

Another choice is the Oracle Cloud, they offer a free month of access and then they have an always free tier.  The always free tier includes the ability to run two AMD based VMs.  I have one these running for a while and it has been fine.  This is a good option if you want a linux server in the cloud with a public IP address.

![Oracle vm details](oracle-vm.png)

## [DigitalOcean](https://www.digitalocean.com/try/free-trial-offer)

DigitalOceans also has a free trial available.  Some of my co-workers use DigitalOcean and find it to be very stable and cost effective.  You can run what they call a droplet which is similar in concept to an AWS EC2 instance.  The [smallest droplet](https://www.digitalocean.com/pricing/droplets) includes 512mb of Ram and 1vCPU for $4.  You can run RockyLinux, RHEL, or pretty much any other Linux instance on this base.

 Anyway, I hope that helps you as you start to explore the Public Cloud space.


 ___

 End of doc

 ___


