# working with SELinux

Moving a file from your home directory to your web server root using sudo will likely result in SELinux issues.


```bash
type=AVC msg=audit(1681542543.159:2376283): avc:  denied  { read } for  pid=473789 comm="nginx" name="rhel-8.5
-x86_64-boot.iso" dev="dm-0" ino=101804691 scontext=system_u:system_r:httpd_t:s0 tcontext=unconfined_u:object_
r:user_home_t:s0 tclass=file permissive=0
```

restore the default SELinux permissions to the new file in a folder with existing SELinux policies applied.

```bash
restorecon -v rhel-8.5-x86_64-boot.iso
```

You could update the SELinux policy to allow files from your home directory to be exposed by the web server.

```bash
sudo ausearch -c nginx --raw | audit2allow -M nginx-file
ls -al nginx-file-*
cat nginx-file.te
sudo semodule -X 300 -i nginx-file.pp
```

