# Course: Network and System Administration
## Bonus lab: Installing Rocky Linux 8

>__Note:__ This lab is not required to successfully complete the course.  This lab is available as it may help to improve or expand your Network and Linux experience and knowledge

>__Note:__ This lab should be completed on your desktop or laptop using your Hypervisor (VMWare player, Fusion or UTM).

> __Objective:__ The objective here it to create a secondary local linux install.  This process is very similar to the lab1 when we installed Red Hat Linux 8.

# How to install RockyLinux

[Rocky linux](https://rockylinux.org/about) is one of number of open source RHEL based linux distributions.  Rocky Linux is built using the Red Hat source code which is release to the public for use on the licensing agreement.  Other similar Linux distributions include Alma Linux and Navy linux.

It is assumed that you already have a local hypervisor installed such as VMWare Player, VMWare Fusion, UTM or Hyper-V.  Using any of these tools you will start your install of RockyLinux just like you have done for RHEL8.

## Download the RockLinux ISO

You can get any version of the RockyLinux that you want to try but this guide provides tips on version 8.

[RockyLinux Downloads]( https://rockylinux.org/download)

once you have the ISO file downloaded start your local hypervisor (VMWare, UTM, Hyper-V) create a new virtual machine and when it asks for your ISO file click browse and navigate to your RockyLinux downloaded ISO file.

For memory, CPU and disk size you can copy the settings we used for the Development servers.  1 Gb Ram, 1 CPU, and 16 Gb of hard disk.

## Starting the Install

Once you start your new install the first screen you should see is the bios based install menu.  Here you want to move your mouse into the console click to allow your session to focus on the emulator.  At this point you should be able to use the arrow keys on your keyboard to move up and select **Install Rocky Linux 8** and then press ENTER to continue the installation process.

[rocky linux install bios](images/rl-8-bios.png)

Next I expect that you will see the Language menu displayed in a GUI screen.  It is very likely that your mouse will move very slowly and the response will feel delayed.  But if English and English (United States) are good options for you move the mouse down to **Continue**



You are going to run through the same options as we have seen with the RHEL 8 install.  Here we are going to move through a number of steps.  They are numbered on the screenshot and you should find notes as you navigate down the install guide that correlate to each numbered step.

![Install Summary](images/rl-8-install-summary.png)

You will select Network and Host Name first.  Here we expect to see the option to enable the network interface here. If you are not able to click the slider in the top right to turn the interface on and off there is an issue.

1. We also want to add a hostname here such at itfdn-rl8-dev02.localdomain.  Click **Done** when complete to return to install summary menu.

![network](images/rl-8-install-network.png)

2. Next we are going to define the Installation Destination.  On this screen we are just going to accept the defaults or the automatic configuration. Simply click Done on this menu to return to the Installation summary menu.

![Install destination](images/rl-8-install-install-destination.png)

3. Now we are going to complete the software selection.  As we have done before we are going to start with a Minimal install with Headless Management enabled.  Once selected click Done to return to the Installation summary menu.

![Software selection](images/rl-8-install-software.png)

4. Now we can set the Time and Date.  Here you can click on the map close to your location or search through the menu for a City near you in the same timzone.  In my example I have selected Los Angeles but I live in Seattle but both are in the same timezone.  Click Done when complete to return to the Installation Summary.

![timezone](images/rl-8-install-timezone.png)

5. Now we want to set the root password.  As this server is installed on your local laptop and is not connected to the internet the password does not need to be complex.  Be sure you don't forget your root password as this is your backup account if you have issues with your own account.  If you select a simple password like password or root to match the username the system will make you press Done twice to ensure you know the password it weak.

![root setup](images/rl-8-install-root.png)

6. Finally setup your own account. Be sure to check the box to grant yourself Administrator rights.  It is common to keep usernames lowercase.  Set and do not forget your password.  When done click Done to return to the Installation summary menu to Begin the installation.

![set up your account](images/rl-8-install-user.png)

7. The final step is to start the Installation.  Did you notice that we never had to enter a Red Hat username and password?  We never had to register this system like we did with our RHEL 8 based systems. This is because the install files and packages are all available on the internet for free.

![begin install](images/rl-8-install-begin.png)

Once the install completes you will need to click Reboot System**.  If you are using UTM you will also have to clear the CD/DVD setting in your UTM console.  If you get an error that is it locked you may have to shut the server down, clear the CD/DVD menu and then restart your server.

![reboot](images/rl-8-install-reboot.png)

The expected bios menu after a successful install should look similar to the following screenshot.  The start up process has a default delay of a few seconds and then it will continue to start up.

![successful boot](images/rl-8-install-normal-boot.png)

Once your system reboots you should be able to login with the console and find your IP address using `ip address show` just like we have done with our RHEL based servers.

>**Note:** For UTM users you will have to clear out the boot.iso file before the system reboots or the install process will start over again.

![stop install cycle](images/rl-8-install-utm-clear-cd.png)

If you see this error, click OK and try again.  If the system restarts and returns to the initial bios boot menu seen at the begininng then you can stop the server, clear the CD/DVD menu and then restart the server.

![utm error](images/rl-8-install-utm-error.png)

