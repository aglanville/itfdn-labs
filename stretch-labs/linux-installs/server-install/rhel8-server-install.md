# RHEL 8 servers install

These notes are specific to bare metal installs of RHEL.

Partitioning based on 32G disk

Create partitions for the following directories.

/boot 1Gb
/ 10Gb
/home 5Gb 
swap 16Gb
/var 10Gb

What partitions to create and how large?
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/system_design_guide/partitioning-reference_system-design-guide

How much swap?
https://access.redhat.com/solutions/15244 