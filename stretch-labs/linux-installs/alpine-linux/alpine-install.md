# How to install Alpine Linux

Alpine Linux is not from the same Linux family as Red Hat Linux.  This installation, the configuration and the management of this Linux Operating System will be a little different for those that have focused on RHEL based systems.

[Alpine Linux](https://www.alpinelinux.org/about/) stives to be as small meaning the disk size, cpu and memory requirements are much less than required with RHEL based systems. Alpine Linux also strives to be very resouce efficient, so even with this small footprint Alpine Linux can achieve many of your goals.  Alpine linux is often used as the base for Linux Containers.

Always good to have the documentation available, you can find the Alpine user Handbook here: [Alpine Docs](https://docs.alpinelinux.org/user-handbook/0.1a/index.html)

## Objective

This guide will try to help you install Alpine Linux on your local hypervisor. The first step is to download the CDRom image or the ISO file.

### VMWare Fusion

### Mac M1/M2 using UTM

Download the standard aarch64 ISO file.

When configuring the system type select aarch64 from the drop down menu.

keyboard == us
disk install == sys