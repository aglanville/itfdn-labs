# Installing Kubernetes on RockyLinux8 with the Containerd runtime for the LEAD node and Flannel

The goal of this lab is to walk you through the setup of a Kubernetes cluster using RockyLinux 8 servers.  Each Linux server was created with a 16 Gb disk, 2 vCPUs, and 4 Gb of RAM.

The following steps should be completed on the lead node or nodes in your cluster.

```bash
sudo dnf -y update
sudo dnf install -y iproute-tc
```

1. Disable swap

```bash
free -h
lsblk
sudo swapoff -a
free -h
lsblk
sudo sed "-i.orig" '/swap/d' /etc/fstab
cat /etc/fstab.orig | grep swap
cat /etc/fstab | grep swap
```

2. Disable SELinux

```bash
getenforce
sudo setenforce 0
sudo sed -i.orig 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
getenforce
```

3. Update firewall rules on the lead nodes

```bash
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --reload
```

4. Install a container runtime environment

We are going to install containerd a project started by docker.

```bash
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
cat /etc/modules-load.d/containerd.conf
EOF
cat /etc/modules-load.d/containerd.conf
```

4.1 Load overlay and br_netfilter modules

Now that we have our cri-o configuration in place we can load the overlay and br_netfilter modules using modprobe.  Modprobe is a linux utility that can intelligently add or remove a module from the Linux kernel. Like many Linux tools when everything goes well modprobe does not return anything to the user.  We are running modprobe with the verbose option just in case something is not right. Running the modprobe br_netfilter will result in the /proc/sys/net/bridge/bridge-nf-call-iptables file being generated.

>**Note**: When does the bridge-nf-call-iptables file show up?

```bash
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
sudo modprobe overlay -v
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
sudo modprobe br_netfilter -v
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
```

4.2 Update kernel parameters

Here are going to create a configuration file for cri-o to update the kernel parameters using [systcl](https://linux.die.net/man/8/sysctl) these parameters will persist reboots.  This settings allow for ip forwarding and bridge functions and allow for packets traversing the bridge to be sent to iptables for processing.

>Note:  `net.ipv6.conf.all.disable_ipv6 = 1` will disable the IPv6 on your host.  To test is out use `sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1`.

```bash
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv6.conf.all.disable_ipv6 = 1
EOF
cat /etc/sysctl.d/99-kubernetes-cri.conf
```

Once these configuration file is in place I like to reboot just to confirm that everything is working as expected upto this point.  We can also confirm the settings using sysctl.

```bash
 sudo sysctl --system
 sudo reboot
 sudo sysctl --system
 ```

4.3 Install Containerd runtime

```bash
sudo dnf -y update
sudo dnf install -y yum-utils
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf -y install containerd.io
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml
```

Now we want to enable SystemdCgroup in the config.toml file.

```bash
cat /etc/containerd/config.toml | grep SystemdCgroup 
sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
cat /etc/containerd/config.toml | grep SystemdCgroup 
```

Now enable and start the containerd process.

```bash
sudo systemctl enable containerd --now
sudo systemctl status containerd
```

Next we are going to create the kubernetes repo configuration file.

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
cat /etc/yum.repos.d/kubernetes.repo
```

check for available versions

```bash
yum -y list --showduplicates kubeadm --disableexcludes=kubernetes
```

Install kubeadm, kubelet and kubectl

```bash
export KVERSION=1.24.0-0
sudo yum install -y kubeadm-$KVERSION --disableexcludes=kubernetes
sudo yum install -y kubelet-$KVERSION --disableexcludes=kubernetes
sudo yum install -y kubectl-$KVERSION --disableexcludes=kubernetes
sudo systemctl enable kubelet --now
```

Install wget and grab config for Flannel.  The pod network needs to match the values used later in the ClusterConfiguration file.  Let alone they will use default values but we are changing them to avoid IP conflicts and confirm we know how to make this update.

```bash
sudo dnf install wget -y
# wget https://raw.githubusercontent.com/flannel-io/flannel/v0.20.2/Documentation/kube-flannel.yml
wget https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml
sed -i.orig 's/10\.244/172\.21/' kube-flannel.yml
```

Next generate the default ClusterConfiguration file

```bash
kubeadm config print init-defaults | tee ClusterConfiguration.yaml
```

Now make some updates to that configuration file.

1. update the advertise Address to reflect the desired public IP address

```bash
sed -i 's/  advertiseAddress: 1.2.3.4/  advertiseAddress: 140.142.159.200/' ClusterConfiguration.yaml
```

2. Update the criSocket to point to the containerd.sock file

```bash
sed -i 's/  criSocket: \/var\/run\/dockershim\.sock/  criSocket: \/run\/containerd\/containerd\.sock/' ClusterConfiguration.yaml
```

3. Add the Systemd cgroupDriver directive to the config file.

```bash
cat <<EOF | cat >> ClusterConfiguration.yaml
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF
```

4. Update the name value, replace node with the actual host FQDN value.

```bash
sed -i "s/	name: node/	name: ${HOSTNAME}/" ClusterConfiguration.yaml
```

Next initialize the kube cluster and define the pod network.  This network is defined in the calico.yaml file, make sure these vales match.

5. Add pod network cidr

Add the line podSubnet to the ClusterConfiguration file.  The pod network defined here should match the values set in the calico.yaml file.

```bash
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "172.21.0.0/16" # --pod-network-cidr
```

Now initialize your Kubernetes cluster.

```bash
sudo kubeadm init --config=ClusterConfiguration.yaml 
```

Now setup your admin functions on the lead node

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Enable kubectl auto-completion

```bash
sudo dnf -y install bash-completion
echo 'source <(kubectl completion bash)' >>~/.bashrc
cat ~/.bashrc
```

Now we hope to create our Flannel Pod Network.

```bash
kubectl create -f kube-flannel.yml
```

Check your nodes and pods, keep in mind it will take a couple minutes for everything to sort out.

```bash
kubectl get pods -A
kubectl get nodes
kubectl get ns -A
```

## How to reset a build

delete the pod network and then reset that kubernetes environment.

```bash
kubectl delete -f kube-flannel.yml
sudo kubeadm reset
rm ~/.kube/config
```













