# Installing Kubernetes on RockyLinux8 with the Containerd runtime for a WORKER node

The goal of this lab is to walk you through the setup of a Kubernetes cluster using RockyLinux 8 servers.  Each Linux server was created with a 16 Gb disk, 2 vCPUs, and 4 Gb of RAM. 

The Kubernetes version in this lab is set to **1.24.0-0**.

The following steps should be completed on all linux hosts. Before you start make sure the system is up to date

```bash
sudo dnf -y update
sudo dnf install -y iproute-tc
```

1. Disable swap

```bash
free -h
lsblk
sudo swapoff -a
free -h
lsblk
sudo sed "-i.orig" '/swap/d' /etc/fstab
cat /etc/fstab.orig | grep swap
cat /etc/fstab | grep swap
```

2. Disable SELinux

```bash
getenforce
sudo setenforce 0
sudo sed -i.orig 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
getenforce
```

3. Update the firewall rules on the worker nodes

```bash
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=30000-32767/tcp                                                 
sudo firewall-cmd --reload
```

4. Install a container runtime environment

We are going to install containerd a project started by docker.

```bash
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
cat /etc/modules-load.d/containerd.conf
EOF
cat /etc/modules-load.d/containerd.conf
```

4.1 Load overlay and br_netfilter modules

Now that we have our cri-o configuration in place we can load the overlay and br_netfilter modules using modprobe.  Modprobe is a linux utility that can intelligently add or remove a module from the Linux kernel. Like many Linux tools when everything goes well modprobe does not return anything to the user.  We are running modprobe with the verbose option just in case something is not right. Running the modprobe br_netfilter will result in the /proc/sys/net/bridge/bridge-nf-call-iptables file being generated.

>**Note**: When does the bridge-nf-call-iptables file show up?

```bash
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
sudo modprobe overlay -v
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
sudo modprobe br_netfilter -v
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
```

4.2 Update kernel parameters

Here are going to create a configuration file for cri-o to update the kernel parameters using [systcl](https://linux.die.net/man/8/sysctl) these parameters will persist reboots.  This settings allow for ip forwarding and bridge functions and allow for packets traversing the bridge to be sent to iptables for processing.

```bash
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
cat /etc/sysctl.d/99-kubernetes-cri.conf
```

Once these configuration file is in place I like to reboot just to confirm that everything is working as expected upto this point.  We can also confirm the settings using sysctl.

```bash
 sudo sysctl --system
 sudo reboot
 sudo sysctl --system
 ```

4.3 Install Containerd runtime

```bash
sudo dnf -y update
sudo yum install -y yum-utils
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
sudo yum -y install containerd.io
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml
```

Now we want to enable SystemdCgroup in the config.toml file.

```bash
cat /etc/containerd/config.toml | grep SystemdCgroup 
sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
cat /etc/containerd/config.toml | grep SystemdCgroup 
```

Now enable and start the containerd process.

```bash
sudo systemctl enable containerd --now
sudo systemctl status containerd
```

Next we are going to create the kubernetes repo configuration file.

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
cat /etc/yum.repos.d/kubernetes.repo
```

check for available versions

```bash
yum -y list --showduplicates kubeadm --disableexcludes=kubernetes
```

Install kubeadm, kubelet and kubectl

```bash
export KVERSION=1.24.0-0
sudo yum install -y kubeadm-$KVERSION --disableexcludes=kubernetes
sudo yum install -y kubelet-$KVERSION --disableexcludes=kubernetes
sudo yum install -y kubectl-$KVERSION --disableexcludes=kubernetes
sudo systemctl enable kubelet --now
```

Now add the node to the cluster using the join command provided by the lead node.

```bash
kubeadm join 140.142.159.200:6443 --token abcdef.0123456789abcdef --discovery-token-ca-cert-hash sha256:2117e1f43adf3e1b9edafeb3a144d8ea4e80973a42bf4369d53276eb7e2c5692
```

If you do not have the join command and token then we can generate a new one now.  These commands are run on the master node to generate a new token.  After this we will need to get the hash.

```shell
kubeadm token list
```

if it has been longer than about 23hrs then your token will have expired and you will need to generate a new token,

```shell
kubeadm token create
```

Next we will also need the get the hash for our join command.

```shell
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'
```

```shell
sudo kubeadm join {kube master node ip}:6443 \
    --token ${kubeadm token} \
    --discovery-token-ca-cert-hash ${kubeadm public key hash}
```
