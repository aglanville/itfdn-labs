# Installing Kubernetes on RockyLinux8 with the CRI-O runtime

The goal of this lab is to walk you through the setup of a Kubernetes cluster using RockyLinux 8 servers.  Each Linux server was created with a 16 Gb disk, 2 vCPUs, and 4 Gb of RAM.

The following steps should be completed on all linux hosts. Before you start make sure the system is up to date

```bash
sudo dnf -y update
```

1. Disable swap

```bash
free -h
lsblk
sudo swapoff -a
free -h
lsblk
sudo sed "-i.orig" '/swap/d' /etc/fstab
cat /etc/fstab.orig | grep swap
cat /etc/fstab | grep swap
```

2. Disable SELinux

```bash
getenforce
sudo setenforce 0
sudo sed -i.orig 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
getenforce
```

3. Update firewall rules on the lead nodes

```bash
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --reload
```

3.1 Update the firewall rules on the worker nodes

```bash
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=30000-32767/tcp                                                 
sudo firewall-cmd --reload
```

4. Install a container runtime environment

We are going to install [CRI-O](https://cri-o.io/) as our runtime environment.  We going to create a conf file under /etc/modules-load.d called k8s.conf.  This file will help to enable a bridge net filter.  This feature allows for transparent masquerading and to facilitate Virtual Extensible LAN (VxLAN) traffic for communication between Kubernetes pods across the cluster.

```bash
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
cat /etc/modules-load.d/k8s.conf
```

4.1 Load overlay and br_netfilter modules

Now that we have our cri-o configuration in place we can load the overlay and br_netfilter modules using modprobe.  Modprobe is a linux utility that can intelligently add or remove a module from the Linux kernel. Like many Linux tools when everything goes well modprobe does not return anything to the user.  We are running modprobe with the verbose option just in case something is not right. Running the modprobe br_netfilter will result in the /proc/sys/net/bridge/bridge-nf-call-iptables file being generated.

>**Note**: When does the bridge-nf-call-iptables file show up?

```bash
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
sudo modprobe overlay -v
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
sudo modprobe br_netfilter -v
ls -la /proc/sys/net/bridge/bridge-nf-call-iptables
```

4.2 Update kernel parameters

Here are going to create a configuration file for cri-o to update the kernel parameters using [systcl](https://linux.die.net/man/8/sysctl) these parameters will persist reboots.  This settings allow for ip forwarding and bridge functions and allow for packets traversing the bridge to be sent to iptables for processing.

```bash
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
cat /etc/sysctl.d/k8s.conf
```

Once these configuration file is in place I like to reboot just to confirm that everything is working as expected upto this point.  We can also confirm the settings using sysctl.

```bash
 sudo sysctl --system
 sudo reboot
 sudo sysctl --system
 ```

4.3 Install CRI-O

Set a couple environment variables for CentOS 8 and the version of CRIO and Kubernetes you want to install

```bash
export OS=CentOS_8
export VERSION=1.22
sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/devel:kubic:libcontainers:stable.repo
sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/devel:kubic:libcontainers:stable:cri-o:$VERSION.repo
sudo dnf install cri-o
```

Now enable and start the CRI-O runtime and check for the existence of the cri-o sock file.

```bash
sudo systemctl enable crio --now
ls /var/run/crio/crio.sock
`

Next we are going to create the kubernetes repo configuration file.

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
cat /etc/yum.repos.d/kubernetes.repo
```

check for available versions

```bash
yum list --showduplicates kubeadm --disableexcludes=kubernetes
```

Next we are going to install a particular version of Kubernetes.  Create a new environment variable for kubernetes such as 1.22.0-0.  And start the kubelet

```bash
export KVERSION=1.22.0-0
sudo yum install -y kubeadm-$KVERSION --disableexcludes=kubernetes
sudo yum install -y kubelet-$KVERSION --disableexcludes=kubernetes
sudo yum install -y kubectl-$KVERSION --disableexcludes=kubernetes
sudo systemctl enable kubelet --now
```

Check the status of kubelet and containerd, we expect that kubelet will be crashing until we have networking configured.

```bash
sudo systemctl status kubelet 
sudo systemctl status containerd
```

Enable the services to auto start

```bash
sudo systemctl enable kubelet
sudo systemctl enable containerd
```

install wget and grab config for calico

```bash
sudo dnf install wget -y
wget https://docs.projectcalico.org/manifests/calico.yaml
```

Uncomment and update the CIDR range defined in the calico yaml configuration file under **CALICO_IPV4POOL_CIDR**.

```bash
- name: CALICO_IPV4POOL_CIDR
  value: "192.168.100.0/16"
```


Next generate the default ClusterConfiguration file

```bash
kubeadm config print init-defaults | tee ClusterConfiguration.yaml
```

Now make some updates to that configuration file.

1. update the advertise Address to reflect the desired public IP address

```bash
sed -i 's/  advertiseAddress: 1.2.3.4/  advertiseAddress: 140.142.159.200/' ClusterConfiguration.yaml
```

2. Update the criSocket to point to the containerd.sock file

```bash
sed -i 's/  criSocket: \/var\/run\/dockershim\.sock/  criSocket: \/run\/containerd\/containerd\.sock/' ClusterConfiguration.yaml
```

3. Add the Systemd cgroupDriver directive to the config file.

```bash
cat <<EOF | cat >> ClusterConfiguration.yaml
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF
```

4. Update the name value, replace node with the actual host FQDN value.

Now start the init process.

```bash
sudo kubeadm init --config=ClusterConfiguration.yaml
```

Now setup your admin functions on the lead node

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Check your nodes and pods, keep in mind it will take a couple minutes for everything to sort out.

```bash
kubectl get pods -A
kubectl get nodes
kubectl get ns -A
```



FileExisting-tc]: tc not found in system path


