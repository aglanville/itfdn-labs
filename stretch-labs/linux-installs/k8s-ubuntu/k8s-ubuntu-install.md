# How to install K8S on Ubuntu

## prerequisites

Make sure vi is the default editor

`sudo update-alternatives --config editor`

## install prep

Firewall rules

```bash
sudo ufw allow 6443/tcp
sudo ufw allow 2379:2380/tcp
sudo ufw allow 10250/tcp
sudo ufw allow 10259/tcp
sudo ufw allow 10257/tcp
```

Enable overlay and bridging for k8s

```bash
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
sudo modprobe br_netfilter
sudo modprobe overlay
```

enable networking for k8s

```bash
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
```

apply those settings to the current session

```bash
sudo sysctl --system
```

disable swap

```bash
free -h | grep -i swap
sudo sed "-i.orig" '/swap/d' /etc/fstab
sudo swapoff -a
free -h | grep -i swap
```

Install containerd

```bash
sudo apt update -y
sudo mkdir -p /etc/apt/keyrings
sudo chmod 755 /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install containerd.io
```

Update containerd config

```bash
sudo mv /etc/containerd/config.toml /etc/containerd/config.toml.orig
containerd config default | sudo tee /etc/containerd/config.toml >/dev/null
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
```

Start containerd

```bash
sudo systemctl enable containerd --now
```

Install required packages

```bash
sudo apt install apt-transport-https ca-certificates curl -y
```

Install the K8S certs and repo

Note: In releases older than Debian 12 and Ubuntu 22.04, /etc/apt/keyrings does not exist by default. You can create this directory if you need to, making it world-readable but writeable only by admins.

```bash
echo "deb [signed-by=/etc/apt/keyrings/kubernetes.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes.gpg
```

Install kubelet, kubeadm and kubectl

syntax for specific versions: `sudo apt-get install -y kubelet=1.26.5-00 kubeadm=1.26.5-00 kubectl=1.26.5-00`

```bash
sudo apt update
sudo apt install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
sudo systemctl enable kubelet --now
```

Install wget and grab the config for Flannel.  The pod network needs to match the values used later in the ClusterConfiguration file.  Let alone they will use default values but we are changing them to avoid IP conflicts and confirm we know how to make this update.

```bash
sudo dnf install wget -y
# wget https://raw.githubusercontent.com/flannel-io/flannel/v0.20.2/Documentation/kube-flannel.yml
wget https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml
sed -i.orig 's/10\.244/172\.21/' kube-flannel.yml
```

Next generate the default ClusterConfiguration file.
```bash
kubeadm config print init-defaults | tee ClusterConfiguration.yaml
```

Now make some updates to that configuration file.

1. update the advertise Address to reflect the desired public IP address (the FQDN value is not valid here).

>__Note:__ You can do this using kubeadm with the option --control-plane-endpoint=\<IP ADDRESS>


```bash
sed -i 's/  advertiseAddress: 1.2.3.4/  advertiseAddress: 140.142.159.200/' ClusterConfiguration.yaml
```

2. Update the criSocket to point to the containerd.sock file

```bash
sed -i 's/  criSocket: \/var\/run\/dockershim\.sock/  criSocket: \/run\/containerd\/containerd\.sock/' ClusterConfiguration.yaml
```

3. Add the Systemd cgroupDriver directive to the config file.

```bash
cat <<EOF | cat >> ClusterConfiguration.yaml
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF
```

4. Update the name value, replace node with the actual host FQDN value.

```bash
sed -i "s/	name: node/	name: ${HOSTNAME}/" ClusterConfiguration.yaml
```

Next initialize the kube cluster and define the pod network.  This network is defined in the calico.yaml file, make sure these vales match.

5. Add pod network cidr

Add the line podSubnet to the ClusterConfiguration file.  The pod network defined here should match the values set in the calico.yaml file.

```bash
echo 'apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "172.21.0.0/16" # --pod-network-cidr' >> ClusterConfiguration.yaml
```

Now initialize your Kubernetes cluster.

```bash
sudo kubeadm init --config=ClusterConfiguration.yaml
```







```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Now we hope to create our Flannel Pod Network.  This is only required on the first nodes.

```bash
kubectl create -f kube-flannel.yml
```

____
____

### issues with Calico but this should work...

Install calico network on the lead node, not required once one node is up and running.

```bash
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml
```

___
___

Add nodes to your cluster, using the join command and token provided when the in

```
sudo kubeadm join ulc-8.ulcert.uw.edu:6443 --token 9w9puh.evlnohknrc5478z9 \
	--discovery-token-ca-cert-hash sha256:837fac409e38f69acc75f246e2c94c0b50545830f6211433203f08f8921d3bfb
```

## Troubleshooting tips

If you want to reset the lead node you will have to delete the CNI plugin which in this case is Calico.  We will also have to remove the CNI config cruft and the .kube configs.

```bash
kubectl delete -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml
sudo kubeadm reset
sudo rm -rf /etc/cni/net.d/
rm -rf $HOME/.kube
```

If you have issues want want to reset a node just run the kubeadm reset.

```bash
sudo kubeadm reset
```

If the kubeadm join fails with a message like

```text
sudo kubeadm join 140.142.159.9:6443 --token abcdef.0123456789abcdef \
        --discovery-token-ca-cert-hash sha256:773ef85a0a670b9db43259870a11e2855b0289f407a92d746b0dba1d57b6478b
[preflight] Running pre-flight checks
error execution phase preflight: [preflight] Some fatal errors occurred:
	[ERROR CRI]: container runtime is not running: output: time="2023-06-18T08:53:36Z" level=fatal msg="validate service connection: CRI v1 runtime API is not implemented for endpoint \"unix:///var/run/containerd/containerd.sock\": rpc error: code = Unimplemented desc = unknown service runtime.v1.RuntimeService"
, error: exit status 1
[preflight] If you know what you are doing, you can make a check non-fatal with `--ignore-preflight-errors=...`
To see the stack trace of this error execute with --v=5 or higher
```

[kubeadm error](images/kubeadm-join-failure.png)

Re-install containerd

1. Set up the Docker repository as described in https://docs.docker.com/engine/install/ubuntu/#set-up-the-repository
2. Remove the old containerd:apt remove containerd.io
3. Update repository data and install the new containerd: apt update, apt install containerd.io
4. Remove the installed default config file: rm /etc/containerd/config.toml
5. Restart containerd: systemctl restart containerd


### Remove node from cluster

First cordon the node to avoid any new pods from being scheduled there.  Next drain the node of an existing pods and then finally delete the node.

>__Note:__ we use ignore-daemonsets as otherwise the drain process can hang waiting for daemonsets pods to migrate.

```bash
kubectl get nodes
kubectl cordon node
kubectl drain <node name> --ignore-daemonsets
kubectl delete node <node name>
```

___

End of Lab

___