# How to install CentOS 7 on Virtual Box

The preferred desktop virtualization solution for our course is either VMWare player or Fusion.  However Virtualbox can be used as a substitute or simply for the experience of working with another solution.

CentOS prior to version 8.5 was a version based open-source operating system that based on the same code base as Red Hat Enterprise Linux (RHEL).  In this lab we are deploying CentOS 7.x.  The End of Life [(EoL)](https://wiki.centos.org/About/Product) date for CentOS 7 is June 30th, 2024 so while it still has a large deployment many organizations are migrating to RHEL 8 or 9 at this time.

You can of course install the latest CentOS release following this guide and will likely only find minor changes during the install process.

## VirtualBox

Virtualbox is another desktop virtualization solution, comparable to the VMWare products. It is owned by Oracle who picked it up as part of the acquisition of [Sun Systems](https://www.oracle.com/corporate/pressrelease/oracle-buys-sun-042009.html).

* First we need to install Virtualbox, find the install notes for your platform, Windows or Mac here: [Virtualbox](https://www.virtualbox.org/wiki/Downloads).  There is a preview version that works with the M1/M2 Mac laptops.

## Setup virtual server

We need to setup a new virtual machine for our Linux server.  Once VirtualBox is installed open the interface and click new on the top navigation bar.  In the next window provide a name for your virtual machine.  Here I have used itfdn140dev.  Change the Type to "Linux" and set Version to "Red Hat (64-bit).

![new machine](virtualbox-new-machine.png)

Next take the default Memory size (ram allocation) of 1024mb (1Gb) and click "Continue".

![Memory size](virtualbox-memsize.png)

Next we are going to create the hard disk, here select "VMDK" and click "Continue". 

![create disk](virtualbox-disk.png)

>Note: VDI, VMDK and VHD all support dynamic growth.  VDI is the native VirtualBox standard.  VMDK was developed by VMWare. VMDK drives can be broken into 2Gb files as you may have noticed during your VMWare installs. VHD is the native Microsoft disk format used by Virtual PC.

For storage options select Dynamically allocated. This means that even though we allocate 20Gb of disk the virtual machine will not use that disk until required.  It is very likely that your disk will never pass 10G in actual disk usage.

![disk storage](virtualbox-disk-storage.png)

Finally for storage size I suggest you set the disk size to 12Gb.  You can always add another disk if you want later.

Set the disk size you want and click "Create".

![disk size](virtualbox-disk-size.png)

## Install Linux

It is assumed that you have already downloaded the Linux ISO file you want to use. If that is not the case download the latest CentOS7 minimal disk image [HERE](http://isoredirect.centos.org/centos/7/isos/x86_64/).  

>Note: [ISO is disk image file](https://en.wikipedia.org/wiki/ISO_image), these images in the past were often copied to and from CD or DVD disks.  These disks were inserted into the CDRom or optical drive of a given computer.

Here is a screenshot of what you are looking for:

![CentOS7 download](centos7-minimal-download.png)

Once you have the image downloaded next you need to mount this ISO file to your your new virtual machine.

1. Select your virtual machine name on the left . 
2. Click "Settings" on the top navigation menu.
3. Select your "Empty" Controller: IDE
4. Right click on the small circle to the right of IDE Secondary Master
5. From the pop out menu click "Choose/Create a Virtual Optical Disk..."

![VirtualBox settings](virtualbox-add-iso.png)

In the next storage menu click add and navigate to your ISO image. You may have multiple images if you just downloaded a new version.  Select the one you want to use and click "Choose".  On the next screen click "OK" to complete the process.

![Virtual box add ISO](virtualbox-attach-iso.png)

Now start your new linux server and walk through the install steps.  You will see a pop up asking you to select which disk to boot from this is just because this is a new install. Confirm that your ISO file is selected and click "Start".

![virtualbox start](virtualbox-start-vm.png)

Next when you click into onto your new virtual machine another pop-up will show informing you that your mouse will be captive to the virtual machine util you press the release key.  Click Ok your mouse should be captive in the screen.

![virtualbox mouse warning](virtualbox-capture.png)

The system will auto boot in about 60 seconds and check your ISO image.  This step is not required and can be skipped if you use your arrow keys to move up to the "Install CentOS 7" line and then press enter.

>Note:You can click the X on any of the informational messages that show up over your virtualbox console screen.

![virtualbox select install](virtualbox-centos-select.png)

Now follow the guide from lab1 if you need additional tips on the install process.

## Configure networking

Virtualbox does not create the same local network as we have seen with VMWare so any connections we want to make to our new VirtualBox server have to be port forwarded by our localhost to the required port.  To do this we will tell Virtualbox that connection on our localhost IP address (127.0.0.1) on a given port should be forwarded to the new server IP on port 22 for SSH or port 80 for HTTP for example.

To configure this open open the Virtualbox console, select your virtual machine, click settings, network, ensure you are attached to "NAT", Cable connected is checked and click Port Forwarding.

![Virtualbox NAT](virtualbox-nat.png)

Now we need to define the port forwarding addresses.  You will need one rule for each service you want to expose.  In this case we are adding SSH but you may also want to add HTTP and HTTPS among others.

In this example we are configuring port 2222 on the host system (Your MAC or Windows host) on port 127.0.0.1 (the local loopback address) to port forward to the IP address from the CentOS host on port 22.

![Virtual box port forward](virtualbox-portforward.png)


>Note: Ports from 0-1024 are considered well known ports and often restricted.  It is better to select a port outside of that range for your local listener.

## How to connect to your new CentOS7 guest

You will need to connect to your linux host using the loopback and the non standard port you defined.  In my example above I used port 2222.  If you are using a mac then your connection will look something like this.

```
ssh -p 2222 <USER>@localhost
```

![ssh to centos guest](virtualbox-ssh-localhost.png)

If you are using Windows and putty your host or IP address will be 127.0.0.1 and the port in this example would be **2222**

## Troubleshooting

**Q.** I am unable to connect to my linux server, I get a message that the connection was refused.  
**A.** This could be caused by a few issues.  First make sure that your local listener is above port 1024, try using 1025 for example.  Confirm that you have the right IP address from your linux guest `ip addr show`.  Make sure the SSH service is running `sudo systemctl start sshd`.

**Q.** I am not able to open the console for my virtual machine.

A. If you are using a mac make sure you have granted Virtual Box remote control access in the **Security and Privacy** menu under **Settings**.

![security and privacy](images/mac-settings-security.png)

___

## End of Lab

___