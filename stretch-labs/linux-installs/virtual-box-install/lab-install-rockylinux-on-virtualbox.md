# How to install RockyLinux 8 on Virtual Box

The preferred desktop virtualization solution for our course is either VMWare player or Fusion.  However Virtualbox can be used as a substitute or simply for the experience of working with another solution.

[RockyLinux](https://rockylinux.org/) is an open-source operating system that is based on the same code base as Red Hat Enterprise Linux (RHEL).  RockyLinux is a project that was started by Gregory Kurtzer an original founder in the CentOS project after the announcement was made to switch to a stream deployment for CentOS.

## VirtualBox

Virtualbox is another desktop virtualization solution, comparable to the VMWare products. It is owned by Oracle who picked it up as part of the acquisition of [Sun Systems](https://www.oracle.com/corporate/pressrelease/oracle-buys-sun-042009.html).

* First we need to install Virtualbox, find the install notes for your platform, Windows or Mac here: [Virtualbox](https://www.virtualbox.org/wiki/Downloads).  There is a preview version that works with the M1/M2 Mac laptops.

## Setup virtual server

We need to setup a new virtual machine for our Linux server.  Once VirtualBox is installed open the interface and click new on the top navigation bar.  In the next window provide a name for your virtual machine.  Here I have used itfdn140dev.  Change the Type to "Linux" and set Version to "Red Hat (64-bit).

![new machine](virtualbox-new-machine.png)

Next take the default Memory size (ram allocation) of 1024mb (1Gb) and click "Continue".

![Memory size](virtualbox-memsize.png)

Next we are going to create the hard disk, here select "VMDK" and click "Continue". 

![create disk](virtualbox-disk.png)

>Note: VDI, VMDK and VHD all support dynamic growth.  VDI is the native VirtualBox standard.  VMDK was developed by VMWare. VMDK drives can be broken into 2Gb files as you may have noticed during your VMWare installs. VHD is the native Microsoft disk format used by Virtual PC.

For storage options select Dynamically allocated. This means that even though we allocate 20Gb of disk the virtual machine will not use that disk until required.  It is very likely that your disk will never pass 10G in actual disk usage.

![disk storage](virtualbox-disk-storage.png)

Finally for storage size I suggest you set the disk size to 12Gb.  You can always add another disk if you want later.

Set the disk size you want and click "Create".

![disk size](virtualbox-disk-size.png)

## Install Linux

Now we need to downloaded the Linux ISO file you want to use. In this case we want to get the latest RockyLinux 8 Minimal ISO.  You should be able to find that [HERE](https://rockylinux.org/download).  

>Note: [ISO is disk image file](https://en.wikipedia.org/wiki/ISO_image), these images in the past were often copied to and from CD or DVD disks.  These disks were inserted into the CDRom or optical drive of a given computer.

Here is a screenshot of what you are looking for:

![CentOS7 download](images/rockylinux-minimal-download.png)

Once you have the image downloaded next you need to mount this ISO file to your your new virtual machine.

1. Select your virtual machine name on the left .
2. Click "Settings" on the top navigation menu.
3. Select "Storage" from the top navigation menu.
4. Select your "Empty" Controller: IDE.
5. Click on the small circle to the right of IDE Secondary Master
6. From the pop out menu click "Choose/Create a Virtual Optical Disk..."

![VirtualBox settings](images/virtual-box-add-rl8-iso.png)

In the next storage menu click the **Add** button in the to navigation menu and locate your ISO image. You may have multiple images if you just downloaded a new version.  Select the one you want to use and click "Choose".  On the next screen click "OK" to complete the process.

![Virtual box add ISO](images/virtual-box-attach-rl8-iso.png)

Now select your new Rocky Linux guest on the left and review the settings.  Make sure your  and click start on the top menu.your new linux server and walk through the install steps.  You will see a pop up asking you to select which disk to boot from this is just because this is a new install. Confirm that your ISO file is selected and click "Start".

![virtualbox start](images/virtualbox-start-rl8-vm.png)

Next when you click into onto your new virtual machine another pop-up may show informing you that your mouse will be captive to the virtual machine util you press the release key.  Click Ok your mouse should be captive in the screen.

![virtualbox mouse warning](virtualbox-capture.png)

>Note: If you are using a mac you may have to grant VirtualBox the ability to control your computer.  Check settings --> Security & Privacy and then enable VirtualBox as shown [HERE](images/mac-settings-security.png)

The system will auto boot in about 60 seconds and check your ISO image.  This step is not required and can be skipped if you use your arrow keys to move up to the "Install RockyLinux 8" line and then press enter.

>Note:You can click the X on any of the informational messages that show up over your virtualbox console screen.

![virtualbox select install](images/virtual-box-start-rl8-install.png)

Although we are installing Rocky Linux 8 this time you should find the menus to be very similar to what we saw in [**Lab 1**](https://canvas.uw.edu/) when installing RHEL8.  This is because this is the same code base, the difference is that the logos for RHEL have been replaced with Rocky Linux logos.  Review [**Lab 1**](https://canvas.uw.edu/) if you want help completing the install.

## Configure networking

Virtualbox does not create the same local network as we have seen with VMWare so any connections we want to make to our new VirtualBox server have to be port forwarded by our localhost to the required port.  To do this we will tell Virtualbox that connection on our localhost IP address (127.0.0.1) on a given port should be forwarded to the new server IP on port 22 for SSH or port 80 for HTTP for example.

To configure this open open the Virtualbox console, select your virtual machine, click settings, network, ensure you are attached to "NAT", Cable connected is checked and click **Port Forwarding**.

![Virtualbox NAT](images/virtual-box-nat.png)

Now we need to define the port forwarding addresses.  You will need one rule for each service you want to expose.  In this case we are adding SSH but you may also want to add HTTP and HTTPS among others.

In this example we are configuring port 2222 on the host system (Your MAC or Windows host) on port 127.0.0.1 (the local loopback address) to port forward to the IP address from the CentOS host on port 22.  The Guest IP address is something you will have to get from your own guest.  By default we use DHCP for our guest IP address but you know this can be changed to a static IP address.  See [Lab 9 - Configuring your local network interfaces](https://canvas.uw.edu) for notes on this process.

![Virtual box port forward](images/virtual-box-port-forward.png)

>**Note**: Ports from 0-1024 are considered well known ports and often restricted.  It is better to select a port outside of that range for your local listener.

## How to connect to your new Rocky Linux guest

You will need to connect to your linux host using the loopback and the non standard port you defined.  In my example above I used port 2222.  If you are using a mac then your connection will look something like this.

```bash
ssh -p 2222 <USER>@localhost
```

![ssh to centos guest](virtualbox-ssh-localhost.png)

If you are using Windows and putty your host or IP address will be 127.0.0.1 and the port in this example would be **2222**

## Troubleshooting

**Q.** I am unable to connect to my linux server, I get a message that the connection was refused.  
**A.** This could be caused by a few issues.  First make sure that your local listener is above port 1024, try using 1025 for example.  Confirm that you have the right IP address from your linux guest `ip addr show`.  Make sure the SSH service is running `sudo systemctl start sshd`.

**Q.** I am not able to open the console for my virtual machine.

A. If you are using a mac make sure you have granted Virtual Box remote control access in the **Security and Privacy** menu under **Settings**.

![security and privacy](images/mac-settings-security.png)

___

## End of Lab

___