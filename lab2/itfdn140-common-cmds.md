# Course: Network and System Administration
## In Class Lab: 2
## Name: System navigation and common commands

>**Objective**: Work with some of the common commands just discussed in the lecture.  Learn to use an SSH connection and avoid using the VMWare console to login to your development host.

>__Note:__ It is assumed that you are doing this lab on your development server and **NOT** your production server.

You must open your VMWare player or Fusion application and then start your Development server in order to work through this lab. The name of your virtual machine maybe be something like **itfdn-rhel8-dev01** or **itfdn-dev01**.

**Mac users**

![Fusion start guest](fusion-start-guest.png)

**Windows users**

![Player start guest](vmware-player-guest.png)

Once your **Development** server is running move on to the tasks below.

___

## Tasks

Login to your development RHEL 8 Linux server using your user account, not **root**.  

If you are using a **MAC** this means opening your terminal client.

![Mac Terminal](mac-term.png)

Once your mac terminal is open then try to connect using your `yourusername@dev_host_ip_address` Here is an example of what that might look like.

![Mac ssh connection](mac-term-conn.png)

If you are using a **Windows** host this means opening a Putty session.

![Putty session](putty-dev-connection.png)

Click here if you don't have the putty client installed yet. This will just download the Putty SSH client [Putty SSH Client](https://the.earth.li/~sgtatham/putty/latest/w32/putty.exe).  

> **Note**: Remember do not login as **root**.

If you see a warning message, take a minute to read it.  This is likely something you will only see the first time you connect if you accept the connection.

Here is what the warning might look like using Windows and Putty.

![Putty certificate warning](putty-cert-warning.png)

On a Mac the message is a little more subtle but it may look like this.

![Mac cert warning](mac-cert-warning.png)

## Command review

When running the following series of commands notice what directory you are in before and after using **pwd** and **cd**.

```shell
pwd
ls
cd /
ls
pwd
cd 
pwd
```

Notice when you run hello world with and without quotes how the output changes.

```shell
echo hello       world
echo "hello      world"
```

Now we are going to use the Word Count command wc to check the characters, words and lines in files.

>__Question:__ Do you see password values in the passwd file?

```shell
wc /etc/passwd
cat /etc/passwd
wc /etc/group
cat /etc/group
```

Now we are going to create directories and files and then try to move and delete those files and directories.

>__Question:__ Can you use **rmdir** to remove a directory that contains files?

```shell
cd /
mkdir /tmp/TEST
ls /tmp/TEST
cd /tmp/TEST
touch testfile.txt
ls 
file testfile.txt
rmdir /tmp/TEST
rm testfile.txt
rmdir /tmp/TEST
```

Now we are going to move files and remove files.

>__Question:__ What does the -i option with rm do?

```shell
cd /tmp
touch my_data_file.txt
mv my_data_file.txt my_renamed_data_file.txt
ls *txt
rm -i my_renamed_data_file.txt
rm --help
```

Here are some sample commands from the list above showing how the commands may run.

![example ls command](cmd-list.png)

## Create another local account

>Objective: Create a new user that is a member of the group `admin`.  The group admin is something you will also need to create.

First we need to create a group called admin, and print out or `cat` the /etc/group file to see if your group was added.  Notice that we must use sudo to elevate our rights here as the average user is not allowed to create system groups.  The first groupadd example below will fail...then you prepend `sudo` and it will work!

```shell
/usr/bin/sbin/groupadd admin
sudo /usr/sbin/groupadd admin
cat /etc/group
```

Next create a local account for the user Dennis Ritchie.  Once you have created the user view the contents of the /etc/passwd file and see if you can find the new line created for Dennis.

```shell
sudo /usr/sbin/useradd -c "Dennis Ritchie"  -G admin dritchie
cat /etc/passwd
```

Next we are going to set a password for this new account.  Notice when you try to run the command without using sudo it fails to run.

```shell
/usr/bin/passwd dritchie
sudo /usr/bin/passwd dritchie
```

Here is a screenshot of what it would look like to create a new account for the user Student One with a userid of __student1__ and set the password.

![Add user](add_user.png)

## Directory structure review

> The objective here is to discover some of the directories and files on your system as you become more comfortable at the command line.

Open up two SSH sessions to your development host and place them side by side.

Change directories to the root of the system and list the contents of that directory in the first SSH session.

```shell
cd /
ls
```

In your second SSH session change directory to /usr and ls the contents of that directory.

```shell
cd /usr
ls
```

Compare these two windows, do you see duplicate directories?

In either SSH session change directories to /var/log and list the contents.  What sort of file names do you see?  This is where you find most of the logs on the system.  

```shell
cd /var/log
ls
```

## Real USER vs Effective USER

Next we are going to review some more of the commands discussed in the lecture.

* use the "who" command to determine which account you are logged in as.
* use the "whoami" command to confirm this.
* use "who am i" now and review the difference in the output.

```bash
who
whoami
who am i
```

Now using sudo become the root user

```shell
sudo su - root
```

now repeat the who , whoami and who am i commands.  What differences do you see?

Next compare the differences between **cat** and **echo**. One command will print the contents of a file to the screen and the other simply echos back what you enter, which command prints the contents of a file to your screen for you to read?

```shell
/bin/cat /etc/passwd
/bin/echo /etc/passwd
```

Can you run the **cat** and **echo** command without prepending /bin to each command?

## Navigating the filesystem with **cd**

Here we are confirming the behavior of the **cd** or **Change Directory** command.  Notice how we can use **cd** and a path to change directories.  Also notice what happens when we just enter the command **cd** with no file path.

* Using **cd** move to /var/log.
* Use /bin/pwd to confirm your working or current directory.
* Enter **cd** with no additional options and use /bin/pwd to reveal your location. 
* Finally enter cd followed by a dash "-" and then /bin/pwd.
* Did your working directory change and if so to where?

```bash
cd /var/log
/bin/pwd
cd
/bin/pwd
cd -
/bin/pwd
```

## Creating directories with **mkdir**

Using **mkdir** create a directory under /tmp called myTestDir

``` shell
/usr/bin/mkdir /tmp/myTestDir
```

Confirm it exists using the /bin/ls and then remove the directory using **/usr/bin/rmdir** as shown in the next example

```shell
/usr/bin/ls /tmp/myTestDir
/usr/bin/rmdir /tmp/myTestDir
/usr/bin/ls /tmp/myTestDir
```

The /bin/touch command is another simple utility.  The purpose of this command is to change file timestamps but it will also create empty files.  Using touch now create an empty file under tmp called testDel.txt

``` shell
/usr/bin/touch /tmp/testDel.txt
```

Now remove the file

```shell
/usr/bin/rm -i /tmp/testDel.txt
/usr/bin/rm --help
```

What did the -i option do? what does the --help option do?

## pty and tty sessions

Linux has the concept of a physical terminal and a pseudo terminal.  A physical terminal means you are directly connected to the console. When we use the vmware application or the UTM application and sign in without using putty or a terminal we have established a physical connection.  When we use putty or terminal these applications are emulating a physical connection and are referred to as pseudo terminals.

You can see an example of this now if you open up your VMware or UTM console and sign in.  Once logged in run **who** to see a list of active connections.  You should see some lines include pty for a pseudo terminal and some show tty for a physical terminal. Try to start a new Putty or Terminal based connection and then run **who** again. Can you see any new lines displayed by the who command and do they indicate a pty session or a tty session?

___
Lab complete
___
