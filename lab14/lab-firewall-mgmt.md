# In class lab: IPTables and Firewalld

>__Objective:__ Gain a basic understanding of how to manage local host firewall rules

>__Note:__ It is assumed that you are doing this lab on your **development** host and not your production server.

In this lab we are going to work with firewalld and also get some exposure to the systemctl command which is part of the Systemd design and the preferred way to manage services on RHEL and CentOS.

First check to see if you have iptables, nftables or firewalld installed? Notice how we are searching for two values with grep in this command. The syntax is simpler with newer grep tools such as egrep. This grep command will look for iptables or firewalld.

```bash
dnf list installed | grep "nftables\|firewalld"
dnf list installed | grep "iptables"
```

If iptables is installed and running disable it now. In most cases iptables will not be installed so this commands may result in errors or command not found.

```bash
sudo systemctl stop iptables 
sudo systemctl disable iptables 
```

![iptables service output](expected_iptables_output.png)

If you wanted to install iptables; note we do **NOT** want to install iptables, you would run the following command.

>sudo dnf install iptables iptables-services


If firewalld is not installed now, we can install it now.  What can you add to the following dnf install command to automate the install and skip the step to accept the install by pressing Y/y?

>__Note:__ The UI tool for firewalld is **firewall-config** but we are not using the graphical interface

```shell
sudo dnf install firewalld
```

Now we will enable firewalld, start and check the status of firewalld.   Note the the output from the enable command, what does it tell you, does the process create file or script?

>__Note__ To both start and enable firewalld use **systemctl enable firewalld --now**

```shell
sudo systemctl enable firewalld
sudo systemctl start firewalld
sudo systemctl status -l firewalld
```

>__Note:__ If you see a warning regarding **AllowZoneDrifting** you can disable it.  [How to disable AllowZoneDrifting ](AllowZoneDrifting.html)

Now we are going to list available and active zones using firewall-cmd another new tool.

```shell
sudo firewall-cmd --get-zones
sudo firewall-cmd --get-default-zone
```

![active zone](public-zone.png)

Now we can set a variable to our active zone. Using the active zone variable we can list the configured ports for your active zone.

```shell
DEFAULT_ZONE=`firewall-cmd --get-default-zone`
sudo firewall-cmd --zone=${DEFAULT_ZONE} --list-all
```

Here is an example of how to add and how to remove a rule.  What happens if you try to add a port twice?

>__Note:__ In a previous lab you may have added http already, we are adding port 80 tcp/udp now mostly for the edification purposes.

```shell
sudo firewall-cmd --zone=${DEFAULT_ZONE} --list-all
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --add-port=80/tcp
sudo firewall-cmd --zone=${DEFAULT_ZONE} --list-all
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --add-port=80/udp
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --add-port=80/udp
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --add-port=8080/udp
sudo firewall-cmd --zone=${DEFAULT_ZONE} --list-all
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --remove-port=8080/udp
sudo firewall-cmd --zone=${DEFAULT_ZONE} --list-all
```

We use the **--permanent** option to ensure that if the system is restarted this firewall rule will be applied again.  

The next command shows an example of how to list the ports defined to firewalld.  The reload command will apply the updates.

```bash
sudo firewall-cmd --list-ports
sudo firewall-cmd --reload 
```

The next commands show us how to see services defined, we could have defined the service http in this case to also open up port 80.

```bash
sudo firewall-cmd --get-services
sudo firewall-cmd --zone=${DEFAULT_ZONE} --list-all
sudo firewall-cmd --state
```

What about removing a rule?  it is a very similar process.

```bash
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --add-service=https
sudo firewall-cmd --reload 
sudo firewall-cmd --list-service
sudo firewall-cmd --zone=${DEFAULT_ZONE} --permanent --remove-service=https
sudo firewall-cmd --reload 
sudo firewall-cmd --list-service
```

## Working with and managing services using systemd

You can view the existing list of services managed by systemctl entering just systemctl with no arguments.  You might grep the output for a keyword if you were checking on a service.

```bash
systemctl
systemctl | grep "ssh\|nginx"
```

![systemctl list](systemctl-services.png)

The systemd basic part is called a "unit".  You can view the available units using:

```bash
sudo systemctl -t help
```

Working with systemd targets replace runlevels you can see the default target using.

```bash
systemctl get-default
```

Services are managed using systemctl in CentOS/RHEL 7.  Here are some options for working with services using systemd.

```bash
sudo systemctl --type=service
```

Shows the state of services, running, failed, exited, etc. using list-unit-files

```bash
sudo systemctl list-unit-files --type=service
```

We will cover systemd and service management in further detail later in the quarter.

## Git and Markdown

Now we are going to add some more notes to our Git repo and update our README.md file to point to these notes.

First make sure you are in the correct folder.

```bash
cd ~/git/gitlab/itfdn-140-inclass-$USER
```

Next we are going to create a file called firewall-tips.md and add some initial content.  Copy the next code snippet that starts with **echo**.  This should create the file **firewall-tips.md**.

```bash
echo "# Firewalld and firewall-cmd
The following are notes and tips related to **firewalld** and **firewall-cmd**
You can check the status, stop and start firewalld with the following commands.
" >> ~/git/gitlab/itfdn-140-inclass-$USER/firewall-tips.md
```

Next you can add a link from **README.md** file to the **firewall-tips.md** file.  The syntax for this using Markdown is \[description\]\(file\).  Copy the following markdown example to your README.txt file to add this update.  

```bash
[firewalld and firewall-cmd](firewall-tips.md)
```

Finally add these updates to your gitlab project, commit the changes and then push them to your remote repository.

___

## Lab Complete

___