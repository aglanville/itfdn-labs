# How to disable AllowZoneDrifting in Firewalld

1. Back up your existing firewalld.conf
2. Change AllowZoneDrifting=yes to no
3. restart the firewalld service


>**Question:** What is the sed "-i.orig" option doing in the command below? hint check the main page for **-i**.

```bash
sudo sed "-i.orig"  s/AllowZoneDrifting=yes/AllowZoneDrifting=no/g /etc/firewalld/firewalld.conf
sudo diff /etc/firewalld/firewalld.conf /etc/firewalld/firewalld.conf.orig
sudo systemctl restart firewalld
```

For more details see: [https://firewalld.org/2020/01/allowzonedrifting](https://firewalld.org/2020/01/allowzonedrifting)

___
___