# Class hint file

## Hint file

Try to figure it out but if you need help review these examples.  Keep in mind there are alternative answers to the questions.  When working at the Linux command line there are always a variety of solutions to the problem.

```
cut -d: -f1 /etc/passwd
find /etc/*conf | xargs grep example | awk '{print $1}' | uniq -c
cat gettysberg.txt | tr -s ' ' '\t'
```

## Re direction lab

```shell
find /var -name "*log" 2> /tmp/find_error.txt
```
