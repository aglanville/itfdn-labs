# In Class Lab 3

>__Objective:__ To work with some of the common tools discussed in the lecture. In this lab we are creating shell one liner commands, meaning that we stringing together one or more Linux utilities to create the desired output.

>__Note:__ many of these solutions will utilize the pipe "|" symbol which we have not discussed yet but will soon.

>__Note:__ It is assumed that you are doing this lab on your development server and **NOT** your production server.

You must open your VMWare player or Fusion application and then start your Development server (**itfdn-rhel8-dev01** or **itfdn-dev01**) in order to work through this lab.

**Mac users**

![Fusion start guest](../fusion-start-guest.png)

**Windows users**

![Player start guest](../vmware-player-guest.png)

___

## Tasks

1. Using **cut** pull all the usernames from the passwd file, the output should look similar to the following screenshot. First view the file using `cat /etc/passwd`.  Can you see how the fields in this file are delimited?  Next use `cut` with the -d and -f options to define the delimiter and the field you want to print out to your screen.

    ![Using cut](cut-passwd.png)

Click here if you want some [additional hints](../hints/lab3-hints.html).

2. The /etc/passwd file also includes the SHELL value for each account.  Most users have a bash shell defined `/bin/bash`.  Which field in the /etc/passwd file contains this value?  Using your new cut skills see if you can cut out just the shell values.  

>__Stretch task:__ Can you update your results to only show values once?  Can you include a count for each value?  This will require using the pipe "|" symbol.  WE have not talked about the pipe "|" symbol yet but will soon.  Your output might look something like this:

![default shell sorting](default_shell-1.png)

Click here if you want some [additional hints](../hints/lab3-hints.html).

1. Consider the following command output.  How might you be able to only display just the **clean_requirements_on_remove** value for your yum.config file? meaning just output __True__.  If you look closely you may notice that the the variable and value are separated by something other than a colon, what is the new delimiter and can you use cut to filter the fields?

``` shell
grep **clean_requirements_on_remove** /etc/yum.conf 
```

Click here if you want some [additional hints](../hints/lab3-hints.html).

4. In the following **awk** command examples we print out fields based on white space delimiters. We can print out select fields or all fields.  In this example we are sending the output from `who` into the awk command to be parsed. We can insert a tab between each field if desired also using the `"\t"` symbol as shown in the example.  We can also have `awk` include the total number of fields found in the output.  We do this using the number function (NF).

``` shell
who 
who | awk '{print $2}'
who | awk '{print $5 "\t" $1 }'
who | awk '{print NF $0}'
who | awk '{print NF "\t" $1 "\t" $2 "\t" $3}'
```

Can you just print out the date field?

Click here if you want some [additional hints](../hints/lab3-hints.html).

5. Next using **find** and **xargs** locate any file under /etc/ that ends in `.conf` and includes references to "example".  Next limit the output to just one instance of each file that meets these requirements.  This first example is too broad it is looking at everything from root "/" down, how can you update the path to start the search under /etc/..

```shell
find / -type f -name "*.conf"
```

Click here if you want some [additional hints](../hints/lab3-hints.html).

6. Using sed output just lines 5 through 9 from /etc/passwd.  Now using sed print the contents of /etc/passwd without including lines 5 through 9.  There are examples in the lecture slides for sed.  

```shell
cat /etc/passwd 
cat /etc/passwd  | wc -l
sed 2,3p /etc/passwd 
sed 2,3p /etc/passwd | wc -l
sed 10,30d /etc/passwd | 
sed 10,30d /etc/passwd | wc -l
sed -n 2,6p /etc/passwd
sed -n 2,6p /etc/passwd | wc -l
```

Click here if you want some [additional hints](../hints/lab3-hints.html)

7.  Print to stdout (your screen) just the usernames from /etc/passwd sorted in reverse.  Try to do this using the commands covered in the lecture such as cat and sort.  Remember you can look for help using `man sort` or `sort --help` for example.

Click here if you want some [additional hints](../hints/lab3-hints.html)

8.  Print out just your username from /etc/passwd using another shell one line command.  A shell one line command is a series of Linux commands linked together with a '|' pipe symbol.
   
   Click here if you want some [additional hints](../hints/lab3-hints.html)

9.  Install **wget** on your development host using yum.

``` shell
sudo yum install wget -y
```

Next grab a copy of the Gettysburg address (guess who watched Lincoln recently) for your development host.

``` shell
wget https://www.ulcert.uw.edu/gettysburg.txt
```

Now determine how many lines of text are in the Gettysburg address file you copied to your system.  

Using the same file translate each space into a tab and determine how many lines of text that created. Try using --help with tr to see the options.

10. Print to stdout the contents of the Gettysburg address but replace lowercase instances of "the" with uppercase "THE".  hint use sed.

 Click here if you want some [additional hints](../hints/lab3-hints.html)

___
Lab complete
___

