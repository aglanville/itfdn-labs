# In class lab: if then else and how to read input from a user.

>Objective: Adding else to your shell scripts and capturing user input with the `read` function

>Note: It is assumed that you are doing this lab on your **development** host and not your production server

## Review arguments

This section is attempting to show how user provided and system default variables can be used in your scripts.

Create a new script called arguments. Your task is to fix this script. Remember $0 is the script name, $1, $2 etc are the arguments passed in.  $# is the number of arguments provided and $* contains the values of all arguments provided.  The example below has the wrong variables for listed in the echo commands.  Your challenge is to clean those up.

```bash
#!/bin/bash
# Title: arguments.sh
# Author:
# Date:
# Purpose
# Update:

if [ "$#" -ne 3 ]; then
    echo "No arguments detected, please enter: city name, the day(i.e. Wednesday)  and the month (i.e. June)."
    echo "exiting now"
    exit 0
fi

echo "The script name is: $1"
echo "The total arguments provided was: $0"
echo "The day provided was: $*"
echo "All the arguments provided were: $2"
```

Here is what your output may look like

![run arguments script](arguments.png)

Now let's think about an else test with a number guessing game.  Create a new script called guess.sh use your template.sh as the basis and then copy into that script the code example below.  In this example we are using a simple if/else logic block.

Create a script called guess.sh based on your template.sh file and then copy into this new script the logic below.

```bash
myguess=3

echo "Welcome to the number guessing game!"
read -sp "Enter your guess between 1-5: " yourguess

if [ "$myguess" != "$yourguess" ]; then
    echo "$yourguess was not right"
    echo "my number was $myguess"
else
    echo "$yourguess was right"
fi
```

Shell scripts support nested IF statements but these can be also be written using **elif** which is short for else if.  To try this out we are going to update the guess script to include clues using the if/elif/else logic if the user fails to guess the answer. 

-gt greater than
-lt less than
-eq equal too

Create a new script called guess-tips.sh and again use your template.sh as the basis and then copy in the following code example.  

As you might expect there is a minor logic issue with this script, try it out and then fix the problem.

```bash
myguess=3

echo "Welcome to the number guessing game!"
read -sp "Enter your guess between 1-5: " yourguess

if [ "$myguess" -gt "$yourguess" ]; then
    echo "$yourguess was too high"
elif [ "$myguess" -lt "$yourguess" ]; then
    echo "$yourguess was too low"
else
    echo "$yourguess was right"
fi
```

Finally create another script called **day.sh** Again use your template.sh script to start and then copy the code sample below into the script.  Run the script and test the output.  Next, see if you can add one or two more lines to the following script for any day not defined yet.

```bash
read -p "What Day is it? " day

if  [ "$day" = "Monday" ]
    then
    echo "oof, the week is just starting"
elif    [ "$day" = "Friday" ]
    then
    echo "nice, you made it through the week"
elif    [ "$day" = "Sunday" ]
    then
    echo "Well, hopefully you got some chill time this weekend"
else
    echo "Sorry, I don't recognize the day you entered"
fi
```

___

## Lab complete

___


