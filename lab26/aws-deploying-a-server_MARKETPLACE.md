# Deploying a linux server within AWS

>Objective: Deploy a virtual machine

It is assumed that you have already created an SSH key pair for AWS and that you have a VPC defined for your Linux host.  If this is not the case then return to the first two labs to complete those steps now.

We have in this course walked through deploying a linux host on our local machines using either VMWare or Virtual Box as the embeded hypervisor.  We have also worked with a private cloud deployment of Linux which is running on VMware ESX, a bare metal hypervisor.  In this lab we will continue this theme by deploying a Linux server to the AWS public Cloud.

We need to return to AWS via the Vocareum portal which is available from the AWS lab module in canvas.

![Vocareum landing page](vocareum_portal_aws.jpg)

Once you are logged into AWS use the Services search feature to find EC2.

![Services Search](services_search.gif)

On the EC2 landing page click the Launch Instance option.  On the next page select the **AWS Marketplace** tab on the left hand side and enter **centos7** in the search menu.  This should return an option for **CentOS 7(x86_64) - with Updates HVM**. Click "Select" on the right hand side to continue to the next screen.
![Centos AMI Selection](centos_ami_selection.gif)

You can review the next page which will display the expected hourly costs for the different sized AWS virtual machines.  Once done click **Continue**.  I am assuming you are using the UW provided AWS account and as such the **free tier** does not apply so we are going to select the t2.nano host.  If you are using your own AWS account and it is eligible for the free year of service then you should select the t2.micro server. Click **Next: Configure Instance Details** in the bottom right corner to continue.

![t2 nano server](t2_nano.gif)

On Step3 select your network and subnet using the drop down menus for each option.  Next enable Auto-assign Public IP.

![Step 3](step3_network.gif)

If you look a little further down you should see an **Advanced Details** section.  Expand this section. Here you can add steps you would like your server to complete after the installation completes.  The following simple shell script should update the system and install nginx.

```
#!/bin/bash
# update the system
sudo yum update -y
# install epel repo, required for the nginx install
sudo yum install epel-release -y
# install and start nginx
sudo yum install nginx -y
sudo systemctl start nginx
```
![Advanced Details](step3-adv_details.gif)

When done click **Next: Add Storage**  We are going to leave the storage size set to 8Gb and move on to adding tags.  Click **Next: Add Tags**.  Here we want to add one Name tag with a value like **name-www**. Click **Next: Configure Security Group** to continue.

![Name tag](add_tag.gif)

Here we are going to define rules to allow ICMP, SSH, and HTTP traffic to our AWS hosted CentOS linux server. We need to limit the traffic to our host as much as possible to do this figure out your IP address range using a site like [What is my IP](https://www.whatismyip.com/ip-address-lookup/).

![AWS security group](aws_security_group.gif)

Once done click **Review and Launch**.  ON step7 review the options selected and click **Launch**. On the next pop up select your existing SSH key pair and click **Launch Instance**.

![Review Options](step7_review.gif)

![Select keys](select_key_pair.gif)

The install wll take a couple minutes to complete the install.  Once complete click the instance id to be redirected to the EC2 landing page and review the status of your new linux host.

![Launch Status](launch_status.gif).

The EC2 details will show the external IP details and the internal IP address which we expect to fall in the 10.9.8.0/24 subnet.  Test your server using ping from a command line.  Next test your web server using a browser or curl and the public IP address.  Finally try to connect to the linux host using ssh with a username of **centos**, your private key and the public IP address defined by AWS.

![Linux host](linux_host.gif)
