# In class lab: Deploying a Linux server within the AWS cloud

>**Objective**: Deploy a virtual machine in AWS

It is assumed that you have already created an SSH key pair for AWS in [Lab 25 AWS Networking](https://www.ulcert.uw.edu/itfdn-labs/lab25/aws_networking.html)  and created a **Virtual Private Cloud (VPC)** in the same lab as they will be required in this lab for your host.  So, if this is not the case then return and complete those steps now.

We have in this course walked through deploying a linux host on our local machines using either VMWare or [Virtual Box](https://www.ulcert.uw.edu/itfdn-labs/virtual-box-install/lab-install-rockylinux-on-virtualbox.html) as the embedded hypervisor.  We have also worked with a private cloud deployment of Linux which is running on VMware ESX, a bare metal hypervisor on the UW private cloud.  In this lab we will continue this theme by deploying a Linux server to the AWS public cloud.

We need to return to AWS via the Vocareum portal which is available from the AWS lab module in canvas. Once the Vocareum console is open click **Start Lab** and then **AWS** to login to the AWS console.

![Vocareum landing page](vocareum_portal_aws.png)

Make sure you are in the **us-west-2** Region, also labeled as **US West(Oregon)**

![US West 2](us-west-region.png)

Once you are logged into AWS use the Services search feature to find EC2 and click on the **Dashboard** link.

![Services Search](services-search.png)

Here we expect to see at least 1 key pair the other values maybe set to 0 unless you already have other instances deployed.  Ensure the **New EC2 Experience** is selected and that you are working in the **Oregon** region

Click the **Launch instance** down arrow and click on **Launch Instance**. 

![launch ec2 instance](launch-instance.png)

Here we will configure another instance Linux instance this time based on [**Rocky Linux**](https://rockylinux.org/) which an open source operating system based on the Red Hat source code. It will feel very familiar to you I expect at this point.

First we want to give our instance a name and then search for the [RockyLinux AMI image](https://aws.amazon.com/marketplace/pp/prodview-2otariyxb3mqu).

* Name and tags: yourname-os-version-prod# eg glanville-rl-8-prod1
* Enter **rockylinux** in the search bar for the catalog or click **Browse more AMIs**

![name instance](images/../name-instance.png)

* On the next page ensure you have the AWS MarketPlace AMI tab selected
* Locate Rocky Linux 8 (Official)
* Click **Select**
* Click **Subscribe now** when presented with price details for the image


![select rl 8](images/../select-rockylinux.png)

Now you should return to the **Launch an instance** menu

* The AMI image type should auto fill with **RockyLinux** details
* Instance type: **t2.small**
* Key pair: select your key pair
* Network settings: click **Edit**
   * VPC: **Select your VPC**
   * Subnet: Select your /24 subnet
   * Auto-assign public IP: **Enable**
   * Firewall: select **Create security group**
   * Security group name: provide a name
   * Description: provide a description
   * Inbound security group rules:
      * Security group rule 1: Type= **SSH**, Source= **My IP**
      * Security group rule 2: Type= **HTTP**, Source= **Anywhere**
      * Security group rule 1: Type= **All ICMP**, Source= **Anywhere**

We have defined rules to allow ICMP, SSH, and HTTP traffic to our AWS hosted RHEL8 linux server. We want to limit the SSH traffic to our host as much as possible for security reasons.  

>__Stretch task__ Add the class production network range of 140.142.159.0/24. to your SSH security group rule

You can review the other options and when done click **Launch Instance**

![Configure instance](configure-instance.png)

Next you should see the **Launching instance** menu.  This should only take a minute or two to complete, click **View all instances** when this step is complete.

![launch instance](launch-instance.png)

On the instances menu you should see the **Instance state** once this changes to **running** you should be able to login to your new RHEL 8 server using your AWS generated private key.

![instances](check-instances.png)

Once the instance is running select it and review the configuration details.  You want to find the Public IP value so that you can connect to this host as the **ec2-user**.  Notice that your internal IP address should fall in the 10.9.8.0/24 network.

![instance details](instance-details.png)

>__Note:__ Do you have the private key that you created in [lab 25](https://www.ulcert.uw.edu/itfdn-labs/lab25/aws_networking.html) This should be a pem file probably in your downloads folder called `lab25-key-pair.cer` or `lab25-key-pair.pem`, if not you will never be able to login to this new Linux server and will have to start over.

Now try to connect to the linux host using ssh with a username of **rocky**, with your AWS private key and the public IP address defined by AWS.

The following is an example of the first login from a MAC.  Windows users should load the ppk file into the Putty Agent and then login using a new putty profile.  In this ssh example I am using the `-i` option to point to the identity file (private ssh key). Alternatively I could have added this key to my SSH-Agent using `ssh-add ./lab25-key-pair.cer`.  

>__Note:__ The default pem file permissions are probably too insecure for SSH and will have to be updated.

![ssh login](mac-ssh-login.png)

## Install the Nginx HTTP web server

Return to [Lab 13](https://www.ulcert.uw.edu/itfdn-labs/lab13/lab-web-server-install.html) for help with installing the Nginx web server.

___

## Troubleshooting tips

___

**Q**. My launch fails with a message about the ICMP security group.

**A**. Try to change the order and put the ICMP rule after the SSH rule or just remove it and add it manually later.  This feels like an AWS bug as the rule says to allow all ports for ICMP.

![security group fail](security-group-fail.png)

Q. My linux server private ip is not in the expected range (10.9.8.0/24)

A. Make sure selected your VPC when creating the instance otherwise you may have been assigned to the default VPC with a different IP range.

![wrong private ip](wrong-private-ip.png)

**Q**. I am unable to login the error shows too many login attempts and my ssh connection fails.

**A**  Try to pass in the arguments to explicitly use a designated key and then also to point that key.  

![ use only one ssh key](ssh-limit-keys.png)

If this fails then you might try to clear any keys loaded into your SSH agent using `ssh-add -D`.  Review the following screenshot to see the error, how to clear the keys and then a successful login.

![ failed ssh login ](too-many-failures.png)

**Q**. My image fails to load with an error that it is not supported in the region.

**A**. Create a new subnet for your VPC in one of the regions that is supported.  This is a rare error but does occur at times.
[AWS instance not supported in Region](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-INSTANCE_TYPE_NOT_SUPPORTED-error.html).  

**Q**. I don't know what to use for my putty or mac terminal ssh connection host name?

**A**. Return to __Services__ and search for __EC2__.  Next select __Instances__ from the left hand menu and look for your Public IPv4 DNS address or Public IPv4 Address in the middle view.

![AWS public address](aws-public-address.png)

---

### Lab Complete

___