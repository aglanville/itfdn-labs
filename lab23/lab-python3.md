# In class lab: Python

>Objective: Learn how to install Python2 and 3, create a python template and start to write python scripts.

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

## Installing python

First check to see if python 2 and 3 are installed, it is likely that neither are installed by default.

```bash
 python --version
 python3 --version
```

If either returns "command not found" then that version or versions is not installed.

![no such file](python-command-not-found.jpg)

First we will install Python2, even though this is a very old version many tools still look for Python2.  

>__Note:__ There are no special repositories required for this install as it is coming from the standard Red Hat sources.  However, you can get the latest version from [Python.org](https://www.python.org/)

```bash
sudo dnf install python2
python2 --version
```

Next we will install Python3.

>__Question:__ What is the -y doing in the following dnf install command example?

```bash
sudo dnf install python3 -y
python3 --version
```

It is a best practice to point to the version of Python you want to run in your scripts but you can configure a default version if desired.

```bash
which python
python --version
which python2
which python3
sudo alternatives --set python /usr/bin/python3
which python
python --version
ls -la /usr/bin/python
```

![python versions](set-python-default.png)

Now we are going to create a folder under our home directory for python scripts and create a template file.  This is the same as our shell template except that it points to the python3 binary.

```shell
cd
mkdir -p ~/scripts/python
cd scripts/python
vi template.py
```

![python folder](python_folder.gif)

```python
#!/usr/bin/python3
# Title: <script_name>.sh
# Date: 00/00/2022
# Author:
# Purpose: Default python script template
# Update:

print ("This is my python template!")
```

## Writing your first python script

Now create a new python script called myvariables.py This script should define 3 variables.  Next this script should print these variables to the screen in one line with spaces between each word.  Remember that in python we use the + symbol to concatenate strings.

```python
day1 = "monday"
day2 = "tuesday"
day3 = "wednesday"
print (day1 +" " + day2)
```

Next we are going to print out characters based on their index value.  For this exercise we will print out single characters and then ranges of characters.  Finally we will add spaces between the characters. 

```python
print (day1[0])
print (day1[5])
print (day1[0:2])
print(day1[0] + " " + day1[2])
print(day1[3] + " " + day1[4] + " " + day1[5])
```

Now these two lines have errors.  Run the code and consider the error messages. First add the following line, save the file and test it. Note the error and fix the problem.

>**Hint**: look closely at the line above and compare it to the line you just added.

```python
print(day1[3] + " " + day1[4] + " "  day1[5])
```

Now add the following line, save the file, run it, note the error, fix it and the test again.

>**Hint**: How large is index for that variable?  remember 0 is a valid value...

```python
print(day1[0] + " " + day1[6])
```

>**Hint**: Check this link for an image of the [solution](first-py-script.png)

## Working with string format and user input

Remember that the string format method allows for variable substitution. We use the curly braces {} as placeholder.  In this exercise fix the following script so that it prints out:
 "Python is another tool for me to use"

Your objective is to see where you should add the third set of curly {} braces to fix the output.

Create a new file called `python_print.py`, you can do this by coping the template file or reading in the python template as we have done in our shell scripts.

>Note: The output currently prints "Python is another for tool to use".

```python
# <insert template here>
lang = 'Python'
func = 'tool'
owner = 'me'

print ('{} is another for {} to use' .format (lang, func, owner))
```

>**Hint**: Check this link for an image of the [solution](python-print.png)

The next sample is a simple user input field to test out. This script is slightly broken but you can fix it by adding the curly {} braces in the print line at the appropriate place.

Create a file called `python_hello.py` using your python template to start and then add the following content.

```python
# <insert template here>
name = input('enter your name: ')
# hint where are the {} braces that allow for variable substitution?
print ('hello ' .format(name))
```

>**Hint**: Check this link for an image of the [solution](python-hello.png)

___

## **Lab complete**

___

