# Course: Network and System Administration
## In Class Lab: 6
## Name: User management and file permissions

>__Note:__ Complete this work on your **Development** Linux server.
>**Note**: Review the lecture notes for additional tips on how to complete these steps.

>**Objective**: Work with file and folder permissions and review process to create users, groups, folders and files.

You must open your VMWare player or Fusion application and then start your Development server (**itfdn-rhel8-dev01** or **itfdn-dev01**) in order to work through this lab.

**Mac users**

![Fusion start guest](../fusion-start-guest.png)

**Windows users**

![Player start guest](../vmware-player-guest.png)

___

# Tasks

## Creating users

We are going to create two new user accounts; one for [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) and the other for [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie).  

The usernames should be **kthompson** and **dritchie** be sure to include their full names in the comments section. These users should be members of the secondary group research and users.  Take a look at the groups file and see which of these groups already exists.  When creating user accounts it is a best practice to use all lowercase letters.

>Note: The example below only shows the user **kthompson**, you have to repeat the process for **dritchie**.
>Note: there is a minor error in group assignments below.  See if you can spot it or review the next note for another clue.

```shell
cat /etc/group
cat /etc/group | grep users
cat /etc/group | grep research
sudo useradd -c "Ken Thompson" kthompson
sudo groupadd research
sudo usermod -aG research kthompson
groups kthompson
sudo usermod -G users kthompson
groups kthompson
```

>Note: Are the users members of research **AND** users, if not why?  hint you may need to **_append_** your new groups.  This means using the -a and the -G.

Once you have both groups added to kthompson repeat the necessary steps to add Dennis Ritchie with the same groups. 

If you need help setting up these users check this [HINT](../hints/lab6-hints.html)

Next create another group called mentors.  Add this as another secondary group for both Ken and Dennis.  Make sure both users still have their default primary groups in addition to the new group "**mentors**".  Look at the previous example to complete this task.

If you need help setting up these users check this [HINT](../hints/lab6-hints.html)

You can print the contents of /etc/group to your screen now using `cat`  to see your new groups added.  It might look something like the screenshot below but with users defined next to some of the group lines.

![groups](groups.jpg)

Now we need to set the passwords for both of these accounts and test the logins for both of these accounts.  You should be able to start new putty or terminal sessions for both users to test the logins.  This means you should have 3 sessions open, one for each user (yours, dritchie, kthompson).  First you must set the passwords for Ken and Ritchie.  As you are doing this work on your development hosts the passwords can be simple.  Set the password for kthompson **thompson** and the password for dritchie to **ritchie** (note do NOT do this on your production systems as these passwords are far too simple).

If you need a hint regarding how to set the passwords check this [HINT](../hints/lab6-hints.html)


![3 windows putty ssh sessions](3connections-windows.png)

![3 ssh sessions using the mac terminal](3connections-mac.png)

Now login with all three users at the same time.  What in the shell is different? does the shell provide any clues as to your user? 

Try entering a couple commands in each terminal

```shell
who am i
pwd
whoami
```

![3 logins](3logins.png)

Next we are going to modify the shell for Dennis Ritchie and set it to /sbin/nologin.  The result should be that you are not able to login as dritchie after making this change. Next we are going to check the contents of the /etc/shells file and also confirm that we have updated the shell definition for dritchie.

```shell
grep dritchie /etc/passwd
sudo usermod -s /sbin/nologin dritchie
cat /etc/shells
grep dritchie /etc/passwd
```

![no login](nologin.png)

Now change the default shell back to /bin/bash for dritchie.  Review the commands above to see how to make this change.

```shell
sudo usermod -s /bin/bash dritchie
grep dritchie /etc/passwd
```

## File and folder permissions

Now we are going to create a new folder and permission the folder so that other users can view and edit content in that directory.

First log in with your own account create a folder under /usr/share called "teams".  This folder will be provide write access to members of the "research" group.  The users kthompson and dritchie should not need sudo rights to do this once we have the correct permissions in place.  The folder "research" should not allow the other group or users rights to the contents of the folder, including **YOU** (unless you add the research group to your user).

```shell
sudo mkdir /usr/share/teams
sudo chown root:research /usr/share/teams
sudo chmod 770 /usr/share/teams
ls -la /usr/share/teams
cd /usr/share/teams
```

> Note: If you login again now and try to access the folder `/usr/share/teams` you should be **denied**.

![permission denied](perm_denied.png)

Next login in as kthomspon  and create a directory called **bell_plans** under /usr/share/teams.  This folder should be owned by kthompson but the group should be research.  In this folder create a file called userinfo.txt.  Permission this folder so that the group research has read, write and execute permissions.  Other should only have read permissions.  Now login as dritchie and try to update the userdata.txt file but make sure the group ownership remains research.

> Note: the `chown` command will fail, as the user kthompson cannot change owner permissions just group permissions

> Note: The command `touch` creates a blank file, or changes the timestamp for an existing file

```shell
cd /usr/share/teams
mkdir bell_plans
cd bell_plans
touch user_info.txt
cd ..
ls -laR /usr/share/teams/
chown -R root:research bell_plans 
chgrp -R research bell_plans 
ls -laR /usr/share/teams/
```

Next in terminal for dritchie login again and change directory to /usr/share/teams.

```shell
cd /usr/share/teams
ls
whoami
pwd
```

Now login at kthompson and update the file called user_info.txt found in the folder called bell_plans under /usr/share/teams. 

```bash
cd /usr/share/team/bell_plans
vi user_info.txt
```

Add the following details to the file.

```bash
USERNAME        FIRST       LAST        STARTDATE
bjoy            Bill        Joy         11/8/1954
bfox            Brian       Fox         12/11/1959
```

Now log in as dritchie, navigate to the /usr/share/teams/bell_plans directory and update the file user_info.txt.  Add one more line for Richard Stallman and confirm the file and group ownership, if it is not research, update it the ownership.

```shell
cd /usr/share/teams/bell_plans
ls -la *
vi user_info.txt
ls -la *
```

If you are not able to write to the file user_info.txt make sure the group research has read and write access to the file.  This can be added using `chmod g+rw /usr/share/teams/bell_plans/user_info.txt`. What is another way to do change the file permissions?

```shell
rstallman       Richard     Stallman    3/16/1953
```

[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)

---

End of lab

---