# Course: Network and System Administration
## In Class Lab: 0
## Name: Meet the command line and create your Red Hat account

>**Objective**: Gain some experience with the Linux command line interface

>**Objective**: Create your Red Hat account and link your developer license

We are going to use a PC/x86 emulator written in [JavaScript](https://www.youtube.com/watch?v=c-I5S_zTwAc) by [Fabrice Bellard](https://en.wikipedia.org/wiki/Fabrice_Bellard) called [JSLinux](https://bellard.org/jslinux/).  

Fabrice Bellard is a French developer who has contributed a lot to the Open Source community especially around virtualization. Fabrice wrote QEMU (Quick EMUlator) an open source emulator which is often found on systems running Kernel-based Virtual Machine (KVM).  We will be using the commercial solutions from VMWare this quarter but you may run into QEMU in your IT career.

JavaScript is one of the core web technologies alongside HTML and CSS.  JavaScript is a client side solution that runs in the browser (Chrome, Edge, Firefox). JavaScript is one of the most popular programming languages if not number 1.  We do not teach JavaScript in this class but we will cover shell scripting and later in the course introduce Python.  The logic used for Shell and Python is very similar to what you will find in JavaScript and other programming languages. If HTML, CSS and JS are of interest to you consider the [UW PCE certificate program in Front-End development with HTML, CSS and JavaScript](https://www.pce.uw.edu/certificates/front-end-development-with-html-css-and-javascript).

## Launching Linux

We are going to launch the [Fedora](https://getfedora.org/) based Linux system from JSLinux. Fedora is based on the same code base as RedHat Enterprise Linux that we will focus on during this course. As we will be working from the command line a lot this quarter this is a good opportunity to demonstrate what that will look and feel like.

First go to [https://bellard.org/jslinux](https://bellard.org/jslinux/vm.html?cpu=riscv64&url=fedora33-riscv.cfg&mem=256)

This should launch a terminal in your browser that looks like the screenshot below.  You are logged in as root on a system running a recent version of Fedora Linux.

![run linux](images/cli-demo-jslinux.png)

Here you can start to enter some commands such as:

>__Note:__ Make sure you click on the terminal image in your browser to activate your session so that you can begin to enter text.

```bash
whoami
pwd
cd /
ls
cd
ls
clear
cat /etc/redhat-release
file /etc/redhat-release
file /bin/bash
file --help
```

Here is what this might look like in practice.

![cli demo](images/cli-demo-jslinux.gif)

We are going to talk about this more tonight and this quarter but these are some simple examples of the sorts of commands you can run from the cli (Command Line Interface).

## Create your Red Hat account

Now we need to head over to Red Hat and create a new account.  I suggest you use your UW email address but that is your choice.  You will get some emails from Red Hat after registering for an account.

1. Go to [Redhat.com](https://www.redhat.com/en)
2. Click on Account in the top right corner

![redhat.com](../lab0/images/redhat-com.png)

3. Click on **Register now** and complete the registration process.

![redhat registration](../lab0/images/redhat-registration.png)

4. Once your registration is complete go to [developers.redhat.com](https://developers.redhat.com/) and login.  You should see a prompt to accept license agreements and then you will be logged in and your Red Hat developer license will be activated.

![redhat subscriptions](../lab1/rhel8/images/rhel8-subscriptions.png)

You should be ready for the next lab now.

## Troubleshooting tips

If you don't see anything in the terminal when you enter text make sure you have clicked into the terminal with your mouse first.  This will focus your keyboard input onto that terminal session.

If you see an error "No such file or directory" be sure you have the right file name.  Try to use **tab** completion, ask if you are not sure what that is.

![file not found](../lab0/images/file-not-found.png)

[Return to Canvas Modules](https://canvas.uw.edu/courses/1703394)

---

Lab complete

---
