# In class lab

>Objective: Work with local network tools and configuration files.

>Note: It is expected that you are doing this work on your development linux host and **NOT** your production server.

>Note: Review the lecture notes for additional tips on how to complete these steps.

It is possible that your primary network interface device id is **NOT** ens33, if it is differnt you will need to replace `ens33` in these examples with your device name.

Print out your network interface details using ip and compare the difference using the -4 flag. Print out just the loopback or just the ethernet interface details. 

```
ip addr
ip -4 addr
ip addr show ens33
ip addr show lo
```

![network interface](network-interface.png)

Display detailed ethernet values using ethtool, note the Duplex and speed settings. Note the driver details with -i and check the latest set of statistics using -S
  
```
ethtool ens33
ethtool -i ens33
ethtool -S ens33
ethtool --help
```

Change the link status, the physical connection to your virtual machine.

>Warning: These commands may disable your system, don't run them on your production host.

```
ip link
sudo ip link set lo down
ip link show lo
ip link show ens33
ip link
sudo ip link set lo up
ip link
ip link set ens33 down
ip link show ens33 
ip link set ens33 up
ip link
```

## Configuration files

>Note: file names are based on device names, your file name may be different

Locate your network configuration files

![network scripts](network-configs.png)

Review your network configuration files.  Note your BOOTPROTO setting.  Note your onboot setting, review the other settings.

```
cat /etc/sysconfig/network-scripts/ifcg-ens33
```

![ethernet device configuration](ifcf-ens33.png)

## Configure your development host with a static IP.

The IP used needs to in the same range as what your VMWare is providing.  We will talk about networks, subnets and routes later but for now just assume that if your current IP address is 192.168.122.129 that you should either keep that address or pick a number in the last octet that is very close such as 192.168.122.130 or 131 for example.

We are going to backup our config file as it is good practice to backup system files before making changes.  Then we will change the BOOTPROTO line to none, confirm ONBOOT is set to yes, add our desired IP address with IPADDR, add our NETMASK, GATEWAY and DNS server.  Make these changes using Vim or your preferred command line editor.

```
cp /etc/sysconfig/network-scripts/ifcfg-ens33 ~/ifcfg-ens33.backup
```

![back up config](ifcfg-backup.png)

Next locate your default gateway value using `ip route` your are looking for the value defined for "default".  This is also your DNS server.

![default gateway](gateway.png)

In my example my final config file will look like this:

![static ip](ifcfg-static.png)

Restart the network service after making the updates.

## Lab Complete