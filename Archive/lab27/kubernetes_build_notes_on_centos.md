# Local Kubernetes build notes for CentOS 7

>Objective: Create a kubernetes environment with a single master and two worker nodes.  This is for a demo purposes in production you would always install at least 3 master nodes.

## Install Centos7
First we need to create 3 new virtual machines.  One will be the master, 2 will be nodes.  This should each have 1GB RAM and 8GB disk.

You can use the lab notes from building our dev host as a guide [Building a dev host](https://www.ulcert.uw.edu/labs/lab1a/itfdn140-spring-19-lab1a.html).

Things to consider:

* No need to partition the disk in this case, use the defaults
* Change the hostname to make cenkubeX.localdomain where X is replaced with 1, 2 or 3.
* Update the sudoers file and remove the password requirement (find the wheel group line with no password required)
* Update your installs (sudo yum update -y)
* Confirm your hostnames in /etc/sysconfig/hostname
* update hosts file with something similar to the following. You will need to update the IP addresses and hostnames to match your environment.
```
10.16.XXX.XXX   cenkube1 cenkube1.localdomain
10.16.XXX.XXX   cenkube2 cenkube2.localdomain
10.16.XXX.XXX   cenkube3 cenkube3.localdomain
```

2. Disable SELInux by updating the file /etc/sysconfig/selinux.  Change enforcing  to disabled.
3. disable swap using the command and updating the fstab file.
```
# sudo swappoff -a
```
4. Next edit /etc/fstab and comment out the swap line.
![Fstab file](fstab-swap.gif)

5. install yum utils and update a couple other packages
```
yum install yum-utils device-mapper-persistent-data lvm2 -y
```

6. Next add the docker repo
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
7. install a specfic version of docker, first list available versions, find the one you want and install it.
```
# sudo yum --showduplicates list docker-ce
# sudo yum install docker-ce-18.03.1.ce-1.el7.centos -y
```
8. Next we are going to create another yum repo file, this time for kubernetes.  First 
create the file **/etc/yum.repos.d/kubernetes.repo**.  Then paste the following contents into this new file.

```
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
```

9. Next install kubelet, kubeadm and kubectl
```
# sudo yum install kubelet kubeadm kubectl -y
```
10. Next add your user to the docker group, and reboot.
```
# sudo usermod -a -G docker $USER
# sudo reboot
```
11. After logging back in we need to start the docker and kubelet services and enable then to autostart in the future.
```
# sudo systemctl start docker kubelet
# sudo systemctl enable docker kubelet
```
12. Next run docker info and make review the output for any errors.  You should not need sudo to run the docker commands if your are a member of the docker group.  If you have issues, restart the docker daemon and log out and back into your linux host.
```
# sudo systemctl restart docker
# docker info
```
If you see the following error 
> WARNING: bridge-nf-call-iptables is disabled
try to enable the bridge using
```
sudo sysctl net.bridge.bridge-nf-call-ip6tables=1
sudo sysctl net.bridge.bridge-nf-call-iptables=1
```

13. Next we are going to stop and disable firewalld.  Later you can review the traffic and create a rule set to support our kubernetes cluster.

```
# sudo systemctl stop firewalld
# sudo systemctl disable firewalld
```
14. Next we are going to initialize and launch our kubernetes master instance.
```
# sudo kubeadm init --apiserver-advertise-address=10.16.15.132 --pod-network-cidr=10.16.15.0/24
```
Assuming all goes well you will be presented with output informing you how to configure your admin users shell and the token to use when joining nodes to your master instance. It will look something like the following.
```
Your Kubernetes master has initialized successfully

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kub
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

  kubeadm join 10.16.XX.XXX:6443 --token YYYYYZZZZZ.AAAXXXXXXX --discovery-token-ca-cert-hash sha256:8444f27bfacb414e2143e8e0ad387102c41e09206c09892da06fb4ec68085125
  ```
## Define the overlay network
Next we are going to deploy flannel our overlay network.  This will allows the pods, nodes and containers to talk to each other.

## confirming that your kube master is starting.
Next we can run a couple quick commands to confirm that our kubernetes master is online.
```
# kubectl get nodes
# kubectl get pods --all-namespaces
```
Some of your services may be starting or pending which is expected at this point.

## Adding nodes to your kube cluster.
Now login to cenkube2 and run through steps 1 to 13.  Step 14 is where you will add your node to the cluster.

The command will look something like this but your token value and master IP will be different.
```
kubeadm join 10.16.15.132:6443 --token 958ioz.mj0yevhe9j03c0iw --discovery-token-ca-cert-hash sha256:8444f27bfacb414e2143e8e0ad387102c41e09206c09892da06fb4ec68085125
```
Rerun the get pods command to see if you now have containers being created.

## add your last node to the k8s cluster.
Now run through steps 1 through 13 again for the second node in your cluster.  Step 14 will again be joining this node to your custer again with the kubeadm command.

Once all are joined run the get nodes command again to confirm that you have one master and two workers.

## Deploying a pod to your cluster
Now we are going to deploy nginx to our cluster.  Login to your master to create the new deployment and list the details of the deployment.
```
# kubectl create deployment nginx --image=nginx
# kubectl describe deployment nginx
```
Now we are going to expose nginx outside the cluster using a NodePort. Once defined we will list our new pods and the svc we have created.
```
kubectl create service nodeport nginx --tcp=80:80
kubectl get pods
kubectl get svcs
```
Review the output and connect to the nodes using the ports defined in the output from pods.
```
# curl cenkube2:XXXX
# curl cenkube3:XXXX
```
Next connect to the nodeport IP from the kube master and from your desktop.
```
# curl http://10.X.X.X

Post questions and comments to the TEAMS channel and if nothing else maybe screen shot the output from your kubectl get svcs command and post it to TEAMS.

Enjoy!