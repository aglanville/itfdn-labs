# Introduction to Ansible

>Objective: install and run some simple Ansible ah-hoc commands and playbooks.

In this lab we are going to use your development host as the Ansible master and run commands against your production and AWS hosts.

## Install Ansible
This is expected to be fairly simple.  You can use Yum, pip3 or install from source.  In this lab choose from either a pip or yum install.

### Yum install Ansible

You should already have the epel (Extra Packages for Enterprise Linux) repository installed but just in case that step is listed below.  What is the -y doing in the yum install command below?  I expect you to find Ansible version 2.x installed when running the version command.

```
sudo yum install epel-release
sudo yum install ansible -y
ansible --version
```

### Running ad-hoc commands

Next we want to run some ah-hoc commands against your prod host.  The first example is just a simple ping using the ping module (-m ping) with the inventory item of your prod host (-i ulc-XXX.ulcert.uw.edu).  The -all command says run this against all hosts in the inventory provided.

Remember your Ansible host is going to authenticate using your SSH keys.  So, you must have your SSH key loaded.  You will want to do this for your Prod key and your AWS key.  Assuming you have a key for aws called **aws.key** in your .ssh directory and it is owned by you and the permissions are set to 600 you would load and list that key like this.

```
ssh-add ~/.ssh/aws.key
ssh-add -l
```

If you receive an error like "Could not open a connection to your authentication agent" you may need to start the ssh agent.  Review the following screenshot for tips on how to do this.

![ssh agent](ssh-agent.jpg)

Now we can try to run these ad-hoc ansible commands.  You can use SSH passwords to authenticate by using the -k option but I really want you to use your ssh keys.

```
# ansible all -i ulc-XXX.ulcert.uw.edu, -m ping
# ansible all -i ulc-XXX.ulcert.uw.edu, -m shell -a '/bin/echo hello $USER!'
```

![ping](ansible-ping.jpg)

### Running playbooks

This time we need to define an inventory file.  We are going to make an ansible directory, create an inventory file and then create a playbook to check if Nginx is running.

```
# mkdir -p ~/ansible/playbook
```

First we need to create an inventory file.  We will define a block for __all__ hosts but this actually exists by default but having it helps to remind us that it is valid group. Next we will create one for our prod hosts and then our AWS hosts.  Be sure to update the values to reflect your hosts.  Create this file under ~/ansible

```
[all]
ulc-190 ansible_host=ulc-XXX.ulcert.uw.edu
aws-1   ansible_host=3.83.XX.XX

[aws]
aws-1  	ansible_host=3.83.XX.XX

[prod]
ulc-190 ansible_host=ulc-XXX.ulcert.uw.edu
```

Next we need to create a playbook. Playbook are written in YAML and starts with three dashes.  

create a file called nginx-latest.yml with the following content under ~/ansible/playbook.

```
---
- hosts: all
  tasks:
    - name: ensure nginx is at the latest version
      yum: name=nginx state=latest
```

Now run this playbook using

```
# ansible-playbook -i ~/ansible/inventory ~/ansible/playbook/nginx-latest.yml
```

Next we want to see if nginx is running on prod
create a file called nginx-started.yml with the following content.

```
---
- hosts: prod
  tasks:
    - name: start nginx
      service:
          name: nginx
          state: started
      become: yes
```

run this playbook

And now we can stop Nginx

```
---
- hosts: prod
  tasks:
    - name: start nginx
      service:
          name: nginx
          state: stopped
      become: yes
```

Finally run this playbook.

End lab.







