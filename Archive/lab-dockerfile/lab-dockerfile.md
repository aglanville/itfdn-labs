# In class lab: Dockerfile

>Objective: Work with Dockerfile and create a custom container

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

## Prep

It is a best practice to start your build process in an empty directory.  You can create a .dockerignore file which works much like the .gitignore file.

So to do this create a new directory now called webdev1 in your home directory.

```
mkdir ~/webdev1
```

## HTML static site

We are going to quickly mock up a static web site for testing purposes.  This site will have index.html file, a style.css file and a couple images files in a directory called images.  Create these files in the webdev1 directory you just created.

Here is a sample index.html file.

```
<!DOCTYPE html>
<html lang="en">

<head>
// tells the browser what sort of encodig to expect
  <meta charset="UTF-8">
  <title>Custom Docker Container</title>
  // cascading style sheets can be embeded but using an external file is more flexible.
  <link rel="stylesheet" href="css/style.css">
</head>

<header>
    <h1>
    <p>Site navigation</p>
    </h1>
    <nav>
      <ul>
        <li>
          <a href="#home">Home</a>
        </li>
        <li>
          <a href="#docs">Documents</a>
        </li>
      </ul>
    </nav>
  </header>

<body>
<h1>Docker, Dockerfile, HTML and CSS.<h1>
<p>Hello and welcome to my static web site running in a docker container. <p>
</body>

<section>
  <section id="home">
    <h1>Here is the primary introduction for the site.</h1>
  </section>
  <section id="docs">
    <h1>Add content here for your site</h1>
  </section>
</section>

</html>
<footer>
    <small>Demo site used to review Docker, HTML and CSS basics.</small>
</footer>
```

CSS sample file

```
html {
    margin: 0;
    padding: 0;
    background-color: #777;
    margin: 0 auto;

}
body {
    width: 70%;
    margin-right: auto;
    margin-left: auto;
    font: 100% Arial, Helvetica, sans-serif;
   /* padding: 1em 50 px; */
    padding: 2.5% 2.5% 0;
    /* background-color: whitesmoke; */
    border-bottom: 10px solid green;
}
h2 {
    font-family: Georgia, 'Times New Roman', Times, serif;
    font-size: 2em;
    font-weight: normal;
    font-style: italic;
}
p {
    font-size: 1em;
    line-height: 1.6;
    text-align: justify;
}
```

## Dockerfile

Every docker file starts with the source image or base image.  Next consider adding a couple labels for the image name and the author.  Labels are simple key/value pairs.

```
FROM nginx:latest
LABEL name="mycontainer1" version=v0.1
LABEL author="angus"
```

Next we will add directives to the dockerfile to copy in our index.html file.  Then crate the images and css folders.  After which we can copy in the style.css and all the images to those directories.

```
COPY index.html /usr/share/nginx/html
RUN mkdir /usr/share/nginx/html/images
COPY images/*png /usr/share/nginx/html/images/
COPY  style.css /usr/share/nginx/html
```

Now we can build the image.

```
docker build --pull --rm -f Dockerfile -t customweb:latest .
```


