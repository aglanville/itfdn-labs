# Kubernetes and minikube

>Objective: Install minikube locally to provide a test environment for simple Kubernetes testing.

In this lab we are going to install another embedded hypervisor, virtualbox.  Other hypervisors are supported but Virtualbox is the best supported platform for this environment.    We will have to load three components locally.  Once installed we will start minikube from the command line and it will launch the Virtualbox instance of minikube.  You should not try to start, stop or delete minikube from within Virtualbox.

* First we need to install Virtualbox, find the install notes for your platform, Windows or Mac here: [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
*  Next we are going to install the kubectl command on our local machine, follow the install guide for your platform, Windows or Mac here: [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* Finally install Minikube on to your local platform, Windows or Mac here: [Minikube](https://github.com/kubernetes/minikube/releases)

Once this is complete you can start, stop and delete your Minikube instance from the command line.

>Note Windows users: The file extensions are often hidden but required at the command line so you may need to use **minikube.exe start** for example.  Also you may need to start the windows cmd prompt with administrative rights.

```
# minikube start
# minikube stop
# minikube delete
```

The minikube install may take 10 minutes or so to get going. While you wait start playing with this online environment: [Learn Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics)

## Once minikube is up.

You should see an all clear message like the one shown below.

![minikube](minikube-running.jpg)

Now we are going to check on your minikube local IP.  We will also switch the context for __kubectl__ to point to minikube running in your Virtualbox instance. Finally we should be able to launch the minikube dashboard.  

```
# minikube ip
# kubectl config use-context minikube
# kubectl get pods --all-namespaces
# kubectl get svc
# kubectl get nodes
# minikube dashboard
```

![kube dashboard](kube-dashboard.jpg)

Now close the dashboard session using "ctrl+c" on your keyboard. 
Next we are going to deploy "hello world" for minikube and then relaunch the dashboard.

```
# kubectl run hello-minikube --image=k8s.gcr.io/echoserver:1.10 --port=8080
# minikube dashboard
```

Kill the dashboard again "ctrl+c"

Now let's grab some kubernetes cluster details.

```
# kubectl cluster-info
# kuberctl cluster-info dump
# kubectl get 
# kubectl get endpoints
# kubectl describe service/kubernetes 
```

Once you are done playing with kubernetes in your minikube environment stop the service.

```
# minikube stop
```

## lab complete.