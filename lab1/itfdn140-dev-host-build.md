# In Class lab: Setup your local/development servers

>Objective: Install VMWare on your desktop and install CentOS 7 to be used for in class labs

The objective here is to install your development instance of CentOS 7.  This is the system you will be using for all the labs in class throughout the quarter.  Another host will be made available for your homework assignments.  You should have already downloaded the ISO to your desktop but if that is not the case grab a copy of it now here:

[CentOS7 ISO](https://www.ulcert.uw.edu/iso/CentOS-7-x86_64-Minimal-1804.iso)

>Note: CentOS8 is available now and we may work with it later in the quarter; however, CentOS7 is still a common platform

It is expected that you are using either [VMware Player](https://my.vmware.com/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/12_0) or [VMware Fusion](https://my.vmware.com/web/vmware/info/slug/desktop_end_user_computing/vmware_fusion/8_0) however it is possible to use other desktop based virtualization solutions such as [Virtualbox](https://www.virtualbox.org/wiki/Downloads). For now I would like to use VMWare but I will post notes on using VirtualBox later for those that are interested.

>Note: When using either VMware player or Fusion you will be given the option for a quick install, avoid this option as we want to review the menu options.

## Confirming that your computer will support Virtualization.

> Note: This is for the Windows users, if you are working with MAC system then you can skip this section.

Most Windows 10 systems will have the virtualization support enabled by default.  However, some systems may not have this enabled and it will require an update in the BIOS to resolve this issue.

To see if your Windows system has Virtualization enabled open up task manager (Ctrl + Shift + Esc) click on the performance tab, select "CPU" and then look in the main window pane to see if next to Virtualization it shows **enbled**.  If you see **disabld** next to Virtualization then you will have to reboot and enable virtualization.  Each vendor may have a slightly different method to enter the BIOS and to enable this feature.

![Virtualization enabled](task-perf.png)

Here are a few sites to review for tips on how to enable virtualization. Please ask if you have questions, we can work on this update together.

- [https://2nwiki.2n.cz/pages/viewpage.action?pageId=75202968](https://2nwiki.2n.cz/pages/viewpage.action?pageId=7520296)
- [https://www.bleepingcomputer.com/tutorials/how-to-enable-cpu-virtualization-in-your-computer-bios/](https://www.bleepingcomputer.com/tutorials/how-to-enable-cpu-virtualization-in-your-computer-bios/)
- [https://smallbusiness.chron.com/enable-virtual-hardware-bios-68364.html](https://smallbusiness.chron.com/enable-virtual-hardware-bios-68364.html) 

## Configure your desktop virtualization client

Before we can begin to walk through the Linux installation we must configure our desktop virtualization client (VMWare Player or Fusion).  These are the steps to define the hardware specification for our installation. This includes options like how much disk space to allocate, how much RAM to allocate, the name of our image and so on.

### VMware Fusion notes (v10)

1. Launch Fusion
2. File -> new.
3. Select "Install from disc or image" and drag the image to the install window.
4. On the "Create a New Virtual Machine" select your image and click "Continue
5. Accept the default boot firmware "Legacy BIOS" and click "Continue"
6. On "Finish" menu click "Customize Settings".  Give the settings file a name like "itfdn-dev01" and save it.
7. The Settings window should open next, here update the system memory, and hard disk size.  Use the "Show All" to switch between settings.
    * memory = 512mb
    * hard disk = 12GB

1. Close the settings menu when complete and start your virtual machine.

### VMWare Player notes (v12)

1. Launch VMware player.
2. Click Player -> File -> New Virtual Machine
3. On the "Welcome to the New Virtual Machine Wizard select "I will install the operating system later" and click next.
4. On the "Select a Guest Operating System" screen select "Linux" and "CentOS 64-bit"
5. Name the Virtual Machine, enter something like "itfdn-dev01" and click "next"
6. Specify Disk Capacity change Maximum disk size to 12 GB
7. Ready to Create Virtual Machine, click "Customize Hardware"
8. Select Edit the Virtual Machine settings and update the hardware values.
    * Select Memory on the right side set it to 512Mb.  
    * Select New CD/DVD  and on the right side select "Connect at power on"
        * Use ISO image file and then browse to your ISO image and click "Open".
    * Click "close"
    * Click "Finish" and "Play virtual machine".  

### Start the install

You should now be looking at something similar to this screen shot.

![Inital CentOS 7 install](centos7_initial_install.GIF)

This will likely default to **Test this media and install CentOS 7"**.  This step should be unnecessary. Navigate up one row and select **"Install CentOS 7"** and press enter.  If you delay too long after approximately 60 seconds the system will auto launch the test and install option.  If happens you may be able to press the ESC key to skip the media test.

**Next** the system should start to load the media and then a **"Welcome to CENTOS 7"** screen should display.  Here you should select your preferred language and then click "Continue".

![centos_language](centos7_install_select_lang.jpg)

The next screen should be the "INSTALLATION SUMMARY".  Here we want to review all the options starting with **Date & Time**.

![install summary](install_summary.jpg)

When the Date & Time screen loads set the region to the Americas and set the City to "Vancouver" and click "Done".

![Date and Time](centos7_install_set_date.png)

**Next** select Installation Destination from the **Install Summary** menu and click the toggle for "I will configure partitioning" and click "Done".

![Configure partitioning](centos7-install-select-disk.PNG)

Here we want to define our partitions, click the "+" sign and create a new mount point.

![add mount point](centos7-install-add-mount-points.PNG)

Add the following mount points:

* swap = 512MB
* /boot = 500mb
* /var = 3GB
* / = remaining disk space...

> Note: Don't be concerned if you see available space remaining it should be very little, around, 992.5 KiB.

Now review your updates and click "Done" when ready.

![Mount point review](centos7-install-all-mounts-added.PNG)

**Next** click "Accept Changes" and you should return to the INSTALLATION SUMMARY menu.

![Accept Changes](centos7-install-disk-accept-changes.PNG)

**Next** select "Network and hostname** you may need to scroll down in the summary page to see this option. Here you want to:

* Select network and hostname
* Set your hostname **itfdn-dev01.local**  
* Enable your network interface (slide toggle to "on")

Finally click "Done" when ready.

![hostname and IP address](centos7_install_create_hostname.png)

Finally review your settings and then click "Begin Installation".
As the installation begins you should see the Configuration "USER SETTINGS" screen next.  Here you will set your **ROOT** password and create your own local **USER** account.  Be sure to give your local account administrator rights.

Set your **root** password to **root4me2**.  There are often problems with lost usernames and passwords on the these local linux installs and this will help us to sort them out later if we all know the default root password.

![Create user](centos7_install_create_user-1.png)

![set ROOT password](centos7_install_create_user2v2.jpg)

Once the process is complete you should see an option to "Reboot" your new development host.  Once the system restarts you should be able to login using either the root account or your own local account using the vmware console.

### After the Reboot

Now you need to find the IP Address for your development CentOS 7 host.

```shell
ip addr show
```

![IP address](fusion-display-ip-addr-v2.jpg)

**Next** confirm your sudo rights by executing the following commands.  Consider the command examples provided and discuss with others in the class what they are doing.

```shell
sudo -l
sudo su root
who am i
exit
who am i
```

**Next** connect to your new development host using SSH, this means you are not using the VMWare console to interact with your Development linux host but using another tool.  Students using a MAC or Linux have an SSH client built available by default from the terminal application.  Windows users will have to download the [Putty SSH client](https://the.earth.li/~sgtatham/putty/latest/w32/putty.exe) and connect to their Development Linux host

The following is what it will look like on a Mac or Linux host.

![Root login](ssh_root_login.png)

Here is an example of the Putty client initial menu.  Here you would need to enter the IP address which in this example would be 172.16.15.247 but your guest IP address is probably something else.  Make sure the "Connection type" is set to SSH and click Open.

![Putty client](PuTTY.png)

**Next** run the same commands as previously run using the VMWare console.  What output difference do you see? it is minor so you may have to compare them side by side.  Discuss this with others in class and be prepared to talk about it.

**Last task:** Login using your local user account.  This is the user you defined during the setup process in addition to setting the root password as the system was installing.

![user creation](user-creation.jpg)

#### Stretch challenge, (requires editing a file and we have not covered this yet)

> Objective: Remove the ability to login as root via SSH

To make this change we need to update the SSH config file and set **PermitRootLogin** to no and restart the SSH service.

1. Backup sshd_config to sshd_config.orig

```shell
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
```

2. Edit the /etc/sshd/sshd_config locate the line **PermitRootLogin**, ensure it is not commented out and that the value is set to "no".

3. Restart the sshd service

```shell
sudo systemctl status sshd
sudo systemctl restart sshd
sudo systemctl status sshd
```

4. Confirm that you are not able to login as "root" via SSH to your development host.

___
Lab Complete
__
