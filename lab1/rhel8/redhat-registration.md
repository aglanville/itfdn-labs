# RedHat Registration

You will need to create Red Hat account and then associate it to the developer profile in order to license your development RHEL 8 host.

Start by going to [RedHat.com](https://www.redhat.com/en) and click **Log In**. On the next page click **Register now**.  I recommend you use your UW email address for this purpose but that is your choice.

Once the account is created navigate to the [RedHat developer](https://developers.redhat.com/) site and try to Log in again there.  This process should link your RedHat account to the developer account.  Once logged in check your user profile and find the subscriptions option.

![redhat subscriptions](images/rhel8-subscriptions.png)

Here you should be able to find details about your subscription.  You should have the option to license 16 RHEL instances.

![Red hat registered subscription](images/redhat-registered-subscription.png)

Here are a couple more related links that may be helpful.

- [https://developers.redhat.com/blog/2021/02/10/how-to-activate-your-no-cost-red-hat-enterprise-linux-subscription#step_1__make_sure_you_have_a_red_hat_account](https://developers.redhat.com/blog/2021/02/10/how-to-activate-your-no-cost-red-hat-enterprise-linux-subscription#step_1__make_sure_you_have_a_red_hat_account)

- [https://developers.redhat.com/articles/renew-your-red-hat-developer-program-subscription](https://developers.redhat.com/articles/renew-your-red-hat-developer-program-subscription)

- Check subscriptions:
  [https://access.redhat.com/management](https://access.redhat.com/management)

___

End of document

___