# Course: Network and System Administration
## In Class Lab: 1
## Name: Setup your local/development servers

>**Objective:** Install VMWare on your desktop and install RHEL 8 which will be used for in class labs

>**Assumptions:** You should have already downloaded the ISO to your desktop but if that is not the case grab a copy of it now here: [RHEL 8 ISO](https://www.ulcert.uw.edu/iso/rhel-8.5-x86_64-boot.iso)

The objective here is to install your development server instance of RHEL 8.  This is the system you will be using for almost all the labs in class throughout the quarter.  Another Linux server will be made available for your homework assignments later this week, this will be your production server. 

It is expected that you are using either [VMware Player](https://customerconnect.vmware.com/en/downloads/info/slug/desktop_end_user_computing/vmware_workstation_player/16_0) or [VMware Fusion](https://customerconnect.vmware.com/web/vmware/evalcenter?p=fusion-player-personal) if your hardware has an Intel/AMD CPU, however it is possible to use other desktop based virtualization solutions such as [Virtualbox](https://www.virtualbox.org/wiki/Downloads). For now I would like to use VMWare but I will post notes on using VirtualBox later for those that are interested.

For Macs with Apple Silicon CPUs M series chips please download [UTM](https://mac.getutm.app/).  You can purchase a license for UTM using the Mac App Store link or download the free version directly from the UTM site.

Once you have downloaded either VMware player, VMware fusion or UTM run through the install steps and accept the defaults unless you have a custom option you prefer.

## **Windows users:** How to confirm your computer will support Virtualization (VMware)

>Note: This is for the Windows users, if you are working with MAC system **skip** this section.

>**WARNING**: Many users have reported issues trying to use **Windows 10 or 11 HOME** with HyperV and other Virtualization solutions like VMWare player.  It is recommend that you use Windows 10 or 11 Professional edition.

Most Windows 10 systems will have the virtualization support enabled by default.  However, some systems may not have this enabled and it will require an update in the BIOS to resolve this issue.

To see if your Windows system has Virtualization enabled open up task manager (Ctrl + Shift + Esc) click on the performance tab, select "CPU" and then look in the main window pane to see if next to Virtualization it shows **enbled**.  If you see **disableld** next to Virtualization then you will have to reboot and enable virtualization.  Each vendor may have a slightly different method to enter the BIOS and to enable this feature.

![Virtualization enabled](../task-perf.png)

Here are a few sites to review for tips on how to enable virtualization. Please ask if you have questions, we can work on this update together.

- HP systems: [https://support.hp.com/us-en/document/c03824350](https://support.hp.com/us-en/document/c03824350)
- Dell systems: [https://www.dell.com/support/kbdoc/en-us/000195978/how-to-enable-or-disable-hardware-virtualization-on-dell-systems](https://www.dell.com/support/kbdoc/en-us/000195978/how-to-enable-or-disable-hardware-virtualization-on-dell-systems)
- More generalized tips [https://www.bleepingcomputer.com/tutorials/how-to-enable-cpu-virtualization-in-your-computer-bios/](https://www.bleepingcomputer.com/tutorials/how-to-enable-cpu-virtualization-in-your-computer-bios/)
-  More generalized tips [https://smallbusiness.chron.com/enable-virtual-hardware-bios-68364.html](https://smallbusiness.chron.com/enable-virtual-hardware-bios-68364.html) 

## Configure your desktop virtualization client

Before we can begin to walk through the Linux installation we must configure our desktop virtualization client (VMWare Player or Fusion).  These are the steps to define the hardware specification for our installation. This includes options like how much disk space to allocate, how much RAM to allocate, the name of our image and so on.

>Note: When using either VMware player or Fusion you will be given the option for a quick install, avoid this option as we want to review the menu options. For users with Apple Silicon Macs please follow the UTM instructions.

### UTM for Apple Silicon Macs
1. Launch UTM
2. Click -> Create a New Virtual Machine
3. Select **Emulate**
4. Select **Linux**
5. Browse to the location of your RHEL 8 ISO, and click **Open**, then **Continue**.
6. On the Hardware window, adjust memory and CPU Cores, then click **Continue**.
    * memory = 2048
    * CPU Cores = 2
7. On the Storage window, adjust drive size = 16GB, then click Continue.
8. We won't use a Shared Directory, so just click Continue.
9. On the Summary page, name your VM something like **itfdn-rhel8-dev01** and click Save.

Now start your new virtual machine.  The initial startup is fairly slow but give it time.  Expect the boot to take up to 2 mins.

Once you see content in the console continue on to the **Start the Install** section further below.


![UTM Summary](images/utm-step7.png)

>**Note**: You may have to clear or empty the CD/DVD setting to avoid the system restarting the install process on reboot.  See this [IMAGE](images/utm-dvd-clear.png) for more details

___
___

### VMware Fusion notes

1. Launch Fusion
2. File -> new.
3. Select **Install from disc or image** and drag the image to the install window.
4. On the **Create a New Virtual Machine** select your image and click **Continue**
5. On **Choose Operating System** select **Linux and Red Hat Enterprise Linux 8 64-bit**.
6. Accept the default boot firmware **Legacy BIOS** and click **Continue**
7. On **Finish**x menu click **Customize Settings**.  Give the settings file a name like **itfdn-rhel8-dev01** and click **Save**.
8. The Settings window should open next, here update the system memory, and hard disk size
    * memory = 2048mb
    * hard disk = 16GB

    ![Fusion settings](images/RHEL-fusion-settings.png)
    * use the **Show All** button to switch between settings
  
     ![fusion show all](fusion-show-all.png)
9. Close the settings menu when complete and start your virtual machine.

Once you see content in the console continue on to the **Start the Install** section further below.

___
___

### VMWare Player notes

>**Windows users**: Do not double click on the ISO image as Windows will mount the file as a disk.  Just click once to select it.  Here is what a mounted image file might look like in windows: [mounted](../rhel8/images/RHEL8-windows-mounted.png) and here is what the image looks like if just selected: [image file](images/RHEL8-windows-selected.png).

>**Windows users**: If you are using Microsoft One Drive do not save your virtual machine to the cloud. When naming your virtual machine below look for an option to save your virtual machine image locally on your computer.  Saving the image to the Microsoft One Drive cloud may make your server run very slowly.

![one drive](../rhel8/images/one-drive.png)

1. Launch VMware player.
2. Click Player -> File -> New Virtual Machine
3. On the "Welcome to the New Virtual Machine Wizard select "I will install the operating system later" and click next.
4. On the "Select a Guest Operating System" screen select "Linux" and "RHEL 8 64-bit"
5. Name the Virtual Machine, enter something like "itfdn-rhel8-dev01" and click "next"
6. Specify Disk Capacity change Maximum disk size to 16 GB
7. Ready to Create Virtual Machine, click "Customize Hardware"
8. Select Edit the Virtual Machine settings and update the hardware values.
    * Select Memory on the right side set it to 2048Mb.  
    * Select New CD/DVD  and on the right side select "Connect at power on"
        * Use ISO image file and then browse to your ISO image and click "Open".
    * Click "close"
    * Click "Finish" and "Play virtual machine".  

### Start the install

You should now be looking at something similar to this screen shot.

![Initial RHEL 8 install](../rhel8/images/RHEL-start-install.png)

This will likely default to **Test this media and install Red Hat Enterprise Linux 8.5"**.  This step should be unnecessary. Using your mouse point to "Install Red Hat.." and click once.  This should change your context to the VMWare console.  Now you can use the arrow keys to navigate up one row and select **Install RHEL 8...** and press enter.  If you delay too long (approximately 60 seconds) the system will auto launch the test and install option.  If this happens you can just be patient while it checks the media or you can close VMWare and restart the process over.

>__Note:__ To force VMWare to release your mouse for mac users press **Control and Command** for Windows users press **Control and Windows**.

**Next** the system should start to load the media and then a **Welcome to RED HAT ENTERPRISE LINUX 8.5** screen should display.  Here you should select your preferred language and then click **Continue**.

![RHEL_language](../rhel8/images/rhel8-install-language.png)

The next screen should be the "INSTALLATION SUMMARY". Review the choices, we will update at least 7 of these options.

>__Note:__ You must have your Red Hat account linked to your Developer license at this point.  If this is not the case or if you are having issues see [Red Hat Account Creation](https://www.ulcert.uw.edu/itfdn-labs/lab1/rhel8/redhat-registration.html)

![RHEL install summary](../rhel8/images/rhel8-install-steps.png)

1. We are going to start with *Network & Host**.  Here we want to set the host name and enable your network interface, this is how your Linux server will connect to the internet.  Set the hostname to something like **itfdn-dev01.localdomain** and click **Apply**.  Next enable the Ethernet interface by sliding the toggle to **ON** and click **Done**.

![Network and Host](images/rhel8-install-network.png)

2. Next we want to select **Connect to Red Hat**.  Here you want to enter your Red Hat username and your password.  This might be your email or it may be your Red Hat username which may not be the same thing.  In my example my user name is **uw-angus**.  You can confirm this by checking the profile of your Red Hat account.

![Red Hat profile](images/redhat-profile.png)

>**Note**: If you have not created a RedHat account and linked it to your developer account do it now see [Red Hat Account Creation](https://www.ulcert.uw.edu/itfdn-labs/lab1/rhel8/redhat-registration.html) for help.

![RHEL registration](./images/rhel8-install-reg-working.png)

Once things complete you should see something like the following screenshot.

![RHEL reg complete](images/rhel8-install-reg-success.png)

If your registration fails confirm your username and password.  Try to login to the developer site directly to confirm your password is working and verify your username using the login at [developes.redhat.com](https://developers.redhat.com/)

1. Next select "Date & Time".  Once the screen loads select your region and find a city that will reflect your time zone.  Toggle the Network Time slider to **ON**.  Finally "Done".

![Date and Time](../rhel8/images/rhel8-install-time.png)

4. Now select **Software Selection**.  We want to create a **Minimal Install** with **Headless Management** tools installed. You can install another instance later with the default packages and a desktop but for this course we do not need a desktop.

![RHEL software selection](images/rhel8-install-package-install.png)

5. Now we are going to define the **Installation Destination**.  In this first install we are just going to accept the defaults. Ensure "Storage Configuration" is set to **Automatic** and then click "Done".

![Configure partitioning](../rhel8/images/rhel8-install-disk.png)

6. Set the **Root Password**.  Because this dev system will not be directly connected to a public internet we don't need a strong password I recommend **Passw05d**.  If you loose or forget this password it can be challenging to reset.

>__Note:__ [How to reset your root password when you have direct](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/changing-and-resetting-the-root-password-from-the-command-line_configuring-basic-system-settings#resetting-the-forgotten-root-password-on-boot_changing-and-resetting-the-root-password-from-the-command-line)

![root password](images/rhel8-install-root-password.png)

1. Next we will complete your **User Creation**.  Here you will enter your full name and set your username.  It is a best practice use all lowercase for your username.  Usernames **are** case sensitive.  Set your password to something you won't forget, this system will **NOT** be exposed directly to the internet.  Finally make sure **Make this user an Administrator** is selected.

![user setup](../rhel8/images/rhel8-install-user-password.png)

8. Now we can begin the installation, click **Begin Installation**.  If you selected the minimal software install then your downloads should be around 500mb to complete.

![install starts](images/rhel8-install-500mb.png)

If you see an error related to the "Red hat Insights configuration" click "yes" to ignore and continue.  We are not enabling this feature at this time but if you want to check out this page: [https://access.redhat.com/solutions/2201251](https://access.redhat.com/solutions/2201251)

![ignore error](images/rhel8-install-insights-fail.png)

Once the install completes click "Reboot System" to restart and test your first log in.

![reboot](images/rhel8-install-reboot.png)

Once the system completes the reboot you should see a simple terminal style login prompt.  Here try to enter your username and password.  When you enter the password you will not see any characters or symbols, you will have to trust that it is working and then press enter.

![user login](images/rhel8-login-user.png)

Not providing and visual indication of your password as you enter it at the command line is a security best practice.  It avoids letting anyone near you guess how long your password might be.

### First log in

Now you need to find the IP Address for your development RHEL 8 host.

```shell
ip address show
```

![IP address](images/rhel8-login-ip.png)

**Next** confirm your sudo rights by executing the following commands.  Consider the command examples provided and discuss with others in the class what they are doing.

```shell
sudo -l
sudo su root
who am i
exit
who am i
```

**Next** connect to your new development host using SSH, this means you are not using the VMWare console to interact with your Development linux host but using another tool.  Students using a MAC or Linux have an SSH client built available by default from the terminal application.  Windows users will have to download the [Putty SSH client](https://the.earth.li/~sgtatham/putty/latest/w32/putty.exe) and connect to their Development Linux host

The following is what it will look like on a Mac or Linux host.

![Root login](images/rhel8-ssh-login.png)

Here is an example of the Putty client initial menu.  Here you would need to enter the IP address which in this example would be 172.16.15.247 but your guest IP address is probably something else.  Make sure the "Connection type" is set to SSH and click Open.

![Putty client](../PuTTY.png)

**Next** run the same commands as previously run using the VMWare console.  What output difference do you see? it is minor so you may have to compare them side by side.  Discuss this with others in class and be prepared to talk about it.

#### Stretch challenge, (requires editing a file and we have not covered this yet)

> Objective: Remove the ability to login as root via SSH

To make this change we need to update the SSH config file and set **PermitRootLogin** to no and restart the SSH service.

1. Backup sshd_config to sshd_config.orig

```shell
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
```

2. Edit the /etc/sshd/sshd_config locate the line **PermitRootLogin**, ensure it is not commented out and that the value is set to "no".

3. Restart the sshd service

```shell
sudo systemctl status sshd
sudo systemctl restart sshd
sudo systemctl status sshd
```

4. Confirm that you are not able to login as "root" via SSH to your development host.

## Troubleshooting

Q. My VMWare console seems to lock up randomly and it will not take any input from me.

A. Make sure your Red Hat ISO file is not mounted.  To check this open Windows File explorer and check the left navigation for a new drive which shows the contents of the **rhel-8.5-x86_64-boot.iso** file.  If this is the case right click on the drive and click on **eject**.

![eject ISO](../rhel8/images/eject-iso.png)

Q. I am having problems with pulling down the repository details when installing RHEL.  The System seems to time out a lot also.

A. You may have issues with a local firewall, VPN or AntiVirus software solution.  See if you can disable this services and then restart your Development system install or continue to the steps.

Q. When I select my ISO file in VMWare it seems to spin endlessly and never move to the next screen.

A. Your image file may be corrupt, delete the ISO and download it again.

Q. When I enter my username and password it keeps says **Login incorrect**

A. Double check your username make sure it is not misspelled, Try it with the first letter capitalized if this still fails.  Try to login using the root account and the password you set for that account which if you followed the guide should be **Passw05d**.  If this still fails then you can either start over or try to reset your root password following this [Red Hat documentation guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/changing-and-resetting-the-root-password-from-the-command-line_configuring-basic-system-settings#resetting-the-forgotten-root-password-on-boot_changing-and-resetting-the-root-password-from-the-command-line).  The steps are challenging at this early stage so it may be better to start over.

Q. When I try to login using SSH via Putty or my terminal it seems to take 60 seconds or more to complete the login.

A. You may need to disable GSSAPI authentication in your putty client.  This is found under Connection -> SSH --> Auth.

![putty screen shot](../rhel8/images/putty-gssapi.png)
___
Lab complete
___


