# In class lab: SSH certificate authentication

> Objective: Enable ssh certificate based authentication on your development host

>Note: It is assumed that you are doing this lab on your development host and __NOT__ your production server.

The first objective is to login to your Development server from your host system.  Many of us have windows laptops, the rest have MAC or linux laptops.  In both cases the goal is to connect using ssh keys.  The Windows users will use putty and the MAC or Linux users will use a terminal. Once we have this configured correctly it will be possible to authenticate without a password. 

> Note: Windows users can take advantage of the [Linux subsystem](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to copy the mac/linux users experience.

> Prerequisite: You must have a local account on your development host

In this exercise it is hoped that you already have a local account defined on your development server.  However, if you are still logging in as root now is the time to stop and create a local account.   So, if you do NOT already have a local account and are logging in as root on your development host create a new local user account. 

```shell
useradd -c "full name" <username>
usermod -G wheel <username>
password <username>
```

Now if you have just literally created a user called `username` go back and create one with using your own preferred username, something like the username you created for the uw or your email address for example.

Remember if you really feel the need to become root you can do that via sudo using su. 
Note your effective and real user differs when you do this.

```shell
sudo su - 
whoami
who am i
```

When done logout by typing "exit" and pressing enter to logout.

## Setting up Certificate based authentication

The process for setting up your SSH keys is different for MAC or Linux users and Windows users.  Find the section below for the platform you are working with.  Once you have it setup move on to the second part of this lab.

## Windows users - SSH key setup

>Note you can also review the lecture slides for more tips on this process

In this example we are trying to work with **putty, ssh-keygen**, and **pagent**.  This means you should have the putty suite of tools installed.  If you do not have them installed do that now.  If you don't want to install the suite you can just download the required binaries. 

[Putty suite downloads](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)  

Now launch the Putty Key Generator (PuTTYgen)

![putty launch](puttygen_launch.png)

Now generate a new key pair by clicking the button labeled **Generate**.

> Note you will have to move your mouse around in order to speed up the key generation process as it helps to generate random data points. 

Read the tips provided by PuTTy Key Generator for additional details.

![Putty generator](putty-gen.png)

Next you need to complete 4 tasks.

1. Set a password for your private key, don't forget this password, there is no reset function.
2. Save your public key, give it a descriptive name like myPuttyPublicKey
3. Save your private key, give it a descriptive name like myPuttyPrivateKey
4. Now copy the public SSH key version and paste it onto your development host under /home/{username}/.ssh/authorized_keys.  The .ssh directory and the file authorized_keys do not exist by default.  

Look at number 4 in the screenshot for your windows host.  Notice you are NOT using the myPuttyPublicKey for this but rather the output provided in the key generator tool at the top of the screen.  Be careful not to miss any characters, such as the first s in ssh-rsa.  

![Save keys](save_keys_copy_public_key.png)

Now that we have our keys, we want to load them into the putty Agent (pageant) so putty can authenticate.

![Start Pagent](start_putty_agent.png)

Once running you should find it in your system tray.  Right click and select "Add Key", install your Private key, it will request your passphrase, hopefully you still remember it.  If you have forgotten, lost or somehow corrupted your private key then you will have to generate a new key pair.

![putty add key](putty_agent_add_key.png)

Now try to login using putty to your development server.  You should simply login without entering a password at this point.  The login window should look something like this, note it shows "Authenticating with public key".

![ssh key auth](sucess_logged_in_no_password.png)

If you must enter a password to authenticate then something is wrong.  Check the permissions on your .ssh directory, check the permissions on your authorized_keys file, check the contents of the authorized_keys file.  Check the permissions on your home directory.

* .ssh permissions set to 700
* authorized_keys file set to 644
* home directory set to 755

Once it is working step back and think about why you have to use the pubic key you copied from the putty key gen buffer and not your putty public key file.  Compare the two versions of the public key using notepad or something similar.

## Windows Troubleshooting tips

>Note: If things are not working double check these items

* Is your authorized_keys file name mis-spelled?
* Are missing the first s in ssh-rsa inside the authorized keys file?  The first letter is often dropped if you try to paste the public key value and Vi is NOT in INSERT mode.
* Double check the permissions for .ssh is it set to 700? ( chmod 700 ~/.ssh)
* Double check the permissions for your authorized_keys file is it set to 644? ( chmod 644 ~/.ssh/authorized_keys)
* Make sure the key loaded in your putty Agent matches the public key in your authorized_keys file.
* does your user have a password set?  The user does not need to know the password but there should be a password value set.

## MAC and Linux users - SSH key setup

In this section we are creating and setting up the ssh keys for a Mac or Linux user.  If you already have a key pair created you can use that key pair.  Check your home directory for existing keys, on your mac open a terminal and confirm you are in your home directory.  Then look for a folder called .ssh and within this files that start with id_ and and a matching file that ends in .pub.

![mac default ssh keys](mac-default-keys.png)

It is also possible that you have no keys setup.  In this case we can create a new key pair.  If we accept the defaults we will create two files under /Users/{your username}/.ssh One called id_rsa and the other will be id_rsa_.pub

```shell
/Users/<username>/.ssh/id_rsa
```

1. Open a terminal window  
1. run ssh-keygen and follow the prompts 
![ssh keygen linux](ssh-keygen.png) 
1. run "ssh-add" enter the passphrase for your private key when prompted.  
1. If you are unable to add your key due to the agent not running then you may need to start the agent. Try "ssh-agent" and then "ssh-add" again. If this still fails try "ssh-agent bash" and then "ssh-add"
1. Finally run "ssh-add -l" to see the keys your agent has loaded.

If you did not use the default names for your keys then you will have to provide the name in the path.  

```shell
ssh-add bash /home/{username}/.ssh/{private_key_file}
```

In this example my non default key file was called **id_itfdn140**

```shell
ssh-add bash /home/{username}/.ssh/id_itfdn140
```

1. Paste the contents of the id_rsa.pub file on your local system into your authorized_keys file on your development host.  

The authorized_keys file is not there by default this is something you will have to create.

 This file can contain multiple public keys. Confirm the permissions are correct for the file and the directory. Now try to login using putty to your development server.  If you have to enter a password then something is wrong.  Check the permissions on your .ssh directory, check the permissions on your authorized_keys file, check the contents of the authorized_keys file.  Check the permissions on your home directory.

* .ssh permissions set to 700
* authorized_keys file set to 644
* home directory set to 755 

Connecting to your prod server using the ssh-keys just generated.
Now copy your public key from your local host (Mac, Windows or Linux) to your prod server under ~$HOME/.ssh/authorized_keys.  Remember the file authorized_keys may not exist by default, you may need to create the file.  Make sure the permissions for this file the .ssh directory are correct. 

Now try to connect to your prod server from the same host you connected to your development host from.  If, you are able to connect to your development host from your windows, MAC or linux desktop this should be no problem.  If this fails, review the permissions on your production server.  Review the steps for your local instance.

## Mac/Linux user Troubleshooting tips

>Note: If things are not working double check these items

* Is your authorized_keys file name mis-spelled?
* Are missing the first s in ssh-rsa inside the authorized keys file?  The first letter is often dropped if you try to paste the public key value and Vi is NOT in INSERT mode.
* Double check the permissions for .ssh is it set to 700? ( chmod 700 ~/.ssh)
* Double check the permissions for your authorized_keys file is it set to 644? ( chmod 644 ~/.ssh/authorized_keys)
* does your user have a password set?  The user does not need to know the password but there should be a password value set.

### Stretch task

Try to connect from your development host to your production host.  Do this create a new key pair on your development host using `ssh-keygen`.  Simply run the command and accept the defaults.  This should create the .ssh folder on your development server.  In this folder find the id_rsa.pub file.  Copy the contents of this file to your production server and append them to your __authorized_keys__ file. Next start the ssh-agent on your linux server using `eval $(ssh-agent)`.  Once it is running load your new private key using `ssh-add`.  Finally confirm all is working by connecting from your development server to your production server using your ssh key.

The following is a set of command examples:

```shell
ssh-keygen
cd ~/.ssh
cat id_rsa.pub
eval $(ssh-agent)
ssh-add
ssh-add -l
ssh <user>@<prod server>
```

Once you have this working try running a command on your prod server from your __development__ host using ssh. 

1. login to your development host.
2. run the ssh command shown below where ulc-### is replaced with your real server name.

```bash
  ssh ulc-###.ulcert.uw.edu uptime
```

![dev to prod](dev-2-prod.png)

Can you redirect the output from uptime to a file?  There is an example of how to do this in the screenshot above.  What other commands can you run remotely on your production server from your development server?

___
Lab Complete
___
















