# In Class lab: Docker

>__Objective:__ Install and work with docker.

>__Note:__ It is assumed that you are doing this lab on your **development** host and not your production server.

## Install docker

Most of these steps come from the official [Docker install](https://docs.docker.com/engine/install/centos/) notes. First we want to make sure we have yum-utils installed and then grab the official docker repository configuration for REHL.

```bash
sudo dnf install -y yum-utils
ls -ls /etc/yum.repos.d/ | grep docker
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
ls -ls /etc/yum.repos.d/ | grep docker
```

Now we are going to install the Docker client and [containerd](https://containerd.io/) as the container runtime environment.  

>__Note:__ --allowerasing removes [podman](https://podman.io/) which is the new default container tool for RedHat.  Podman supports docker syntax in an effort to simplify the migration from docker to podman.

```bash
sudo dnf install docker-ce docker-ce-cli containerd.io -y --allowerasing
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker ${USER}
```

>__Note:__ Adding your user to the docker group removes the need for sudo rights to work with docker.  

>__Question:__ What sort of permissions do you see assigned to the file "/var/run/docker.sock"? standard, set uid, set gid or sticky bit.

Now to ensure that we have picked up our new group membership we are going to logout, log back into our dev server and then run the docker hello-world application.

```bash
logout
ssh {your development host}
docker run hello-world
```

Running `hello-world`  should pull down a new image file for the hello-world container. This will look something like the output shown in the following screenshot.  

>__Note:__ Make sure you have bash-completion rpm package installed as the docker-ce-cli includes support for docker.  Use `rpm -lq docker-ce-cli  | grep completion` for more details.

![docker hello](docker-hello.png)

We expect the docker run command to force an image pull, we can review this image now.  

```bash
docker image ls
docker image inspect hello-world
```

Now display details about your docker install.  Docker also by default installs a bridge network interface that will be used by the subsequent containers to connect to your host network.  Each container we start will get a unique IP address on the new docker network that is attached to this bridge. 

>__Question:__ Looking at the output from `ip addr show docker0` What network will your docker container use?

```bash
docker info
ip addr show 
ip addr show docker0
```

## Starting our first docker container

Now we are going to pull down a docker nginx container explicitly using the `docker pull` command. As you might have noticed we just used `docker run hello-world` earlier and now you may have figured out that run will pull and run the image in one command. After pulling the image we will check our available images and see that we now have both hello-world and nginx.

```bash
docker pull nginx
docker image ls
```

Next we are going to create a new container based on the nginx image we just downloaded and push this process to the background.  This concept of managing jobs is something we covered in [Lab 18](https://www.ulcert.uw.edu/itfdn-labs/lab18/lab-processes-systemd.html) if you want a refresher on the concepts.  Once we have the docker container process running in the background we can try to test our new nginx webserver.  When running a docker nginx container we provide the port for docker to listen to internally and the port to expose externally and that we will use to connect to the container service.   In this example we are starting listeners on port **8081**. Remember that when we use the ampersand sign "&" we are pushing the command into the background. Using the foreground command "fg" brings that process back to the focus of your shell. When running `docker ps` note the value under the NAME field, this is a randomly generated name if we don't provide one.

```bash
docker run -p 8081:80 nginx & [press the <RETURN> key here]
curl http://localhost:8081
docker ps 
docker ps | awk '{print $12}'
```

Now open a second ssh session to your development host and place it next to your current shell.  The goal is going to be monitoring the traffic send to the docker0 bridge when testing our docker container using `bmon` which was installed in [lab 20](https://www.ulcert.uw.edu/itfdn-labs/lab20/environments-and-crontab.html).  

```bash
ssh {your development host}
bmon -p docker0
```

Now return to your existing ssh session where we will make 9 http requests to your docker nginx container.  We should see each of these requests in the bmon ssh session which should be open also at this time.  This shell one liner is using a `for` loop this is a concept we will cover in more detail in the unit.  But the logic based on `for object in values X-Z ; do something ; end`.

```bash
for f in {1..9}; do clear; echo "making call $f now.."; sleep 1;  echo; curl http://localhost:8081; sleep 1;clear; done
```

Now we can bring our nginx process back to the foreground with **fg** and shut it down with a **ctrl + c**.  But stopping the container does not remove the container.  Remember the container is based on the image.  The container creates a writeable layer on top of the image and this is object can now be removed.  In some scenarios you might keep the container and restart it later at which point you would find any of the files created when it was running previously.

Using the foreground command "fg" brings that process back to the focus of your shell. When running `docker ps` we can see the **running** docker processes but if we want to see all images those running and stopped we must use the **-a** option.   

When containers are started docker generates a random name, we can see that now under the NAME field.  This value can of course be set as an option when starting the docker image instance.  Also note that once the docker container is stopped you still have the container on disk.  We have to explicitly remove the container from our disk using `docker rm {container id}`.  Try to remove both the hello-world and nginx containers instances.

```bash
fg
ctrl + c
docker ps
docker ps -a
docker rm {CONTAINER ID}
docker ps -a
```

![docker rm container](docker-rm-container.png)

## Creating a named docker container and logging in directly.

Now we are going to create a couple more containers.  In the first example we are going to define a name for our container using the `--name` option.  Next we are going to create another container and let Docker randomly select the external ports to use. We do this with the upper case `-P`.  Using `docker ps` we can see the running docker images and the ports assigned to those instances.  

In the command examples below you will note in one line I have `{port for web2}` you should find the port assigned to **web2** in the output from the previous `docker ps` command and insert the assigned port value there.

```bash
docker run -d --name web1 -p 8082:80 nginx
curl http://localhost:8082
docker ps
docker run -d --name web2 -P nginx
docker ps
docker run -it nginx sh
hostname
pwd
cd /bin
ls
cat /etc/passwd
exit
docker ps 
curl -I http://localhost:{port for web2}
docker stop web1
docker stop web2
docker ps 
docker ps -a
```

![docker stop](docker-stop-containers.png)

You can remove a docker container using the docker name or container id. Each time you stop a docker container it will by default leave behind the stopped container.  If you plan to run the same container instance again it is fine to leave this on disk. However, if you no longer need the container instance you should remove it as they may over time consume disk space you need for other purposes.

## Using a variable with Docker commands

 In the following screenshot you can see I have 3 stopped containers.  First I remove one of them explicitly using the container id. Image that find 100 stopped images that you want to remove, doing it one by one could be an issue. In this situation you might take advantage of in-line command substitution.  You know you can use for example $USER to print your username.  You also know you can use this in an echo command like this `echo "hello $USER"`.  Now we are going to use a similar idea inline with the docker command. We want $() to contain a list of all the stopped docker containers.  We can get a list of all docker container ID values using `docker ps -q -q`.  So, now we are going to nest this command in-line with the `docker rm` command as shown in the example below.

>__Note:__ `docker rm` will not remove a running container, it must be stopped first.  You can stop the container first using `docker stop $(docker ps -a -q)`

```bash
docker run -d -P nginx
docker run -d -P nginx
docker run -d -P nginx
docker ps
docker ps -a
docker ps -a -q
docker stop <container id>
docker rm <container id>
docker ps -a -q
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker ps -a
```

The following is an example of this command in use with a different set of docker containers.

![docker rm](docker-rm.jpg)

If you want to remove the images after stopping the containers you will need the container id or you can use the docker in-line command substitution syntax again .  This might look like `docker image rm $(docker image ls -q -a)`

```bash
~$ docker image ls
~$ docker image rm <image id>
~$ docker image ls
```

## Importing local content into your Docker image

Next we are going to start another nginx instance but this time it will use our local dev host html directory for content.  This assumes you have nginx installed on your dev host and that have a unique index.html file on your dev host.  We are using -v to map the local directory `/usr/share/nginx/html` to the same location in the nginx docker container we start called mydevweb1

__Note:__ If you get an error message related to driver failed make sure firewalld is running. `sudo systemctl status firewalld`.

```bash
docker run --name mydevweb1 -v /usr/share/nginx/html:/usr/share/nginx/html:ro -p 8083:80 -d nginx
curl http://localhost:8083
```

Update your the index.html file on your local host and see if the changes are reflected in your docker container.  Once done stop and clean up this container.

```bash
docker stop mydevweb1
docker ps 
docker ps -a
docker rm mydevweb1
```

## Docker and logs

For this exercise we will need two ssh sessions to our development host.  First we are going to create an docker container instance that will be running nginx.  Then we will attach to that container to watch the requests in real time that we will generate from another ssh session on our development host.

>__Note:__ The second command will result in your first shell session appearing to hang as it is waiting to print out any log data which will be generated by your second SSH session and a shell loop.

See this link to watch a quick demo of these steps, including how to remove an older instance of the myweb84 container [docker logging demo](lab21-myweb84-demo.gif).  

>__Note:__ If you are unable to start myweb84 due to the name already being in use, remove the old version using `docker rm myweb84`.  Adding --rm to the example will force dokcer to remove the container when you kill it by default.

```shell
docker run -d --name myweb84 -p 8084:80 nginx
docker attach myweb84
```

Next establish a second ssh session to your development host and run the following shell command to send a series of http GET requests to your nginx docker container.

```shell
for f in {1..10}; do curl -I http://localhost:8084 ; echo "Loop $fdone "; sleep 1; done ;echo "Loop test done"
```

You should see log entries for each curl request. We are using a simple for loop here, this is something we will cover in more detail later in class.

Next we are going to check the logs on our nginx web server.

```shell
docker logs myweb84
```

Once done use Ctrl+c to end the nginx web server instance and do a little clean up, i.e. stop and remove the containers.

## Search for images

>**Note** [RockyLinux](https://rockylinux.org/) is an opensource version of Red Hat Enterprise Linux.

```bash
sudo docker search rockylinux
sudo docker search debian 
```

Docker registry links

 - https://aws.amazon.com/ecr/
 - https://cloud.google.com/container-registry/
 - https://hub.docker.com

____

## Lab complete

---

### Troubleshooting tips

>Note if you see an error when starting Docker review the logs for clues

```bash
Error starting daemon: SELinux is not supported with the overlay2 graph driver on this kernel. Either boot into a newer kernel or disable selinux in docker
```

If this is the case backup your /etc/sysconfig/docker file and then update it as follows putting a **#** symbol in front of the first OPTIONS and adding the DOCKER_OPTS line.

```bash 
#OPTIONS='--selinux-enabled --log-driver=journald --signature-verification=false' 
DOCKER_OPTS='--storage-driver=devicemapper'
```

If you get `permission denied` when trying to run docker commands confirm your group membership in the `docker` group.  You may need to logout and back in to resolve this issue.  If this still does not fix the problem try to restart the docker service to reset the permissions on the `docker.sock` file.

Here is a screenshot of what your experience might look like.

![docker socket permissions](docker-process-owner.jpg)

If you are unable to start your docker container due to an error about a failed driver make sure firewalld is running.

![failed container](../lab21/docker-container-failed-to-start.png)

___

End of Troubleshooting tips

___
