# In Class lab: Customizing your environment and working with Crontab

>Objective: Work with your shell environment, set variables, update your prompt and review crontab

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

## Environments

There are two groups of environment variables defined either for the shell, some of these are local or limited to your current environment and some of these can be inherited by sub-shell.  Using `set` we can see the shell variables that will not be inherited by sub shells.  Using `env` we can see the environment variables that can be inherited by a sub shell.  Some variables are defined in both environments.  Your shell environment values also may include a number of functions.  We will pipe the output of set to **more** to review the environment variables.

>**Note**: To page down in more just press the spacebar, to exit press q

```bash
env
set | more
env | grep -i hostname=
set | grep -i hostname=
echo $USER
echo $HOSTNAME
echo $HOME
```

set variables are contain by the shell

![set variables](enviro-set.png)

env variables are defined for the shell and may be inherited by subshells.

![env variables](enviro-env.png)

Now we are going to install a new tool called `pstree` which will help to show some of the process tree relationships.  We are also going to set a variable and then export it so it is available for sub shells and which are contained within the shell.

Notice when do you see the variable `mynewvar` returned in your shell, does it show when using set or env?  Are you able to find the value in the shell using echo?  If you spawn a new bash shell does the variable appear in your new shell?

>__Note:__ check the contents of the psmisc package after install using `rpm -lq psmisc`.

```bash
sudo yum install psmisc -y
man pstree
pstree
env | grep -i mynewvar
mynewvar="my demo variable"
echo $mynewvar
env | grep -i mynewvar
set | grep -i mynewvar
bash
env | grep -i mynewvar
set | grep -i mynewvar
exit
exit
```

Now we will sign-in again to our dev host (you should have just logged out).  We want to export the variable `mynewvar` and see if it shows up in our env and set variable output in the initial shell session or in child shell sessions.

The first check here we expect **not** to find the variable defined.  We will also use **pstree** and note our bash shell in the process tree.

```bash
env | grep -i mynewvar
set | grep -i mynewvar
pstree
```

Now we will spawn a new bash shell and set the environment variable. Also run **pstree** and see where your new bash shell in the process tree. 

```bash
bash
mynewvar="my demo variable"
env | grep -i mynewvar
set | grep -i mynewvar
pstree
```

This time we are going to spawn another shell and see if our variable shows up.

```bash
bash
env | grep -i mynewvar
set | grep -i mynewvar
```

This time we are going to export the variable, check if we can see it and then spawn a new bash shell and see if it still shows up.

```bash
export mynewvar="my demo variable"
env | grep -i mynewvar
set | grep -i mynewvar
```

Did you find the variable using both the shell and environment variables? Now we are going to spawn another bash shell and see if the variable still exist.

```bash
bash
env | grep -i mynewvar
set | grep -i mynewvar
```

Now run pstree again and see how many level deep your current bash shell is, from this you should be able to figure out how many times you need to enter exit to actually exit the shell.

![pstree sub bash](pstree-sub-bash.png)

### Working with your PS prompt

Now we are going to change the values displayed by your prompt.  If you are interested in more options check the man pages for bash and search for prompting `man bash`.  In this first example we are changing the prompt to show the full path for the working directory by changing `\W` to `\w`.  Next we will change the prompt to include the date by adding the `\d`.

```bash
echo $PS1
export PS1='[\u@\h \w]$'
cd /
cd /var/log/
cd /etc/yum.repos.d/
export PS1='[\u@\h \w \d]$'
```

Now we are going to work with changing the prompt colors to Cyan then Red and finally back to white.  Check the table below for more color options.

```bash
export PS1='\e[0;36m[\u@\h \w \d]$'
export PS1='\e[0;31m[\u@\h \w \d]$'
export PS1='\e[0;0m[\u@\h \W]\$'
```

#### Color codes

|Color     | [Code]    |
|--------- |-----------|
| Black    | 0;30      | 
| Blue     | 0;34      | 
| Green    | 0;32      | 
| Cyan     | 0;36      | 
| Red	     | 0;31      |
| Purple	 | 0;35      |
| Brown    | 0;33      |
| Blue	   | 0;34      |
| Green    | 0;32      |
| Cyan     | 0,36      |
| Red   	 | 0;31      | 
| Purple	 | 0;35      |
| Brown    | 0;33      |

### Working with .bashrc and .bash_profile

Add the following text to your .bashrc and .bash_profile files replacing the respective file name in each example.

1. Add the following to the bashrc or bash_profile files using just the name of the actual file in each  
  Found this in the **.bashrc**.
  Found this in the **.bash_profile**.
2. Spawn a new bash shell by simply entering `bash`.  Which message did you see?
3. Start a new SSH session to your dev host, which message did you see?

Review the files:

-  /etc/profile
-  /etc/bashrc 

Review the directories:
-  /etc/skel 
-  /etc/profile.d

Install **cowsay** and checkout the alternatives to the cow. See if you can add one of these to your .bashrc file so that is launches when you sign-in.

>__Note:__ You may need to enable another couple RHEL repositories if you have not enabled them before in order to install **cowsay**.

```bash
sudo subscription-manager repos --enable codeready-builder-for-rhel-8-$(arch)-rpms
sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo yum install cowsay
cowsay hello $USER
cowsay -l
cowsay -f bud-frogs "Hello $USER"
```

Now if you want to add this greeting to your login run the following command and then test it by spawning a new bash shell.  Finally check your .bashrc file to see what was added.

```bash
echo 'cowsay -f small "hello $USER"' >> ~/.bashrc
bash
cat ~/.bashrc
```

### Aliases

Take a look at your default aliases.  Running these commands will add a couple new aliases to your shell.  If you like them or want to add others to your default shell then add those lines to your .bashrc file. 

```bash
alias
ll
alias ll='ls -lart --color=auto'
ll
alias h='history'
h
alias gits='git status'
gits
alias du1="du -h -d 1 | sort -n"
bash
cd /var/log
du1
```

### Working with crontab

First we are going to install a simple command line email tool called **mailx**.  Then we are going to setup a root crontab to check the disk status every minute.   Finally, we will work with your crontab and have it check who is logged in every 5 minutes.

To start with we will also need to install an MTA  (Message Transport Agent) such as **sendmail** or **Postfix**.  In previous versions of RHEL this was installed by default.  Once installed we expect to find a listener on port 25 of the loopback (localhost) interface.  We will need to start sendmail and then restart crond to enable this feature.

>__Note:__ Using the option `--now` with systemctl enable will start the service at the same time it is enabled for autostart.

```bash
ss -tan | grep 25
sudo dnf install sendmail -y
sudo systemctl enable sendmail --now
sudo systemctl restart crond
ss -tan | grep 25
```

Now we can install the **mailx** client and review and copy the base crontab configuration file.

```bash
sudo yum install mailx -y
```

copy the crontab file output from the following command.

```bash
cat /etc/crontab
```

Now we are going to create a new crontab file for the root user and paste in the details we just copied from `/etd/crontab`. To do this we just use `sudo` with `crontab -e`.  When the blank crontab file opens paste in the template found in `/etc/crontab`.

Finally add the following lines to the bottom of the new crontab file we are creating, save and close the file.

```bash
* * * * * root ls -la
10 * * * * root free
10,40 * * * * root who
```

This crontab says as the root user run **ls -la** every minute or every hour or every day of every month of every day of the week.

Next it says run **free** at 10 minutes of every hour or every day of every month of every day of the week.

What do you think the 3rd line says?

![root crontab](crontab-root.png)

Now we want to create a crontab for YOUR user. First we need to open your crontab in edit mode.  This can be done using `crontab -e` if you do this with sudo you will open crontab for __root__ and __not__ your account.  Repeat the steps used to create the root crontab.  Meaning copy /etc/crontab first then run `crontab -e` and paste those details into your new crontab file.  However, update  `MAILTO=root` so that it shows your own username save your changes and exit.

Now check your crontab file.

```bash
crontab -l
```

Now open your crontab file for editing using the `-e` option. 

```bash
crontab -e
```

You want to add the following line which will run the df command every 5 minutes of every hour, of every day, of every month and of every week.  `*/5 * * * * df`

You can see what your crontab will look like in the screenshot below.

>**Note**: df is utility that provides disk usage details often referred to as "disk free".  Check for options using `df --help`

![crontab user](user-cron.png)

Now edit the file again change your crontab to run every 2 minutes and add `-h` after df to make the output simpler to read.

Every time the cron job runs it will send a message to you or the user defined in your crontab file.  To read these message at the command line we are going to install `mailx`.  There are other command line email clients with more functionality such as [**mutt**](http://www.mutt.org/) but for this demo we are going to use `mailx`.

Install **mailx** using dnf now if you have not already installed it `sudo dnf install mailx -y`.  Once installed launch mailx by entering `mailx` and pressing enter.

Now using mailx we can read the messages sent.  Launch mailx using `mail` or `mailx`, use the message number to read message or `delete #` to remove a message, `delete *` will delete all messages.

![mailx use](crontab-mailx.png)

You can comment out your cron job now by editing the crontab again and adding `#` before `*/2`. Or maybe set it to run once a day to allow for manual monitoring of your disk usage.

Nice job, you have now configured your own scheduled tasks in linux using crontab.

If you are interested you can become the root user and then check the root user's messages using mailx also.

```bash
sudo su - root
mailx
```

### Working with DNF

Now we are going to work with **DNF**.

The **dnf** download function will allow you to download a package to install later or to move around and install later.  We can also use the rpm tool to show the details of the package.  The `-ql` option for rpm is for list and query of your local rpm data.  We are able to add additional repositories of packages to our RHEL host in addition to the defaults.  We have been doing that already and it is very likley that you already the next two repositories enabled. These are the  Extra Packages for Enterprise Linux or __epel__ repository and the __Code Ready__ RHEL repository.  

>__Note:__ Review the redhat.repo file to see other possible repositories, look for which repos show "enabled = 1" vs "enabled = 0" (1 is enabled and 0 is disabled).

```bash
sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo subscription-manager repos --enable codeready-builder-for-rhel-8-x86_64-rpms
sudo subscription-manager repos --list-enabled
```

Now you should be able to download the lynx package and query the contents.  [Lynx] ](https://lynx.browser.org/).

```bash
sudo dnf download lynx
rpm -ql lynx-*
```

First we are going to see how to find the yum package that **provides** the locate tool. When we search we see a few packages that have locate included but if we look for the filepath you might notice one that comes out of /usr/bin which is a standard location.  That is the one we have been using and it can be found in the **mlocate** package.  In addition it was found in the **base** yum configuration file, which is part of the standard CentOS install.

```bash
yum provides "*/locate"
```

![yum provides](../lab20/yum-provides.png)

Next we are going to use **dnf download** to manually download a package and try to install it using the **rpm** command.  Remember DNF is a package management tool for rpm packages.  We are going to install a bandwidth monitor called [bmon](https://github.com/tgraf/bmon), this tool will monitor how much data is entering and exiting your host on a given interface.  Your development system has two interfaces the loopback interface and the external interface which is probably ens33 or ens192.

I expect that this install will fail, what does the error tell you?  move on to the next section for the solution.

>__Note:__ You must have the **epel** repository installed in order to download **bmon**

```bash
dnf download bmon
sudo rpm -ivh bmon-*
```

This install should fail due to a dependency.  Finding and installing dependencies automatically is what dfn does by default.  In this case we notice the error that rpm tool provides, read it and then go grab the required dependency.

```bash
dnf download libconfuse
rpm -ivh libconfuse-*
sudo rpm -ivh libconfuse-*
sudo rpm -ivh bmon-*
```

![install with rpm](../lab20/rpm-install.png)

Now launch bmon by entering `bmon` and enable the debug and information details as described on the initial page.  Use **q** to quit and exit the program. Check out the man page for bmon for other options such as launching with your preferred interface `bmon -p ens33`.

___

## Lab complete.

___

