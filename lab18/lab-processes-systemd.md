# In class lab: System processes and SystemD

>Objective: Work with systems processes including how to background and foreground a process.  Start and stop services using SystemD and explore the concept of run-levels and targets.

>Note: It is assumed that you are doing this lab on your **development** host and not your production server

## Working with jobs 

Remember a process is a running instance of a program. In this section of the lab we are trying to view these process details.

Process can be run in the foreground or the background. In this exercise we will start a simple **while** loop and push it to the background. The ampersand **"&"** is used to push a process to the background.  The first command **jobs** should return nothing as we don't have any jobs running in our shell yet. The second line should start our while loop.

```bash
 jobs
 while true; do echo "hello $USER, today is `date`" >> /tmp/hello.log ; sleep 5;  done &
```

Your shell is now running a **while** loop. This is an infinite loop as we have used **while true** with no way to enter a false value.

Next we are going to check the hello.log file we are creating.  

>**Question**: How often are we adding content to the **hello.log** file?

```shell
 cat /tmp/hello.log
 ```

Now we are going to check to see if jobs sees our background process and then start another while loop and push it to the background also.

```shell
 jobs
 while true; do echo "ssh connections: $SSH_CONNECTION" >> /tmp/ssh_connections.log ; sleep 3;  done &
 ```

This time instead of using cat we are going to use the **tail** command to view the log file we are creating in real time.  Use 

>**Question**: How often are we adding content to the **ssh_connections.log** file?

Once you see a couple updates use \<ctrl> + c to cancel the tail command.

```bash
 tail -f /tmp/ssh_connections.log
 ```

 Now we are going to stop/kill the first job we have in the background.  Using jobs we should see both jobs running but we want to bring the first job to foreground using the **fg** command.  To stop the hello user loop press \<ctrl> + c.

 ```bash
 jobs
 fg %1
 ```

Next enter fg to bring the other loop to the foreground again and use \<ctrl> + c to kill the loop also.

Here is a screenshot showing most of this process for reference.

![jobs](jobs.png)

## Working with processes

List all running processes, list all processes by user, review system stats and other options.

```shell
 ps -e
 ps -ef
 ps -u $USER
 ps -u $USER -f
 ps -eLf
 ps -ejH
 top
 top -u $USER
 pidof sshd
 echo $$
 pgrep sshd
```

Every process has a Process ID (PID).  You can use the PID to kill a process. One way to find a PID is to use **ps** and list all the processes.  Another option is to use **pgrep** or use **pidof**. You can kill processes using **kill**. In this section we are working to see our job process PID value and kill it.  The while loop is started in a bash shell which is what we need to kill to stop the loop. Remember **$$** returns your current bash shell PID.  Which PID should you kill to stop the background job? 

>__Note:__ ps -fj shows the output in full with jobs formatting.

```bash
 while true; do echo "ssh connections: $SSH_CONNECTION" >> /tmp/ssh_connections.log ; sleep 5;  done &
 jobs
 pgrep bash
 echo $$
 ps -fj 
 kill <PID>
```

What is the Parent Process ID (PPID) for the sleep command?

![working with jobs](jobs.jpg)

In this next example we are going to start the loop again, push it to the background and the bring it back to the foreground.  Once it is back in the foreground you will need to use ctrl+c to end the task

```shell
 while true; do echo "ssh connections: $SSH_CONNECTION" >> /tmp/ssh_connections.log ; sleep 5;  done &
 jobs
 fg %1
```

## Working with SystemD

>__Note__ This part of the lab will force you to login using the VMWare console.  If you were working with real hardware you would have to attach a keyboard and mouse to recover your server.

The systemd basic part is called a "unit". You can view the available units using:

```shell
 systemctl -t help
```

### Runlevels and targets

Previous versions of Linux initialized and shutdown services based on runlevels. However in Redhat 7 and later runlevels have been replaced by systemD and targets.
This image shows the relationship of targets to runlevels 

![Runlevels to targets](runlevels-targets.png)

You can see the default systemd target using the following command.

```shell
 systemctl get-default
```

List all the possible targets, this includes those not active

```shell
 systemctl list-units --type target --all
```

List all currently loaded target units

```shell
 systemctl list-units --type target
```

You can change your target; however, you might notice runlevels 2 through 4 all map to multi-user.target, this means that changing to runlevel 2 3 or 4 all takes the system to the same systemd target.  This can be customized if required. Administrators that worked with runlevels will likely recall that runlevels 2, 3 and 4 were also very similar. Switching to the rescue target will force you back to the VMWare console as you will lose network connectivity. Compare this to "safe mode" on windows hosts for those of you that have had to use that feature.

To switch to the "rescue" target enter the following command.

>NOTE: You will have to use the VMWare console to login after running this command.

```shell
sudo systemctl rescue
```

Now login using the VMWare console where you will need to enter your root password after which you should be able to login and run the list-units command again.

![System rescue mode](system-rescue-login.png)

Now we can check the loaded targets and this time I expect you will see less than 10 units loaded, likely around 6.  Previously when your current target was multi-user how many units were loaded?

```shell
 systemctl list-units --type target
```

![System rescue mode list](runlevels-targets-list.png)

Finally run systemctl default to return to your normal systemd target.

```shell
systemctl default
```

You can also use systemctl to shut the system down in place of commands like shutdown and reboot.

```shell
 systemctl reboot
 systemctl halt
 systemctl poweroff
```

>__Note:__ Running these commands on your production system of may require that you reach out for help starting your server again.

Targets configuration files are available for review under /usr/lib/systemd/system

```shell
ls -l runlevel*target /usr/lib/systemd/system
```

Review the configurations for runlevel 2, 3 and 4 to runlevel 1.

Services are managed using systemctl in RHEL based systems.  We can list the services using the following systemctl option.

```shell
 systemctl --type=service
```

Shows state of services, running, failed, exited, etc. using list-unit-files

```shell
 systemctl list-unit-files --type=service
```

## Systemd in action and working with Chronyd and NTP.

First let's check the time and get a status for the service.

```shell
 timedatectl status
```

Check your timezone and change it to the east coast.

```shell
 timedatectl | grep -i "time zone"
 timedatectl list-timezones
 sudo timedatectl set-timezone <zone value>
 timedatectl | grep -i "time zone"
```

Now configure your system to use chronyd to provide ntp services.

```shell
sudo dnf install chrony
rpm -qc chrony
view /etc/chrony.conf
```

Now we need to enable chrony, start chrony and set timedatectl to use chrony.  

>__Note:__ You can combine enable and start into one line using the --now option `sudo systemctl enable chronyd --now`.

```shell
 sudo systemctl enable chronyd
 sudo systemctl start chronyd
 systemctl status chronyd
 sudo timedatectl set-ntp yes
```

Now check to see if chrony is synchronized, check the sources and get status for the sources.

```shell
 chronyc tracking
 chronyc sources
 chronyc sourcestats
```

Now be sure to reset your timezone back to the your actual time zone before you end up being late to class next week :-)

___

## Lab complete

___