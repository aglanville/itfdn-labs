# In Class lab: Advanced permissions and ACLs

>Objective: Work with advanced permissions to view, set and update settings.  View, manage and restore ACLs

>Note: It is assumed that you are doing this lab on your **development** host and not your production server.

## Working with special permissions.

In this section we are going to create a two files and then change the permissions to enable setuid, setgid, and the sticky bit.  You should review the changes and note your observations and be prepared to discuss your conclusions.  Each time you display the file permissions note the updates (ls -la).  

>__Note:__ If you don't have the user Ken Thompson created review [lab6](https://www.ulcert.uw.edu/itfdn-labs/lab6/itfdn140-lab6.html) for tips.

>__Note:__ We will cover for loops later in class but there are some examples in this section.  


>__Question:__ Why is **kthompson** not able to delete the file `/tmp/mydata.txt

```bash
cd
mkdir demo
cd demo
for f in {1..5}; do echo "found report $f in system">> data1.txt; done
echo "Work to be completed on your development host" > data2.txt
chmod 444 data1.txt
chmod 777 data2.txt
ls -la
chmod 2444 data1.txt
chmod 2777 data2.txt
ls -la
chmod 4444 data1.txt
chmod 4777 data2.txt
ls -la
ls -ld /tmp
echo "users info and sign in dates" > /tmp/srv.txt
echo "user: $USER date: " /tmp/srv.txt
ls -la /tmp/srv.txt
sudo su - kthompson 
rm -f /tmp/srv.txt
echo "Content created on $HOSTNAME" > /tmp/new-srv.txt.txt
date >> /tmp/new-srv.txt.txt
ls -la /tmp 
exit
```

The screenshot below shows two files with special permissions applied and tries to highlight the differences when these permissions are applied to files with read or execute permissions.

>**Stretch task**: You might notice that the special permissions seem to have an upper and lowercase version.  The uppercase S is a visual indication that this permission is not set correctly.  Can you figure out what the problem is?

![special perms example](chmod-special-perms.png)

## Working with ACLs

Next we will take a look at access control lists in Linux.  ACLs help to bridge the gap between modern file permissions and the limitations experienced when working with standard Linux permissions.

Using facl we can display the existing access control lists on a given file or directory. Using the -d option we can display any `default` ACL permissions, it is likely that none are set and none will be displayed.

Using setfacl we are able set or update existing access control lists.  Remember that the `-d` provides a view of the default ACL permissions. 

>__Note:__ You should be signed in as your own user not kthompson for this section of the lab.

```bash
getfacl /home/
getfacl /home/${USER}
getfacl -d /home/${USER}
getfacl /tmp
getfacl -d /tmp
```

Next we are going to use setfacl to add ACLs to a folder. Compare the output between getfacl and getfacl -d what differences do you see?  What difference do you see between the two acl.txt files?

```bash
cd
sudo mkdir /secure
echo "private data stored here" > /tmp/acl-secured-data.txt ; sudo mv /tmp/acl-secured-data.txt /secure/
ls -la /secure
sudo chmod 700 /secure
ls -la /secure
sudo setfacl -m u:${USER}:r-x /secure/
cd /secure
cd /
getfacl /secure
getfacl -R /secure
```

Now we are going to create ACL rules for the users kthmopson and dritchie on the folder /secure


```bash
sudo setfacl -m u:kthompson:rwx /secure/
sudo setfacl -m d:u:kthompson:r-x /secure/
sudo setfacl -m d:u:dritchie:rwx /secure/
sudo setfacl -m u:dritchie:r-x /secure/
getfacl /secure
getfacl -d /secure
cd /
getfacl -R /secure > /home/${USER}/secure-all-acl.txt
getfacl /secure > /home/${USER}/secure-acl.txt
diff /home/${USER}/secure-all-acl.txt /home/${USER}/secure-acl.txt
setfacl -b /secure
getfacl /secure
setfacl --restore /home/${USER}/secure-acl.txt
```

## Git and Markdown

Now we are going to add some more notes to our Git repo and update our README.md file to point to these notes.

```bash
cd ~/git/gitlab/itfdn-140-inclass-$USER
```

next copy and paste this next snippet to get your initial file created.

```text
echo "# ACL tips

The following are notes and tips related to using **getfacl** and **setfacl**

" >> ~/git/gitlab/itfdn-140-inclass-$USER/acl-tips.md
```

Next add a link in your README.md file that points to this new file like this.

```bash
## Additional notes

The following are links to other resources in this repo.

[firewalld and firewall-cmd](firewalld-tips.md)
[getfacl and setfacl cmds](acl-tips.md)

```

Finally add these update to your gitlab project, commit the changes and then push them to your remote repository.

___

## Lab Complete

___






